import React, { Component } from "react";
import StorePageContent from "./StorePageContent.js";
import StoreOtherPageContent from "./StoreOtherPageContent.js";

class StorePage extends Component{
  constructor() {
    super();
    this.goBack = this.goBack.bind(this);
  }

  goBack() {
    this.props.history.push(`${this.props.mainPath}`);
  }

  render() {
    let diff = this.props.match.path.split("/")[3];
    if (this.props.match.path !== "/store/inventory/other") {
      return (
        <StorePageContent
          addToOrder={this.props.addToOrder}
          goBack={this.goBack}
          canCheckOut={this.props.canCheckout}
          diff={diff}
          url={this.props.match.path}
          products={this.props.products}
        />
      );
    } else {
      return (
        <StoreOtherPageContent
          addToOrder={this.props.addToOrder}
          goBack={this.goBack}
          diff={diff}
          canCheckOut={this.props.canCheckout}
          url={this.props.match.path}
          products={this.props.products}
        />
      );
    }
  }
}

export default StorePage;
