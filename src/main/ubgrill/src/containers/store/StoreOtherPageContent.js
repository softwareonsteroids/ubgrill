import React, { Component } from "react";
import ProductDetails from "../../components/store/ProductDetails.js";
import ProductItems from "../../components/store/ProductItems.js";
import CmdBtns from "../../components/extras/CmdBtns.js";
import { Switch, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import api from "../../services/api.js";
import "../../styles/store.css";

class StoreOtherPageContent extends Component {
  constructor() {
    super();
    this.state = {
      inventory: [],
      selectedProducts: [],
      routes: []
    };
    this.addPaths = this.addPaths.bind(this);
    this.addOnClick = this.addOnClick.bind(this);
  }

  addPaths(arr, url) {
    let arrToReturn = [];
    for (let i in arr) {
      let item = arr[i];
      item.path = api.generateProductUrl(url);
      arrToReturn.push(item);
    }
    return arrToReturn;
  }

  componentDidMount() {
    let generatedInventory = api.filterArr(this.props.products, this.props.diff);
    let r_routes = this.addPaths(generatedInventory, this.props.url);
    let t_routes = r_routes.map(item => {
      return {
        path: item.path,
        link: item.productPicLoc,
        exact: true,
        details: { title: item.productName, content: item.productDetails, typeOfImage: item.productType }
      };
    });
    t_routes = [].concat(t_routes, {
      path: this.props.url,
      link: null,
      exact: true,
      details: {}
    });
    this.setState({ inventory: generatedInventory, routes: t_routes });
  }

  addOnClick(val) {
    //console.log("Val: ", val);
    let itemToAdd;
    for (let i in this.state.inventory) {
      let item = this.state.inventory[i];
      if (item.productPicLoc === val) {
        //console.log("Item to add: ", item);
        itemToAdd = item;
        //console.log("Assigned item: ", itemToAdd);
      }
    }
    let addedProducts = [itemToAdd];
    this.setState(
      {
        selectedProducts: addedProducts
      },
      function () {
        //console.log("Selected Products: ", this.state.selectedProducts);
        //printArrOfObjects(this.state.selectedProducts);
        this.props.addToOrder(this.state.selectedProducts);
        this.props.canCheckOut();
      }
    );
  }

  render() {
    return (
      <div className="main-store-page">
        <BrowserRouter>
          <div className="product-details-box">
            <div className="other-page-nav">
              <ProductItems
                child={this.state.inventory}
                typeOfImage={this.props.diff}
              />
            </div>
            <Switch>
              {this.state.routes.map(item => (
                <Route
                  key={item.path}
                  path={item.path}
                  exact={item.exact}
                  render={props => (
                    <ProductDetails
                      addOnClick={this.addOnClick}
                      details={item.details}
                      image={item.link}
                    />
                  )}
                />
              ))}
            </Switch>
          </div>
        </BrowserRouter>
        <CmdBtns page={"store"} subpage={"page"} firstLink={this.props.goBack} />
      </div>
    );
  }
}
 
export default StoreOtherPageContent;