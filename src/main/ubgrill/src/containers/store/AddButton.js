import React, { Component } from "react";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import "../../styles/store.css";

class AddButton extends Component{
  constructor(){
    super();
    this.setProductToAdd = this.setProductToAdd.bind(this);    
  }
  
  setProductToAdd(){
    this.props.onClick(this.props.name);   
  }
  
  render(){
    return (
      <div onClick={this.setProductToAdd} className="product-details-add-button">
        <FontAwesomeIcon icon="plus-circle" size="2x" />
        <span>Add to Order</span>
      </div>
    );
  }
}

export default AddButton;
