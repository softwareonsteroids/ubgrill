import React, { Component } from "react";
import Carousel from "../extras/Carousel.js";
import CmdBtns from "../../components/extras/CmdBtns.js";
import ProductItems from "../../components/store/ProductItems.js";
import ProductDetails from "../../components/store/ProductDetails.js";
import { Switch, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import "../../styles/store.css";
import api from "../../services/api.js";

class StorePageContent extends Component{
  constructor() {
    super();
    this.state = {
      inventory: [],
      selectedProducts: [],
      routes: []
    };
    this.makeArrOfArr = this.makeArrOfArr.bind(this);
    this.addOnClick = this.addOnClick.bind(this);
  }

  makeArrOfArr(arr, splitNum, url) {
    let outsideArr = [];
    let insideArr = [];
    let count = 0;
    for (let i in arr) {
      let item = arr[i];
      item.path = api.generateProductUrl(url);
      insideArr.push(item);
      count = count + 1;
      if (count === splitNum) {
        outsideArr.push(insideArr);
        count = 0;
        insideArr = [];
      }
    }
    if (insideArr.length > 0) {
      outsideArr.push(insideArr);
    }
    return outsideArr;
  }

  componentDidMount() {
    let generatedInventory = api.filterArr(this.props.products, this.props.diff);
    generatedInventory = [].concat(
      this.makeArrOfArr(generatedInventory, 3, this.props.url)
    );
    let r_routes = [].concat(...generatedInventory);
    let t_routes = r_routes.map(item => {
      return {
        path: item.path,
        link: item.productPicLoc,
        exact: true,
        details: { title: item.productName, content: item.productDetails, typeOfImage: item.productType }
      };
    });
    t_routes = [].concat(t_routes, {
      path: this.props.url,
      link: null,
      exact: true,
      details: {}
    });
    this.setState({ inventory: generatedInventory, routes: t_routes });
  }

  addOnClick(val) {
    //console.log("Val: ", val);
    let itemToAdd;
    for (let i in this.state.inventory) {
      let subArr = this.state.inventory[i];
      for (let j in subArr) {
        if (subArr[j].productPicLoc === val) {
          //console.log("Item to add: ", subArr[j]);
          itemToAdd = subArr[j];
          //console.log("Assigned item: ", itemToAdd);
        }
      }
    }
    let addedProducts = [itemToAdd];
    this.setState({ selectedProducts: addedProducts }, function () {
        //console.log("Selected Products: ", this.state.selectedProducts);
        //printArrOfObjects(this.state.selectedProducts);
        this.props.addToOrder(this.state.selectedProducts);
        this.props.canCheckOut();
      }
    );
  } 
  
  render() {
    return (
      <div className="main-store-page">
        <BrowserRouter>
          <div className="product-details-box">
            <Carousel direction={"vertical"}>
              {this.state.inventory.map((arr, i) => (
                <ProductItems key={i} child={arr} />
              ))}
            </Carousel>
            <Switch>
              {this.state.routes.map(item => (
                <Route
                  key={item.path}
                  path={item.path}
                  exact={item.exact}
                  render={props => (
                    <ProductDetails
                      addOnClick={this.addOnClick}
                      details={item.details}
                      image={item.link}
                    />
                  )}
                />
              ))}
            </Switch>
          </div>
        </BrowserRouter>
        <CmdBtns page={"store"} subpage={"page"} firstLink={this.props.goBack} />
      </div>
    );
  }
}

export default StorePageContent;

