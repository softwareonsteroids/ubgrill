import React, { Component } from "react"; 
import { Switch, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import { ErrorAlert } from "../../components/extras/Alert.js";
import AppHeader from "../../components/extras/AppHeader.js";
import AppContainer from "../../components/extras/AppContainer.js";
import StorePage from "./StorePage.js";
import InitialStorePage from "../../components/store/InitialStorePage.js";
import api from "../../services/api.js";
import "../../styles/shell.css";

class Store extends Component{
  constructor(){
    super();
    this.state = {
      productCategories: [],
      inventory: [],
      orders: [],
      canGotoCheckOut: false,
      errorMessage: "",
      modalShown: false
    };
    this.goToAccountPage = this.goToAccountPage.bind(this);
    this.logout = this.logout.bind(this);
    this.addToOrders = this.addToOrders.bind(this);
    this.goToCheckoutPage = this.goToCheckoutPage.bind(this);
    this.goToReservationPage = this.goToReservationPage.bind(this);
    this.isGrillInOrder = this.isGrillInOrder.bind(this);
    this.submitOrder = this.submitOrder.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.setCanCheckout =  this.setCanCheckout.bind(this);
  }
  
  componentDidMount(){
    let that = this;
    api.getAllProducts(this.props.myToken)
      .then(function(response){
        console.log(response);
        let tempCategories = new Set(response.data.map(item => item.productType));
        let categories = [...tempCategories];
        let tempInventory = response.data;
        that.setState({ productCategories: categories, inventory: tempInventory});
    }).catch(function(error){
        console.log(error);
    });
  }
  
  setCanCheckout() {
    if ( this.state.orders.length === 0 && this.state.canGotoCheckOut === false) {
      this.setState({ canGotoCheckOut: true });
    }
  }
  
  goToAccountPage(){
    this.props.history.push("/me");    
  }
  
  goToReservationPage(){
    this.props.history.push("/reservations");   
  }
  
  goToCheckoutPage(){
    this.props.history.push("/checkout");    
  }
  
  isGrillInOrder(){
    let grillOrders = this.state.orders.filter(
      item => item.productType.toLowerCase() === "grills"    
    );
    if (grillOrders.length > 0){
      return true;
    } else {
      return false;    
    }
  }
  
  hideModal(){
    this.setState({ modalShown: false });    
  }
  
  submitOrder(){
     let response = api.retrieve("last_reservation");
     if (response.error.trim().length === 0){
       if(this.isGrillInOrder()){
         api.save("order", this.state.orders);
         this.goToCheckoutPage();
       } else {
         this.setState({ errorMessage: "You cant order without a grill!!", modalShown: true });    
       }
     } else {
       this.setState({ errorMessage: "You cannot order without a reservation!!", modalShown: true});  
     }
  }
  
  logout(){
    this.setState({
      productCategories: [],
      inventory: [],
      orders: [],
      canGotoCheckOut: false
    }, function() {
      api.reset();
      this.props.removeToken();
    });
  }
  
  addToOrders(arr){
    this.setState({ orders: [...this.state.orders, ...arr] }, function(){
      console.log("Selected Orders: ", this.state.orders);    
    });  
  }
  
  render(){
    let pagePaths = [];
    if (this.state.productCategories.length > 0){
      let path = this.props.path;
      pagePaths = this.state.productCategories.map(function(item){
        return {
          item: item.toLowerCase(),
          itemLink: `${path}/inventory/${item.toLowerCase()}`
        };
      });
      pagePaths.push({ item: null, itemLink: `${path}` });
    }
    
    return (
      <BrowserRouter>
        <div className="outerContainer mainBackground appshell">
          <AppHeader dropDownType="store" firstLink={this.goToAccountPage}
            secondLink={this.logout} />
          <AppContainer title={"Store"}>
            <ErrorAlert 
              show={this.state.modalShown}
              onClick={this.hideModal}
              title="OK"
              message={<div>{this.state.errorMessage}</div>}
            />
            <Switch>
              {pagePaths.map(
                 item => item.item !== null ? (
                   <Route exact path={item.itemLink} key={item.itemLink}
                     render={ ({ history, match }) => (
                       <StorePage addToOrder={this.addToOrders} 
                         typeOfPage={item.item} products={this.state.inventory}
                         canCheckout={this.setCanCheckout}
                         history={history} match={match} mainPath={this.props.path}
                       />
                     )}
                   />
                 ) : (
                   <Route exact path={item.itemLink} key={item.itemLink}
                     render={ ({ history, match }) => (
                       <InitialStorePage links={this.state.productCategories}
                         canCheckout={this.state.canGotoCheckOut} history={history}
                         goBack={this.goToReservationPage}
                         submitOrder={this.submitOrder} 
                         orderLength={this.state.orders.length}
                       />
                     )}
                   />
                 )
              )}
            </Switch>
          </AppContainer>
        </div>
      </BrowserRouter>
    );
  }
}

export default Store;