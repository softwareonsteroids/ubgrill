import React, { Component } from "react";
import "../../styles/appshell.css"; 

class SelectionItem extends Component {
  constructor() {
    super();
    this.state = {
      selected: false
    };
    this.setSelected = this.setSelected.bind(this);
  }

  componentDidMount() {
    if (this.props.iparams.length !== null && this.props.iparams.length > 0) {
      let selected = this.props.iparams === this.props.name;
      this.setState({ selected });
    }
  }

  setSelected(evt) {
    this.setState({ selected: !this.state.selected });
    if (this.state.selected === false) this.props.onClick(evt.target.value);
  }

  render() {
    return (
      <button
        className={this.state.selected ? "actual-item selected" : "actual-item"}
        type="button"
        onClick={this.setSelected}
        value={this.props.name}
      >
        <p>{this.props.name}</p>
      </button>
    );
  }
}

export default SelectionItem;
