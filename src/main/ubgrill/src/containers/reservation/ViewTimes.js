import React, { Component } from "react";
import "../../styles/reservation.css"; 

class ViewTimes extends Component {
  constructor(){
    super();
    this.state = {
      indexToBeRemoved: 0    
    };
    this.setIndexToBeRemoved = this.setIndexToBeRemoved.bind(this);
  }
  
  setIndexToBeRemoved(evt){
    this.setState({ indexToBeRemoved: parseInt(evt.target.name) }, function(){
      this.props.onClick(this.state.indexToBeRemoved);    
    });    
  }
  
  render(){
    return (
      <div className="times-item-container-outer">
        {this.props.values.map((item, i) => (
          <div className="times-item-container" key={i}>
            <div className="times-item-container-inner">
              <div>{item["fromTime"]}</div>
              <span>to</span>
              <div>{item["endTime"]}</div>
            </div>
            <button type="button" name={i} onClick={this.setIndexToBeRemoved}>X</button>
          </div>
        ))}
      </div>
    );
  }
}

export default ViewTimes;
