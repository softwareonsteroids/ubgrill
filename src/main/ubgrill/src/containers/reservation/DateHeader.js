import React, { Component } from "react";
import Arrow from "../../components/extras/Arrow.js";
import "../../styles/reservation.css";

class DateHeader extends Component{
  constructor(){
    super();
    this.state = {
      year: 0    
    };
    this.setYear = this.setYear.bind(this);
    this.decreaseYear = this.decreaseYear.bind(this);
    this.increaseYear = this.increaseYear.bind(this);
  }
  
  componentDidMount(){
    let curYear = parseInt(new Date().getFullYear(), 10);
    this.setState({ year: curYear });
  }
  
  setYear(val){
    this.props.setYear(val);
    this.setState({ year: val });
  }
  
  increaseYear(evt){
    evt.preventDefault();
    let curYear = this.state.year;
    curYear = curYear + 1;
    this.setYear(curYear);
  }
  
  decreaseYear(evt){
    evt.preventDefault();
    let curYear = this.state.year;
    curYear = curYear - 1;
    this.setYear(curYear);    
  }
 
  render(){
    return (
      <div className="date-header">
        <Arrow direction={"left"} onClick={this.decreaseYear} />
        <div className="year-box">
          <span>{this.state.year}</span>
        </div>
        <Arrow direction={"right"} onClick={this.increaseYear} />
      </div>
    );
  }
}

export default DateHeader;