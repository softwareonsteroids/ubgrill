import React, { Component } from "react";
import CalendarGridItem from "./CalendarGridItem.js";
import "../../styles/reservation.css";

class CalendarGrid extends Component{
  constructor(){
    super();
    this.state = {
      day:0,
      year:2018,
      allMonths: ["JAN", "FEB", "MAR", "APR", 
        "JUN", "JUL", "AUG", "SEP", "OCT",
        "NOV", "DEC"
      ], 
      daysMonths: [],
      itemsToMap: []
    };
    this.getNumOfDaysGivenMonth = this.getNumOfDaysGivenMonth.bind(this);
    this.getDaysInMonth = this.getDaysInMonth.bind(this);
    this.getArray = this.getArray.bind(this);
    this.setAvailableToBooked = this.setAvailableToBooked.bind(this);
  }
  
  componentDidMount() {
    let daysInMonth = [];
    for (let index in this.state.allMonths) {
      let obj = {};
      obj.month = this.state.allMonths[index];
      obj.numOfDays = this.getDaysInMonth(
        parseInt(index, 10) + 1,
        this.state.year
      );
      daysInMonth.push(obj);
    }
    this.setState({
      daysMonths: daysInMonth
    });
  }
  
  getNumOfDaysGivenMonth(month) {
    for (let index in this.state.daysMonths) {
      let obj = this.state.daysMonths[index];
      if (obj.month === month) {
        return obj.numOfDays;
      }
    }
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  getArray(val) {
    var k = [];
    for (var i = 1; i <= val; i++) {
      k.push(i);
    }
    return k;
  }
  
  setAvailableToBooked(day, mth, selectedday) {
    let arr = this.getArray(this.getNumOfDaysGivenMonth(mth));
    var l = [];
    for (let i in arr) {
      if (arr[i] < day) {
        l.push({
          item: arr[i],
          allbooked: false,
          canBook: false,
          hasBooked: false
        });
      } else {
        l.push({
          item: arr[i],
          allbooked: false,
          canBook: true,
          hasBooked: false
        });
      }
    }
    for (let i in l) {
      if (l[i].item === selectedday) {
        console.log("I have already booked this day: ", l[i].item);
        l[i] = Object.assign({}, l[i], { hasBooked: true });
      }
    }
    return l;
  }
  
  render() {
    let numOfItems = this.setAvailableToBooked(
      this.props.currentDay,
      this.props.month,
      this.props.selectedDay
    );
    return (
      <div className="calendar-grid">
        {numOfItems.map((item, i) => (
          <CalendarGridItem
            item={item.item}
            allbooked={item.allbooked}
            canBook={item.canBook}
            hasBooked={item.hasBooked}
            key={i}
            onClick={this.props.onClick}
          />
        ))}
      </div>
    );
  }
}
  

export default CalendarGrid;
