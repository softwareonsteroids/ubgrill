import React, { Component } from "react";
import Carousel from "../extras/Carousel.js";
import MonthItems from "../../components/reservation/MonthItems.js";
import AppHeader from "../../components/extras/AppHeader.js";
import AppContainer from "../../components/extras/AppContainer.js";
import DateHeader from "./DateHeader.js";
import CalendarGrid from "./CalendarGrid.js";
import EditReservation from "../../components/reservation/EditReservation.js";
import CmdBtns from "../../components/extras/CmdBtns.js";
import api from "../../services/api.js";
import "../../styles/shell.css";

class ReservationPage extends Component{
  constructor(){
    super();
    this.state = {
      selectedYear: 0,
      selectedMonth: " ",
      currentMonth: "",
      currentDay: 0,
      fromTime: " ",
      endTime: " ",
      selectedDay: 0,
      allMonths: [
        ["JAN", "FEB", "MAR"],
        ["APR", "MAY", "JUN"],
        ["JUL", "AUG", "SEP"],
        ["OCT", "NOV", "DEC"]
      ],
      canSubmit: false
    };
    this.logout = this.logout.bind(this);
    this.setSelectedDate = this.setSelectedDate.bind(this);
    this.setSelectedMonth = this.setSelectedMonth.bind(this);
    this.setSelectedYear = this.setSelectedYear.bind(this);
    this.setSelectedDay = this.setSelectedDay.bind(this);
    this.setCanNotSubmit = this.setCanNotSubmit.bind(this);
    this.setSelectedTimes = this.setSelectedTimes.bind(this);
    this.goToAccountPage = this.goToAccountPage.bind(this);
    this.goToStore = this.goToStore.bind(this);
    this.submitReservation = this.submitReservation.bind(this);
    this.getMntIndex = this.getMntIndex.bind(this);
    this.formatMthIndex = this.formatMthIndex.bind(this);
  }
  
  componentDidMount() {
    let unrolledMonths = [].concat(...this.state.allMonths);
    let curMonth = unrolledMonths[new Date().getMonth()];
    let curDay = new Date().getDate();
    let curYear = new Date().getFullYear();
    this.setState({ currentMonth: curMonth, currentDay: curDay, selectedYear: curYear });
   }
  
  setSelectedMonth(val) { this.setState({ selectedMonth: val }); }

  setSelectedYear(val) {  this.setState({ selectedYear: val }); }

  setSelectedTimes(arr) {
    this.setState({
      fromTime: arr.fromTime,
      endTime: arr.endTime,
      canSubmit: true
    });
  }

  setSelectedDay(val) { this.setState({ selectedDay: val }); }

  setCanNotSubmit() { this.setState({ canSubmit: false }); }
  
  setSelectedDate(day, month, year) {
    let selectedDate = "01 Jan 1900";
    if (day > 0) {
      selectedDate = day + " " + month + " " + year;
    }
    return selectedDate;
  }
  
  // Navigation
  goToAccountPage() {
    this.props.history.push("/me");
  }

  goToStore() {
    this.props.history.push("/store");
  }
  
  logout() {
    this.setState(
      {
        selectedYear: 0,
        selectedMonth: " ",
        currentMonth: "",
        currentDay: 0,
        fromTime: " ",
        endTime: " ",
        selectedDay: 0,
        allMonths: [
          ["JAN", "FEB", "MAR"],
          ["APR", "MAY", "JUN"],
          ["JUL", "AUG", "SEP"],
          ["OCT", "NOV", "DEC"]
        ],
        canNotSubmit: true
      },
      function() {
        api.reset();
        this.props.removeToken();
      }
    );
  }
  
  getMntIndex(mth) {
    let mths = ["JAN","FEB","MAR","APR","MAY",
        "JUN","JUL","AUG","SEP","OCT","NOV","DEC"
    ];  
    for (let i=0; i < mths.length; i++) {
      if (mths[i] === mth) {
        let count = i + 1;
        return count;
      }
    }
  }
  
  formatMthIndex(i){
    if (i < 10){
      return `0${i}`;   
    } else {
      return i.toString();
    }
  }
  
  submitReservation(){
    let reservation_new = {};
    let day_reserved = this.state.selectedDay.toString();
    let year_reserved = this.state.selectedYear.toString();
    let date_reserved = " ";
    if (this.state.selectedMonth.trim().length !== 0){
     date_reserved = year_reserved + "-" + this.formatMthIndex(day_reserved) +  "-" + this.formatMthIndex(this.getMntIndex(this.state.selectedMonth)); 
    } else {
     date_reserved = year_reserved + "-" + this.formatMthIndex(day_reserved) +  "-" + this.formatMthIndex(this.getMntIndex(this.state.currentMonth));
    }
    reservation_new["dateReserved"] = date_reserved + " 00:00:00";
    reservation_new["timeReservedStart"] = this.state.fromTime + ":00";
    reservation_new["timeReservedEnd"] = this.state.endTime + ":00";
    api.save("last_reservation", reservation_new);
    this.goToStore();
  }
  
  render(){
    let mthItems = this.state.currentMonth.length > 0 ? (
      <Carousel direction={"horizontal"}>
        {this.state.allMonths.map((arr, i) => (
          <MonthItems key={i} child={arr} onClick={this.setSelectedMonth}
            currentMth={this.state.currentMonth} />
        ))}
      </Carousel>
    ): null;
    
    let initialDate = this.setSelectedDate(
      this.state.selectedDay,
      this.state.currentMonth,
      this.state.selectedYear
    );
      
    return (
      <div className="outerContainer mainBackground appshell">
        <AppHeader dropDownType="reservation" firstLink={this.goToAccountPage}
          secondLink={this.logout} />
        <AppContainer title={"Reservation"}>
          <DateHeader setYear={this.setSelectedYear} />
          {mthItems}
          <CalendarGrid month={this.state.currentMonth} 
            currentDay={this.state.currentDay} selectedDay={this.state.selectedDay}
            onClick={this.setSelectedDay} />
          <EditReservation initialDate={initialDate} 
            setTimes={this.setSelectedTimes} setCanNotSubmit={this.setCanNotSubmit}
            />
          <CmdBtns page={"Reservation"} canSubmit={this.state.canSubmit} firstLink={this.submitReservation} />
        </AppContainer>
      </div>
    );
  }
}

export default ReservationPage;
