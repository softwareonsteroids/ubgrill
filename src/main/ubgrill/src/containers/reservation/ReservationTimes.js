import React, { Component } from "react";
import AddTimes from "./AddTimes.js";
import ViewTimes from "./ViewTimes.js";
import "../../styles/reservation.css";

class ReservationTimes extends Component{
  constructor(){
    super();
    this.state = {
      times: []    
    };
    this.setTimes = this.setTimes.bind(this);
    this.removeTimes = this.removeTimes.bind(this);
  }
  
  setTimes(fromTimeStr, endTimeStr){
    if (this.state.times.length < 1){
      let arr_obj = { fromTime: fromTimeStr, endTime: endTimeStr };
      let that = this;
      this.setState({ times: [...this.state.times, arr_obj] }, function(){
        that.props.setTimes(arr_obj);
      });
    }
  }
  
  removeTimes(index){
    let curItems = this.state.times;
    curItems.splice(index, 1);
    let that = this;
    this.setState({ times: curItems }, function () {
      that.props.setCanNotSubmit();
    });  
  }
  
  render(){
    return (
      <div className="reservation-times-container">
        <div>
           <p>
            <small>
              <i>
                * The start times for reservations are between 9:00(9.00am) and
                18:00(6.00pm). The end times are between 10.00(10.00am) and 
                22:10(10.00pm).
              </i>
            </small>
          </p>
          <p>
            <small>
              <i>* Minimum time to reserve is 1 hour</i>
            </small>
          </p>
        </div>
        <fieldset className="add-times-container">
          <legend> Add times to complete reservation </legend>
          <AddTimes onClick={this.setTimes} />
        </fieldset>
        <fieldset className="view-times-container">
          <legend> View booked times </legend>
          <ViewTimes values={this.state.times} onClick={this.removeTimes} />
        </fieldset>
      </div>
    );
  }
}

export default ReservationTimes;