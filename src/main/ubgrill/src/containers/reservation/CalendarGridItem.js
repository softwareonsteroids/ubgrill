import React, { Component } from "react";
import "../../styles/reservation.css";

class CalendarItem extends Component{
  constructor(){
    super();
    this.state = {
      hasBooked: false    
    };
    this.book = this.book.bind(this);
  }
  
  book() {
    this.setState({ hasBooked: !this.state.hasBooked }, function() {
      if (this.state.hasBooked === true) {
        console.log("Inner value:", this.props.item);
        this.props.onClick(this.props.item);
      }
    });
  }
  
  render() {
    let selectedStates =
      this.props.canBook === false ? (
        <div className="icon-cannot-book" />
      ) : (
        <div onClick={this.book} className="selection-box">
          <input className="selected-day-box" type="checkbox" checked={this.state.hasBooked} />
          <span />
        </div>
      );

    return (
      <div className="calendar-day-container">
        <div className="day-number">{this.props.item}</div>
        {selectedStates}
      </div>
    );
  }
}

export default CalendarItem;
