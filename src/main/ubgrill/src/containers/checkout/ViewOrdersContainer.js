import React, { Component } from "react";
import ViewOrderItemPrice from "../../components/checkout/ViewOrderItemPrice.js";
import "../../styles/checkout.css";

class ViewOrdersContainer extends Component{
   constructor() {
    super();
    this.state = {
      indexToBeRemoved: 0
    };
    this.setIndexToRemove = this.setIndexToRemove.bind(this);
  }

  setIndexToRemove(evt) {
    this.setState({ indexToBeRemoved: parseInt(evt.target.id) },
      function() { this.props.onClick(this.state.indexToBeRemoved); }
    );
  }

  render() {
    return (
      <div className="checkout-page-vieworderscontainer">
        {this.props.orders.map((item, i) => (
          <div className="checkout-page-vieworderscontainer-orderitem" key={i}>
            <span>{i + 1}</span>
            <div className="checkout-page-vieworderscontainer-orderitem-desc">
              <div style={{ fontSize: "1.5rem" }}>{item.productName}</div>
              <div
                className="checkout-page-vieworderscontainer-orderitem-desc-remove"
                id={i}
                onClick={this.setIndexToRemove}
              >
                Remove Item
              </div>
            </div>
            <ViewOrderItemPrice productType={item.productType} price={item.price}  />
            <span>{item.productQuantityPerOrder}</span>
          </div>
        ))}
      </div>
    );
  } 
}

export default ViewOrdersContainer;
