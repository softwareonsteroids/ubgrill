import React, { Component } from "react";
import AppHeader from "../../components/extras/AppHeader.js";
import AppContainer from "../../components/extras/AppContainer.js";
import ViewOrdersContainer from "./ViewOrdersContainer.js";
import ViewAddressContainer from "./AddressContainer.js";
import ViewReservationContainer from "../../components/checkout/ViewReservationContainer.js";
import ViewTotalsContainer from "../../components/checkout/ViewTotalsContainer.js";
import StatusBar from "../../components/checkout/StatusBar.js";
import CmdBtns from "../../components/extras/CmdBtns.js";
import api from "../../services/api.js";
import axios from "axios";
import "../../styles/shell.css";
import "../../styles/checkout.css";

class CheckoutPage extends Component{
  constructor() {
    super();
    this.state = {
      orders: [],
      reservation: {},
      reservation_display: {},
      delivery: {
        time: " ",
        miles: " ",
        feePerMile: 0.57,
        feeTotal: 0
      },
      orderTotal: 0,
      invoiceTotal: 0,
      address: {
        billing: {},
        delivery: {}
      },
      hasAcceptedFinalOrder: false,
      errors: {
        order: {},
        reservation: {}
      }
    };
    
    this.logout = this.logout.bind(this);
    this.goToAccountPage = this.goToAccountPage.bind(this);
    this.goToStorePage = this.goToStorePage.bind(this);
    this.goToReservationPage = this.goToReservationPage.bind(this);
    this.removeOrder = this.removeOrder.bind(this);
    this.toggleHasAccepted = this.toggleHasAccepted.bind(this);
    this.getAddress = this.getAddress.bind(this);
    this.calculateOrderTotal = this.calculateOrderTotal.bind(this);
    this.calculateInvoiceTotal = this.calculateInvoiceTotal.bind(this);
    this.submitFinalOrder = this.submitFinalOrder.bind(this);
    this.round = this.round.bind(this);
  }
  
  componentDidMount(){
    let tempOrders = api.retrieve("order");
    let tempReservation = api.retrieve("last_reservation");
    let that = this;
    this.setState({ orders: JSON.parse(tempOrders.data), reservation: JSON.parse(tempReservation.data) }, function(){
       that.calculateOrderTotal();
    });
  }
  
  logout(){
    this.setState({
      orders: [],
      reservation: {},
      reservation_display: {},
      delivery: {
        time: " ",
        miles: " ",
        feePerMile: 0.57,
        feeTotal: 0
      },
      orderTotal: 0,
      invoiceTotal: 0,
      address: {
        billing: {},
        delivery: {}
      },
      hasAcceptedFinalOrder: false,
      errors: {
        order: {},
        reservation: {}
      }  
    }, function(){
       api.reset();
       this.props.removeToken(); 
    });
  }
  
  goToAccountPage(){
    this.props.history.push("/me");    
  }
  
  goToStorePage(){
    this.props.history.push("/store");    
  }
  
  goToReservationPage(){
    this.props.history.push("/reservations");   
  }
  
  // Gotten from https://stackoverflow.com/questions/1726630/formatting-a-number-with-exactly-two-decimals-in-javascript
  round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }
  
  removeOrder(index) {
    let curOrders = this.state.orders;
    if (curOrders[index].productType.toLowerCase() !== "grills") {
      let itemRemoved = curOrders.splice(index, 1);
      if (Object.keys(this.state.errors).includes("order")){
        this.setState({ orders: curOrders, errors: { ...this.state.errors, order: {}} }, function() {
          this.calculateOrderTotal();
        });
      } else {
        this.setState({ orders: curOrders }, function() {
            this.calculateOrderTotal();
        });
      }
    } else {
      this.setState({ errors: { ...this.state.errors,
          order: { name: "Err", message: "Cannot remove grills." }
        }
      });
    }
  }
  
  toggleHasAccepted() { this.setState({ hasAcceptedFinalOrder: !this.state.hasAcceptedFinalOrder }); }
  
  getAddress(val, typeOfAddress) {
    let that  = this;
    if (typeOfAddress.toLowerCase() === "billingAddress".toLowerCase()) {
      this.setState({ address: Object.assign({}, this.state.address, { billing: val }) },
        function() {
          if ( api.isObjectEmpty(this.state.address.billing) === false &&
            api.isObjectEmpty(this.state.address.delivery) === false) {
            that.calculateInvoiceTotal();
          }
        }
      );
    } else if ( typeOfAddress.toLowerCase() === "deliveryAddress".toLowerCase()) {
      this.setState({ address: Object.assign({}, this.state.address, { delivery: val }) },
        function() {

          if ( api.isObjectEmpty(this.state.address.billing) === false &&
            api.isObjectEmpty(this.state.address.delivery) === false) {
            that.calculateInvoiceTotal();
          }
        }
      );
    }
  }
  
  calculateOrderTotal() {
    let tempStartTime = api.convertTime24to12(this.state.reservation.timeReservedStart);
    let tempEndTime = api.convertTime24to12(this.state.reservation.timeReservedEnd);
    let diffHrs = api.getHourDifference(tempEndTime, tempStartTime);
    let amounts = this.state.orders.map(function(item) {
      let val;
      if (item.productType.toLowerCase() !== "other") {
        val = item.price * diffHrs * item.productQuantity;
      } else {
        val = item.price * item.productQuantity;
      }
      return val;
    });
    let amountSum = amounts.reduce((total, item) => total + item);
    let orderTotal = Math.round(amountSum * 100) / 100;
    let that = this;
    this.setState({ orderTotal: orderTotal,reservation_display: Object.assign({}, this.state.reservation_display, {
          dateReserved: that.state.reservation.dateReserved.split(" ")[0],
          timeReservedStart: tempStartTime,
          timeReservedEnd: tempEndTime
        })
      }
    );
  }
  
  calculateInvoiceTotal() {
    let requestUrl = api.getGoogleRequestUrl( this.state.address.delivery );
    let that = this;
    axios
      .get(requestUrl)
      .then(function(response) {
        that.setState(
          {
            delivery: Object.assign({}, that.state.delivery, {
              miles: response.data.rows[0].elements[0].distance.text,
              time: response.data.rows[0].elements[0].duration.text
            })
          },
          function() {
            let miles = parseFloat(that.state.delivery.miles.split(" ")[0]);
            let milesTotalAmount = miles * that.state.delivery.feePerMile;
            let deliveryCost = parseFloat(this.round(milesTotalAmount, 2).toFixed(2));
            //let deliveryCost = Math.round(milesTotalAmount * 100) / 100;
            let tempInvoiceTotalAmount = that.state.orderTotal + deliveryCost;
            let invoiceTotalAmount = parseFloat(this.round(tempInvoiceTotalAmount, 2).toFixed(2));
            that.setState(
              {
                delivery: Object.assign({}, that.state.delivery, {
                  feeTotal: deliveryCost
                }),
                invoiceTotal: invoiceTotalAmount
              }
            );
          }
        );
      })
      .catch(function(reason) {
        console.log(
          "Error on making request to Google Distance Matrix API: ",
          reason
        );
      });parseFloat
  }
  
  //You have to match item name in the model class as your key in sent json
  
  submitFinalOrder(){
    let that = this;
    api.submitReservation(this.props.myToken, { ...this.state.reservation })
      .then(function(resp){
        let reservationId = resp.data.reservationId;
        let productsToOrder = that.state.orders.map(function(val){
          let item = {};
          item.productId = val.productId;
          item.productQuantity = val.productQuantityPerOrder;
          return item;
        });
        api.submitOrder(that.props.myToken, { reservation: reservationId, productsToBeOrdered: productsToOrder })
          .then(function(response){
            let orderId = response.data.orderId;
            api.addBillingAddressToOrder(that.props.myToken, orderId, { ...that.state.address.billing })
              .then(function(respon){ console.log(respon); })
              .catch(function(error1) { console.log("Cant create billing address: ", error1); });
            api.addShippingAddressToOrder(that.props.myToken, orderId, { ...that.state.address.delivery })
              .then(function(respon){ console.log(respon); })
              .catch(function(error1) { console.log("Cant create shipping address: ", error1); });
            api.createInvoice(that.props.myToken, orderId, { 
                deliveryTime: that.state.delivery.time, 
                deliveryCost: that.state.delivery.feeTotal, 
                deliveryMiles: parseInt(that.state.delivery.miles.split(" ")[0]) 
              }).then(function(respon){
                let invoice = {};
                invoice.isDelivered = false;
                invoice.deliveryDate = that.state.reservation.dateReserved;
                invoice.deliveryTime = that.state.delivery.time;
                invoice.deliveryDistance = that.state.delivery.miles;
                invoice.address = that.state.address;
                invoice.totalCost = that.state.invoiceTotal;
                invoice.products = that.state.orders;
                invoice.itemCost = that.state.orderTotal;
                api.save("invoice", invoice);
                that.goToAccountPage();
              }).catch(function(error1) { console.log("Cant create invoice: ", error1); });
          }).catch(function(reason){
            console.log("Cant create order: ", reason);   
          });
      }).catch(function(error){
        console.log("Cant create reservation: ", error);  
      });
  }
 
  render(){
    return(
      <div className="outerContainer mainBackground appshell">
        <AppHeader dropDownType="checkout" firstLink={this.goToAccountPage}
          secondLink={this.logout} />
        <AppContainer title={"Reservation"}>
          <div className="checkout-page">
             <fieldset className="section-space">
               <legend className="checkout-page-section-header">Orders</legend>
               <ViewOrdersContainer
                 orders={this.state.orders}
                 onClick={this.removeOrder}
               />
               <StatusBar
                 error={this.state.errors.order}
                 onClick={this.goToStorePage}
                 actionName={"Change Orders"}
               />
             </fieldset>
             <fieldset className="section-space">
               <legend className="checkout-page-section-header">Reservation </legend>
               <ViewReservationContainer
                 reservation={this.state.reservation_display}
               />
               <StatusBar
                 error={this.state.errors.reservation}
                 onClick={this.goToReservationPage}
                 actionName={"Change Reservation"}
               />
             </fieldset>
             <fieldset className="section-space">
               <legend className="checkout-page-section-header">Billing Address</legend>
               <ViewAddressContainer
                 address={this.state.address.billing}
                 onClick={this.getAddress}
                 typeOfAddress="BillingAddress"
               />
             </fieldset>
             <fieldset className="section-space">
               <legend className="checkout-page-section-header">Delivery Address</legend>
               <ViewAddressContainer
                 address={this.state.address.delivery}
                 onClick={this.getAddress}
                 typeOfAddress="DeliveryAddress"
               />
             </fieldset>
             <fieldset className="section-space">
               <legend className="checkout-page-section-header">Totals</legend>
               <ViewTotalsContainer
                 itemsAmount={this.state.orderTotal}
                 deliveryFee={this.state.delivery.feeTotal}
                 totalAmount={this.state.invoiceTotal}
               />
             </fieldset>
             <div className="checkout-page-main-checkbox">
              <input type="checkbox" name="Okay"
                checked={this.state.hasAcceptedFinalOrder}
                onChange={this.toggleHasAccepted}
                disabled={this.state.delivery.feeTotal === 0 && this.state.orderTotal !== 0 }
              />
              <label htmlFor="Okay">I am satisfied with my order</label>
             </div>
             <CmdBtns page={"Checkout"} canSubmit={this.state.hasAcceptedFinalOrder} firstLink={this.submitFinalOrder} />
          </div>
        </AppContainer>
      </div>
    );
  }
}

export default CheckoutPage; 
