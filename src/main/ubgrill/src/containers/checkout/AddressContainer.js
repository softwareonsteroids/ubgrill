import React, { Component } from "react";
import StatusBar from "../../components/checkout/StatusBar.js";
import api from "../../services/api.js";
import "../../styles/checkout.css";

const jsonp = require("jsonp");

class ViewAddressContainer extends Component {
  constructor() {
    super();
    this.state = {
      street: " ",
      city: " ",
      state: " ",
      country: " ",
      zipcode: " ",
      err: {},
      success: " "
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.setAddress = this.setAddress.bind(this);
  }

  handleInputChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  setAddress() {
    let stateAddr = this.state;
    delete stateAddr["err"];
    delete stateAddr["success"];
    let response = api.checkAddress(stateAddr);
    if (response) {
      let func = this.props.onClick;
      let addr = this.props.typeOfAddress;
      let url = api.getUriToSend(stateAddr);
      let that = this;
      jsonp(url, null, function (err, response) {
        if (err) {
          //console.log("Returned err: ", err);
          that.setState({ err: { name: "Err", message: JSON.stringify(err) } });
        } else {
          //console.log("Returned data: ", response);
          console.log( "Coordinates: ", response.result.addressMatches[0].coordinates );
          that.setState({ success: "Address is verified" });
          func(stateAddr, addr);
        }
      });
    } else {
      this.setState({ err: { name: "Err", message: "One of the fields is either empty or is not formatted properly"}
        }, function () {
          console.log("Error: ", response);
        }
      );
    }
  }

  render(){
    return(
      <div className="checkout-page-address">
        <div className="checkout-page-address-form-group">
          <label htmlFor="street"> Street </label>
          <input
            className="checkout-page-address-input street"
            name="street"
            type="text"
            onChange={this.handleInputChange}
          />
        </div>
        <div className="checkout-page-address-form-group">
          <div>
            <label htmlFor="city">City</label>
            <input
              className="checkout-page-address-input city"
              name="city"
              type="text"
              onChange={this.handleInputChange}
            />
          </div>
          <div>
            <label htmlFor="state">State</label>
            <input
              className="checkout-page-address-input state"
              name="state"
              type="text"
              onChange={this.handleInputChange}
            />
          </div>
        </div>
        <div className="checkout-page-address-form-group">
          <div>
            <label htmlFor="zipcode">Zipcode</label>
            <input
              className="checkout-page-address-input zipcode"
              name="zipcode"
              type="text"
              onChange={this.handleInputChange}
            />
          </div>
          <div>
            <label htmlFor="country">Country</label>
            <input
              className="checkout-page-address-input state"
              name="country"
              type="text"
              onChange={this.handleInputChange}
            />
          </div>
        </div>
        <StatusBar
          error={this.state.err}
          success={this.state.success}
          onClick={this.setAddress}
          actionName={"Check Address"}
        />
      </div>
    );
  }
}
 
export default ViewAddressContainer;