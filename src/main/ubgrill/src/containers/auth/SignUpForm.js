import React, { Component } from "react";
import Message from "../../components/extras/Message.js";
import { InfoAlert } from "../../components/extras/Alert.js";
import api from "../../services/api.js";
import "../../styles/shell.css";
import "../../styles/auth.css";
import "../../styles/signin.css";

class SignUpForm extends Component{
  constructor(){
    super();
    this.state = {
      username: "",
      password: "",
      fullname: "",
      email: "",
      formerrors: {
        username: "",
        password: "",
        fullname: "",
        email: ""
      },
      submission: {
        errorCode: 0,
        errorMessage: ""
      },
      modalShown: false
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }
    
  handleInputChange(evt){
    this.setState({ [evt.target.name]: evt.target.value });    
  }
  
  handleFormSubmit(evt){
    evt.preventDefault();
    let errors = api.validateFormInput(this.state);
    let response = api.doesFormHaveErrors(errors);
    let formdata = {
      username: this.state.username,
      password: this.state.password,
      firstname: this.state.fullname.split("/")[0],
      lastname: this.state.fullname.split("/")[1],
      email: this.state.email
      
    };
    if (response === true){
      this.setState({ formerrors: errors });
      return;
    }
    let that = this;
    api.register(formdata)
      .then(function(resp){
        console.log(resp);
        console.log(resp.data.username);
        console.log(resp.data.password);
        api.login({username: resp.data.username, password: that.state.password})
          .then(function(inresp){
            that.props.setToken(inresp.data.token, that.state.username);
        }).catch(function(reason){
            let submittedValues = {
              errorCode: 500,
              errorMessage: JSON.stringify(reason)
            };
            that.setState({ submission: submittedValues });
        });
    }).catch(function(error){
      let submittedValues = {
        errorCode: 500,
        errorMessage: JSON.stringify(error)
      };
      that.setState({ submission: submittedValues });
    });
  }
    
  showModal() { this.setState({ modalShown: true}); }
  
  hideModal() { this.setState({ modalShown: false}); }

  render(){
    let usernameStatus = this.state.formerrors.username.length > 0 ? (
      <Message name="username" error={this.state.formerrors.username} />
    ) : null;
      
    let passwordStatus = this.state.formerrors.password.length > 0 ? (
      <Message name="password" error={this.state.formerrors.password} />
    ) : null;
    
    let fullnameStatus = this.state.formerrors.fullname.length > 0 ? (
      <Message name="password" error={this.state.formerrors.fullname} />
    ) : null;
    
    let submissionStatus = this.state.submission.errorMessage.length > 0 ? (
      <Message name="LoginError" error={this.state.submission.errorMessage} />
    ) : null;
    
    return (
      <form className="inner-form-container" onSubmit={this.handleFormSubmit}>
        <InfoAlert 
          show={this.state.modalShown}
          onClick={this.hideModal}
          title="OK"
          message={
            <div>
              <div> Username should be at least 5 and at most 40 characters </div>
              <div> Password should be at least 8 charaters and contain at least one number </div>
              <div> Write your full name as Firstname/Lastname </div>
            </div>
          }
        />
        <div className="signin-form-group">
          <div>
            <label htmlFor="username">USERNAME</label>
            <input type="text" className="form-control" name="username" onChange={this.handleInputChange} required />
            {usernameStatus}
          </div>
          <div>
            <label htmlFor="password">PASSWORD</label>
            <input type="text" className="form-control" name="password" onChange={this.handleInputChange} required />
            {passwordStatus}
          </div>
          <div>
            <label htmlFor="fullname">FULLNAME</label>
            <input type="text" className="form-control" name="fullname" onChange={this.handleInputChange} required />
            {fullnameStatus}
          </div>
          <div>
            <label htmlFor="email">EMAIL</label>
            <input type="text" className="form-control" name="email" onChange={this.handleInputChange} required />
          </div>
          <div onClick={this.showModal} style={{ cursor: "pointer", fontSize: "1.25rem"}}>Instructions</div>
        </div>
        {submissionStatus}
        <footer>
          <input className="action-button" type="submit" name="submit" value="Register" />
        </footer>
      </form>
    );
  }
}

export default SignUpForm; 
