import React, { Component } from "react";
import CarouselItem from "../../components/extras/CarouselItem.js";
import Arrow from "../../components/extras/Arrow.js";
import "../../styles/appshell.css";

class Carousel extends Component{
  constructor(){
    super();
    this.state = {
      activeIndex: 0    
    };
    this.getNextSlide = this.getNextSlide.bind(this);
    this.getPrevSlide = this.getPrevSlide.bind(this);
  }
  
  getNextSlide(evt){
    evt.preventDefault();

    let items = this.props.children;
    let curIndex = this.state.activeIndex;
    let itemsLength = items.length;

    curIndex = (curIndex + 1) % itemsLength;

    this.setState({
      activeIndex: curIndex
    });   
  }
  
  getPrevSlide(evt){
    evt.preventDefault();

    let items = this.props.children;
    let curIndex = this.state.activeIndex;
    let itemsLength = items.length;

    curIndex = ((curIndex - 1) % itemsLength + itemsLength) % itemsLength;

    this.setState({
      activeIndex: curIndex
    });    
  }
  
  render(){
    let typeOfCarousel = `carousel-${this.props.direction}`;
    let typeOfInnerCarousel = `inner-carousel-${this.props.direction}`;
    let arrowsDirection = 
      this.props.direction === "horizontal" ? ["left", "right"] : ["up", "down"];
    return (
      <div className={typeOfCarousel}>
        <Arrow direction={arrowsDirection[0]} onClick={this.getPrevSlide} />
        <ul className={typeOfInnerCarousel}>
          {this.props.children.map((child, index) => (
            <CarouselItem
              key={index}
              index={index}
              activeIndex={this.state.activeIndex}
              child={child}
            />
          ))}
        </ul>
        <Arrow direction={arrowsDirection[1]} onClick={this.getNextSlide} />
      </div>
    );
  }
}

export default Carousel;