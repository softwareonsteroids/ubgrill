import React, { Component } from "react";
import AppHeaderDropDownItem from "../../components/extras/AppHeaderDropDownItem.js";
import "../../styles/appshell.css";

class AppHeaderDropDown extends Component {
  constructor() {
    super();
    this.state = {
      isVisible: false
    };
    this.show = this.show.bind(this);
  }

  show() {
    this.setState({ isVisible: !this.state.isVisible });
  }

  render() {
    let dropDownItems =
      this.props.dropDownType.toLowerCase() === "me" ? (
        <div className={this.state.isVisible ? "drop-down-list show" : "drop-down-list"}>
          <AppHeaderDropDownItem toGoto={this.props.firstLink} title={"Reservations"} />
          <AppHeaderDropDownItem toGoto={this.props.secondLink} title={"Store"} />
          <AppHeaderDropDownItem toGoto={this.props.thirdLink} title={"Logout"} />
        </div>
      ) : (
        <div className={this.state.isVisible ? "drop-down-list show" : "drop-down-list"}>
          <AppHeaderDropDownItem toGoto={this.props.firstLink} title={"My Account"} />
          <AppHeaderDropDownItem toGoto={this.props.secondLink} title={"Logout"} />
        </div>
      );

    return (
      <div className="drop-down-container">
        <button type="button" onClick={this.show} className={"acctn-logo-button"}>
          <div />
        </button>
        {dropDownItems}
      </div>
    );
  }
}

export default AppHeaderDropDown;