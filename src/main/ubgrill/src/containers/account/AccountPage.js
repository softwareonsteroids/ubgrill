import React , { Component } from "react";
import AppHeader from "../../components/extras/AppHeader.js";
import AppContainer from "../../components/extras/AppContainer.js";
import AccountControl from "./AccountControl.js";
import SetStatusContainer from "../../components/account/SetStatusContainer.js";
import OrderTableContainer from "./OrderTableContainer.js";
import { InfoAlert } from "../../components/extras/Alert.js";
import api from "../../services/api.js";
import axios from "axios";
import "../../styles/shell.css";

class AccountPage extends Component{
  constructor() {
    super();
    this.state = {
      username: " ",
      email: " ",
      password: " ",
      savedPassword: " ",
      firstname: " ",
      lastname: " ",
      valuesThatHaveChanged:{},
      formerrors: { },
      previousOrders: [],
      currentOrders: [],
      alertModalShow: false,
      newSelectedDate: " ",
      invoiceIdToChange: " "
    };
    this.logout = this.logout.bind(this);
    this.setAccountValue = this.setAccountValue.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
    this.cancelOrder = this.cancelOrder.bind(this);
    this.convertPasswordToProtected = this.convertPasswordToProtected.bind(
      this
    );
    this.showAlertModal = this.showAlertModal.bind(this);
    this.hideAlertModal = this.hideAlertModal.bind(this);
    this.goToReservationsPage = this.goToReservationsPage.bind(this);
    this.goToStorePage = this.goToStorePage.bind(this);
  }

  componentDidMount() {
    let that = this;
    api.getUserDetails(this.props.myToken)
      .then(function(response){
        let tempUsername = response.data.username;
        let tempEmail = response.data.email;
        let tempPassword = that.convertPasswordToProtected("password");
        let tempFirstname = response.data.firstname;
        let tempLastname = response.data.lastname;
        let tempOrders = [];
        api.getAllInvoices(that.props.myToken)
          .then(function(resp){
            if (resp.data.length !== 0){
              tempOrders = resp.data.map(function(item){
                let val = {};
                val.totalCost = item.orderCost + item.deliveryCost;
                val.deliveryTime = item.deliveryTime;
                val.deliveryDate = item.order.reservation.dateReserved;
                val.deliveryDistance = item.deliveryMiles;
                val.orderId = item.order.id;
                val.hasPayed = item.hasPayed;
                val.reservationId = item.order.reservation.reservationId;
                return val;
              });
              let orderIds = tempOrders.map(item => item.orderId);
              let tempLineItems = [];
              axios.all(orderIds.map(l => api.getAllLineItems(that.props.myToken, l)))
                .then(axios.spread(function(...res){
                  tempLineItems = res.map(item => item.data);
                  let temp_Orders = tempOrders.map(function(item, i){
                     item.products = tempLineItems[i];
                     return item;
                  });
                  let tempPreviousOrders = [];
                  tempPreviousOrders = temp_Orders.filter(item => item.hasPayed === true);
                  let tempCurrentOrders = [];
                  tempCurrentOrders = temp_Orders.filter(item => item.hasPayed === false);
                  that.setState({
                     username: tempUsername, email: tempEmail,password: tempPassword,
                     firstname: tempFirstname, lastname: tempLastname,previousOrders: tempPreviousOrders,
                     currentOrders: tempCurrentOrders
                  });
                })).catch(function(error9){
                  console.log("Error retreiving lineitems, ", error9); 
                });    
            } else {
              let retrievedData = api.retrieve("invoice");
              if (retrievedData.data.trim().length !== 0){
                tempOrders = retrievedData.data;    
              } 
              this.setState({
                username: tempUsername, email: tempEmail, password: tempPassword,
                firstname: tempFirstname, lastname: tempLastname, currentOrders: tempOrders  
              });
            }
          }).catch(function(reason){
            console.log("Cannot connect to invoices: ", reason);
          });
      }).catch(function(error){
        console.log("Cannot connect to api:  ", error);
      });
  }

  logout() {
    this.setState(
      {
      username: " ",
      email: " ",
      password: " ",
      savedPassword: " ",
      firstname: " ",
      lastname: " ",
      errors: "",
      previousOrders: [],
      currentOrders: [],
      modalShow: false,
      newSelectedDate: " ",
      invoiceIdToChange: " "
      },
      function() {
        api.reset();
        this.props.removeToken();
      }
    );
  }

  convertPasswordToProtected(str) {
    var strToReturn = "";
    // eslint-disable-next-line
    for (var char in str) {
      strToReturn = strToReturn + "*";
    }
    return strToReturn;
  }

  saveChanges() {
    let that = this;
    let obj = {}
    let changedValues = Object.keys(that.state.valuesThatHaveChanged).map(function(key){
       if (key.toLowerCase() === "savedPassword".toLowerCase()){
          obj.password = that.state[key];   
       } else {
          obj[key] = that.state[key];   
       }
       return key;
    });
    let errors = api.validateFormInput(obj);
    let response = api.doesFormHaveErrors(errors);
    if (response === true){
      this.setState({ formerrors: errors });
      return;
    }
    api.saveUserChanges(this.props.myToken, obj)
      .then(function(resp){
        console.log(resp);
        that.showAlertModal();
    }).catch(function(error){
      console.log("Cannot submit new user details: ", error);  
    });
  }

  goToReservationsPage() {
    this.props.history.push("/reservations");
  }

  goToStorePage() {
    this.props.history.push("/store");
  }

  setAccountValue(key, val) {
    let that = this;
    switch (key) {
      case "username":
        this.setState({ username: val, valuesThatHaveChanged: Object.assign({}, that.state.valuesThatHaveChanged, { username: true}) });
        break;
      case "email":
        this.setState({ email: val, valuesThatHaveChanged: Object.assign({}, that.state.valuesThatHaveChanged, { email: true}) });
        break;
      case "password":
        this.setState({ savedPassword: val, valuesThatHaveChanged: Object.assign({}, that.state.valuesThatHaveChanged, { savedPassword: true}) }, function() {
          this.setState({
            password: this.convertPasswordToProtected(this.state.savedPassword)
          });
        });
        break;
      case "firstname":
        this.setState({ firstname: val, valuesThatHaveChanged: Object.assign({}, that.state.valuesThatHaveChanged, { firstname: true}) });
        break;
      case "lastname":
        this.setState({ lastname: val , valuesThatHaveChanged: Object.assign({}, that.state.valuesThatHaveChanged, { lastname: true}) });
        break;
      default:
        console.log("Cant set value");
        break;
    }
  }

  showAlertModal(){
    this.setState({ alertModalShow: true });    
  }
  
  hideAlertModal(){
    let that = this;
    this.setState({ alertModalShow: false }, function(){
      that.logout();   
    });
  }

  cancelOrder(name, isPrevious) {
    let that = this;
    if (isPrevious) {
      let orders = this.state.previousOrders.map(function(item) {
        if (item.orderId !== name) {
          return item;
        }
      });
      if (orders.length === 1){
        orders = [];    // that is it only contains me
      }
      this.setState({ previousOrders: orders });
    } else {
      let orders = this.state.currentOrders.map(function(item) {
        if (item.orderId !== name) {
          return item;
        }
      });
      if (orders.length === 1){
        orders = [];   
      }
      this.setState({ currentOrders: orders }, function() {
          api.cancelOrder(that.props.myToken, name)
            .then(function(response){
              console.log(response.data);  
            }).catch(reason => console.log(reason));
        }
      );
    }
  }

  render() {
    let currentTable =
      this.state.currentOrders.length !== 0 ? (
        <OrderTableContainer
          orders={this.state.currentOrders}
          typeOfHeader="current"
          isPrevious={false}
          onClick={this.cancelOrder}
          path={this.props.path}
          newDate={this.state.newSelectedDate}
          invoiceIdToChange={this.state.invoiceIdToChange}
          changeReservationDate={this.showModal}
        />
      ) : null;

    let previousTable =
      this.state.previousOrders.length !== 0 ? (
        <OrderTableContainer
          orders={this.state.previousOrders}
          typeOfHeader="previous"
          isPrevious={true}
          onClick={this.cancelOrder}
          path={this.props.path}
          newDate={this.state.newSelectedDate}
          invoiceIdToChange={this.state.invoiceIdToChange}
          changeReservationDate={this.showModal}
        />
      ) : null;  
    
    let passMessageToDisplay = this.state.savedPassword.trim().length !== 0 ? (
        <div> New password: {this.state.savedPassword} </div>
    ): (
        <div> Your password has not changed </div>
    );
    
    let usernameMessageToDisplay = this.state.username.trim().length !== 0 ? (
        <div> New password: {this.state.username} </div>
    ): (
        <div> Your username has not changed </div>
    );
      
    return (
      <div className="outerContainer mainBackground appshell">
        <AppHeader
          dropDownType="me"
          firstLink={this.goToReservationsPage}
          secondLink={this.goToStorePage}
          thirdLink={this.logout}
        />
        <AppContainer title={"Me"}>
          <InfoAlert 
            show={this.state.alertModalShow}
            onClick={this.hideAlertModal}
            title="OK"
            message={
              <div>
                {usernameMessageToDisplay}
                {passMessageToDisplay}
              </div>
            }
          />
          <AccountControl
            typeOfInput="text"
            name="Username"
            accountValue={this.state.username}
            onClick={this.setAccountValue}
          />
          <AccountControl
            typeOfInput="email"
            name="Email"
            accountValue={this.state.email}
            onClick={this.setAccountValue}
          />
          <AccountControl
            typeOfInput="text"
            name="Password"
            accountValue={this.state.password}
            onClick={this.setAccountValue}
          />
          <AccountControl
            typeOfInput="text"
            name="Firstname"
            accountValue={this.state.firstname}
            onClick={this.setAccountValue}
          />
          <AccountControl
            typeOfInput="text"
            name="Lastname"
            accountValue={this.state.lastname}
            onClick={this.setAccountValue}
          />
          <SetStatusContainer
            errors={this.state.formerrors}
            saveChanges={this.saveChanges}
          />
          {currentTable}
          {previousTable}
        </AppContainer>
      </div>
    );
  }
}

export default AccountPage;
