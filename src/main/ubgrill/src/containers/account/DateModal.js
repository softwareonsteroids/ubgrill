import React, { Component } from "react";
import Modal from "../../components/extras/Modal.js";
import DateSelect from "./DateSelect.js";

class DateModal extends Component {
  constructor() {
    super();
    this.state = {
      day: 0,
      month: " ",
      year: 2018,
      months: [
        "JAN",
        "FEB",
        "MAR",
        "APR",
        "MAY",
        "JUN",
        "JUL",
        "AUG",
        "SEP",
        "OCT",
        "NOV",
        "DEC"
      ],
      selectedDate: " ",
      monthValues: [],
      oldMthValues: [],
      dayValues: []
    };
    this.getDaysInMonth = this.getDaysInMonth.bind(this);
    this.getMntIndex = this.getMntIndex.bind(this);
    this.formatMthIndex = this.formatMthIndex.bind(this);
    this.getDayVals = this.getDayVals.bind(this);
    this.setSelectedDay = this.setSelectedDay.bind(this);
    this.setSelectedMth = this.setSelectedMth.bind(this);
    this.setSelectedDate = this.setSelectedDate.bind(this);
  }

  componentDidMount() {
    let that = this;
    let tempMthVals = this.state.months;
    let curMth = new Date().getMonth();
    for (var cnt = 0; cnt < curMth + 1; cnt++) {
      tempMthVals.shift();
    }
    let mthVals = tempMthVals.map(function(item) {
      return { value: item, label: item };
    });
    this.setState(
      {
        monthValues: mthVals
      },
      function() {
        console.log(("Month values: ": that.state.monthValues));
      }
    );
  }

  getMntIndex(mth) {
    let mths = ["JAN","FEB","MAR","APR","MAY",
        "JUN","JUL","AUG","SEP","OCT","NOV","DEC"
    ];  
    for (let i=0; i < mths.length; i++) {
      if (mths[i] === mth) {
        let count = i + 1;
        return count;
      }
    }
  }
  
  formatMthIndex(i){
    if (i < 10){
      return `0${i}`;   
    } else {
      return i.toString();
    }
  }

  setSelectedDay(selectedDay) {
    this.setState({ day: selectedDay },
      function() {
        console.log("Selected day: ", this.state.day);
        if (this.state.month) {
          let newDate = 
            this.state.year + "-" + this.formatMthIndex(this.getMntIndex(this.state.month)) + "-" + this.formatMthIndex(this.state.day);
          this.setState({ selectedDate: newDate },
            function() {
              console.log("Selected Date: ", this.state.selectedDate);
            }
          );
        }
      }
    );
  }

  setSelectedMth(selectedMth) {
    this.setState({ month: selectedMth },
      function() {
        console.log("Selected month: ", this.state.month);
        this.getDayVals(this.state.month);
        if (this.state.day) {
          let newDate = 
            this.state.year + "-" + this.formatMthIndex(this.getMntIndex(this.state.month)) + "-" + this.formatMthIndex(this.state.day);
          this.setState({ selectedDate: newDate },
            function() {
              console.log("Selected Date: ", this.state.selectedDate);
            }
          );
        }
      }
    );
  }

  getDayVals(mth) {
    let vals = [];
    let selectedMthIndex = this.getMntIndex(mth);
    let numOfDays = this.getDaysInMonth(selectedMthIndex, this.state.year);
    for (var cnt = 1; cnt < numOfDays + 1; cnt++) {
      vals.push({ value: cnt, label: cnt });
    }
    this.setState(
      {
        dayValues: vals
      },
      function() {
        console.log("Day values: ", this.state.dayValues);
      }
    );
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  setSelectedDate() {
    this.props.onClick(this.state.selectedDate);
  }

  render() {
    return (
      <Modal
        show={this.props.show}
        handleClose={this.setSelectedDate}
        buttonMessage="Set New Date"
      >
        <div
          style={{
            display: "flex",
            flexFlow: "column nowrap",
            height: "60%",
            width: "100%"
          }}
        >
          <div style={{ flex: "1" }}>
            <div>Month</div>
            <DateSelect
              options={this.state.monthValues}
              onClick={this.setSelectedMth}
            />
          </div>
          <div style={{ flex: "1" }}>
            <div>Day</div>
            <DateSelect
              options={this.state.dayValues}
              onClick={this.setSelectedDay}
            />
          </div>
        </div>
      </Modal>
    );
  }
}
 
export default DateModal;