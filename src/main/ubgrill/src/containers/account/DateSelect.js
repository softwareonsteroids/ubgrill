import React, { Component } from 'react';
import Select from "react-select";
import "react-select/dist/react-select.css";

class DateSelect extends Component {
  constructor() {
    super();
    this.state = {
      selectedOption: ''
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  handleSelectChange(selectedOption) {
    this.setState({ selectedOption }, function () {
      this.props.onClick(selectedOption.label);
      console.log(`Selected: ${selectedOption.label}`);
    });
  }

  render() {
    const { selectedOption } = this.state;
    const value = selectedOption && selectedOption.value;

    return (
      <Select
        name="form-field-name"
        value={value}
        onChange={this.handleSelectChange}
        options={this.props.options}
      />
    );
  }
} 

export default DateSelect;
