import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../../styles/account.css";

class OrderItem extends Component {
  constructor() {
    super();
    this.state = {
      orderToRemove: " "
    };
    this.setOrderToRemove = this.setOrderToRemove.bind(this);
  }

  setOrderToRemove(evt) {
    this.setState(
      {
        orderToRemove: evt.target.name
      },
      function () {
        console.log("Order to remove: ", this.state.orderToRemove);
        this.props.onClick(this.state.orderToRemove);
      }
    );
  }

  render() {
    let item =
      this.props.typeOfHeader === "current" ? (
        <button
          onClick={this.setOrderToRemove}
          name={this.props.orderId}
          className="cancel-button"
        >
          X
        </button>
      ) : this.props.typeOfHeader === "previous" ? (
        <p>{this.props.date}</p>
      ) : null;

    if (this.props.link !== "/me") {
      return (
        <div className="account-page-ordertablecontent-orderitem">
          <div className="account-page-ordertablecontent-orderitem-cell">
            <Link to={this.props.link}>{this.props.orderId}</Link>
          </div>
          <div className="account-page-ordertablecontent-orderitem-cell-other">
            {item}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

const OrderItemColumns = props => {
  return (
    <div className="account-page-ordertablecontent-columns">
      {props.orders.map((item, i) => (
        <OrderItem
          key={i}
          orderId={item.details.orderId}
          date={item.details.deliveryDate}
          typeOfHeader={props.typeOfHeader}
          link={item.path}
          onClick={props.onClick}
        />
      ))}
    </div>
  );
};

export default OrderItemColumns; 
