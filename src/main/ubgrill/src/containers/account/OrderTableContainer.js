import React, { Component } from "react";
import OrderTableHeader from "../../components/account/OrderTableHeader.js";
import OrderTable from "../../components/account/OrderTableContent.js";
import api from "../../services/api.js";
import "../../styles/account.css";

class OrderTableContainer extends Component {
  constructor() {
    super();
    this.state = {
      myroutes: []
    };
    this.cancelOrder = this.cancelOrder.bind(this);
  }

  componentDidMount() {
    let path = this.props.path;
    let tempRoutes = this.props.orders.map(function(item) {
      let route = {};
      route["path"] = api.generateProductUrl(path);
      route["exact"] = true;
      let details = {};
      details["deliveryTime"] = item.deliveryTime;
      details["deliveryDate"] = item.deliveryDate;
      details["deliveryDistance"] = item.deliveryMiles;
      details["isDelivered"] = "No";
      details["orderId"] = item.orderId;
      details["reservationId"] = item.reservationId;
      details["itemDetails"] = item.products;
      details["total"] = item.totalCost;
      route["details"] = details;
      return route;
    });
    tempRoutes = [].concat(tempRoutes, {
      path: `${this.props.path}`,
      exact: true,
      details: {}
    });
    this.setState({ myroutes: tempRoutes });
  }

  cancelOrder(name) {
    let newRoutes = this.state.myroutes.filter(
      item => item.details.orderId !== name
    );
    this.setState({ myroutes: newRoutes },
      function() {
        this.props.onClick(name, this.props.isPrevious);
      }
    );
  }

  render() {
    let title = this.props.typeOfHeader.toUpperCase() + " ORDERS";
    return (
      <div className="account-page-ordertablecontainer">
        <p>
          <b>{title}</b>
        </p>
        <OrderTableHeader typeOfHeader={this.props.typeOfHeader} />
        <OrderTable
          routes={this.state.myroutes}
          cancelOrder={this.cancelOrder}
          typeOfHeader={this.props.typeOfHeader}
          changeReservationDate={this.props.changeReservationDate}
          newDate={this.props.newDate}
          invoiceIdToChange={this.props.invoiceIdToChange}
        />
      </div>
    );
  }
}
 
export default OrderTableContainer;