import React, { Component } from "react";
import Toggle from "react-toggle";
import "react-toggle/style.css";
import "../../styles/account.css";

class AccountControl extends Component {
  constructor() {
    super();
    this.state = {
      val: " ",
      canEdit: false
    };
    this.handleToggleChange = this.handleToggleChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleToggleChange(evt) {
    this.setState(
      {
        canEdit: evt.target.checked
      },
      function () {
        console.log("Can edit: ", this.state.canEdit);
        if (this.state.canEdit === false && this.state.val) {
          console.log("Val, ", this.state.val);
          this.props.onClick(this.props.name.toLowerCase(), this.state.val);
        }
      }
    );
  }

  handleInputChange(evt) {
    this.setState({
      val: evt.target.value
    });
  }

  render() {
    let inputView =
      this.state.canEdit === false ? (
        <div className="accountpage-accntcontrol-innerview-inputview">
          <p>{this.props.accountValue}</p>
        </div>
      ) : (
          <input
            type={this.props.typeOfInput}
            className="input-control accnt-control"
            name={this.props.name.toLowerCase()}
            onChange={this.handleInputChange}
          />
        );

    return (
      <div className="accountpage-accntcontrol">
        <div className="accountpage-accntcontrol-innerview">
          <label
            className="accountpage-accntcontrol-innerview-label"
            htmlFor={this.props.name.toLowerCase()}
          >
            {this.props.name}:
          </label>
          {inputView}
        </div>
        <Toggle
          defaultChecked={this.state.canEdit}
          icons={false}
          onChange={this.handleToggleChange}
        />
      </div>
    );
  }
} 

export default AccountControl