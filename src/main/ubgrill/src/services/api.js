const axios = require("axios");
const generateUUID = require("nanoid/generate");
let storage = window.localStorage;
const backendUrl = "http://localhost:9090/ubgrill/api";
const GEOCODINGAPIURI = "https://geocoding.geo.census.gov/geocoder/locations/address?";
const GOOGLEMAPSAPIKEY = "AIzaSyCe43xuJivO4RphF2c5PgPWdkbxlFz4mL0";
const CORSPROXYURI = "https://cors-anywhere.herokuapp.com/";
const GOOGLEDISTANCEMATRIXURI = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial";
const ORIGINURL = "&origins=1002+DemingWay+Madison+WI+53717";

let api = {
    save: function(key, value){
        let valueToSave;
        if (typeof(value) !== "string"){
            valueToSave = JSON.stringify(value);
        } else {
            valueToSave = value;
        }
        storage.setItem(key, valueToSave);
    },
    reset: function(){
      storage.clear();  
    },
    retrieve: function(key){
        let response = {};
        response.data = null;
        response.error = `There is no value for this key: ${key}`;
        if ( storage.getItem(key) !== null){
            let retrData = storage.getItem(key);
            if (typeof(retrData) === "string"){
                response.data= retrData;
            } else {
                response.data = JSON.parse(retrData);
            }
            response.error = "";
        } else {
            response.data = "";
        }
        return response;
    },
    login: function(data){
        let loginUrl = backendUrl + "/auth";
        let dataToSend = JSON.stringify(data);
        return axios.post(loginUrl, data: dataToSend);
    },
    register: function(data){
        let registerUrl = backendUrl + "/register";
        let dataToSend = JSON.stringify(data);
        return axios.post(registerUrl, data: dataToSend);
    },
    submitReservation: function(token, data){
        let tokenToSend = " Bearer " + token;
        let reservationUrl = backendUrl + "/my/reservations";
        let dataToSend = JSON.stringify(data);
        return axios.post(reservationUrl, data: dataToSend, {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    saveUserChanges: function(token, data){
        let tokenToSend = " Bearer " + token;
        let reservationUrl = backendUrl + "/me/edit";
        let dataToSend = JSON.stringify(data);
        return axios.patch(reservationUrl, data: dataToSend, {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    submitOrder: function(token, data){
        let tokenToSend = " Bearer " + token;
        let ordersUrl = backendUrl + "/my/orders";
        let dataToSend = JSON.stringify(data);
        return axios.post(ordersUrl, data: dataToSend, {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    addBillingAddressToOrder: function(token, orderId, data){
        let tokenToSend = " Bearer " + token;
        let addressUrl = backendUrl + "/my/orders/" + orderId + "/billing";
        let dataToSend = JSON.stringify(data);
        return axios.post(addressUrl, data: dataToSend, {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    addShippingAddressToOrder: function(token, orderId, data){
        let tokenToSend = " Bearer " + token;
        let addressUrl = backendUrl + "/my/orders/" + orderId + "/shipping";
        let dataToSend = JSON.stringify(data);
        return axios.post(addressUrl, data: dataToSend, {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    // You get a contentType error 415 http status code if you send float with json
    // so you have to change the content-type from application/www-url-encoded to application/json
    createInvoice: function(token, orderId, data){
        let tokenToSend = " Bearer " + token;
        let invoiceUrl = backendUrl + "/my/orders/" + orderId + "/invoice";
        let dataToSend = JSON.stringify(data);
        return axios.post(invoiceUrl,  dataToSend, {
            headers: {
                "Authorization": tokenToSend,
                "Content-Type": "application/json"
            }
        });
    },
    cancelOrder: function(token, orderId){
        let tokenToSend = " Bearer " + token;
        let cancelUrl = backendUrl + "/my/orders/" + orderId;
        return axios.delete(cancelUrl,  {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    getAllInvoices: function(token){
        let tokenToSend = " Bearer " + token;
        let invoiceUrl = backendUrl + "/my/invoices";
        return axios.get(invoiceUrl,  {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    getAllLineItems: function(token, orderId){
        let tokenToSend = " Bearer " + token;
        let lineitemsUrl = backendUrl + "/my/orders/" + orderId + "/lineitems";
        return axios.get(lineitemsUrl,  {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    getAllProducts: function(token){
        let tokenToSend = " Bearer " + token;
        let productsUrl = backendUrl + "/products";
        return axios.get(productsUrl, {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    getUserDetails: function(token){
        let tokenToSend = " Bearer " + token;
        let accountUrl = backendUrl + "/me";
        return axios.get(accountUrl, {
            headers: {
                "Authorization": tokenToSend 
            }
        });
    },
    printArrOfObjects: function(arr){
        let strarray = arr.map(item => {
            return JSON.stringify(item);
        });
        let str = "[" + strarray.join(",") + "]";
        console.log(str);
    },
    _hasNoNumbers: function(str){
        let re = /^[A-Za-z]+$/;
        if (re.test(String(str).toLowerCase())){
            return true;
        } else {
            return false;
        }
    },
    _hasNoSlash: function(str) {
        let re = /[a-zA-Z]\/[a-zA-Z]/;
        if (re.test(String(str).toLowerCase())){
            return true;  
        } else {
            return false;
        }
    },
    titleCase: function(str){
        return str.toLowerCase().split(" ").map(function(word){
            return word.charAt(0).toUpperCase() + word.slice(1);
        }).join(" ");
    },
    generateProductUrl: function(uri){
        return uri + "/" + generateUUID("1234567890abcedfghijklmonpqrstuvwxyzABCEDFGHIJKLMNOPQRSTUVWY", 10);
    },
    filterArr: function(arr, item){
        var arrReturn = [];
        for (let i in arr) {
            if (arr[i].productType.toLowerCase() === item.toLowerCase()) {
                arrReturn.push(arr[i]);
            }
        }
        return arrReturn;
    },
    validateFormInput: function(form){
        var inputerrors = {...form.formerrors };
        Object.keys(form).forEach(function(key){
            switch(key){
                case "username":
                case "firstname":
                case "lastname":
                    if (form[key].trim().length < 5){
                        inputerrors[key] = `${key} must be at least 5 characters`;
                    } else if (form[key].trim().length > 40){
                        inputerrors[key] = `${key} must be at most 40 characters`;
                    }
                    break;
                case "password":
                case "savedPassword":
                    if (form[key].trim().length < 8){
                        inputerrors[key] = `${key} must be at least 8 characters`;
                    }
                    if (api._hasNoNumbers(form[key]) === true){
                        inputerrors[key] = `${key} must contain uppercase, lowercase and numbers!`;
                    }
                    break;
                case "email":
                    // bad idea to test email 
                    let re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
                    if (re.test(String(form[key]).toLowerCase()) === false) {
                        inputerrors[key] = "Email is not valid";
                    }
                    break;
                case "fullname":
                    if (api._hasNoSlash(form[key]) === false){
                        inputerrors[key] = `${key} must be in the form lastname/firstname!`;
                    }
                    let arr = form[key].split("/");
                    // eslint-disable-next-line
                    arr.map(function(item) {
                        if (item.trim().length < 5){
                            inputerrors[key] = `${key} must be at least 5 characters`;
                        }
                        if (api._hasNoNumbers(item) === false){
                            inputerrors[key] = `${key} should not have a number`;
                        }
                    });
                    break;
                default:
                    break;
            }
        });
        return inputerrors;
    },
    doesFormHaveErrors: function(errors){
        return Object.keys(errors).some(x => errors[x]);
    },
    isObjectEmpty: function(objet){
        for (var i in objet){
            return false;
        }
        return true;
    },
    emptyObject: function(objet){
        let prs = Object.getOwnPropertyNames(objet);
        for (var i=0; i < prs.length; i++){
          delete objet[prs[i]];    
        }
    }, 
    convertTime24to12: function(time24h){
        let [hours, minutes, secs] = time24h.split(":");
        let modifier = "AM";
        if (parseInt(hours, 10) > 12) {
            modifier = "PM";
            hours = parseInt(hours, 10) - 12;
        }
        return hours + ":" + minutes + " " + modifier;
    },
    getHourDifference: function(timeString1, timeString2){
        let dateString1 = "01 Jan 1970 " + timeString1 + ":00 GMT";
        let dateString2 = "01 Jan 1970 " + timeString2 + ":00 GMT";
        let date1 = Date.parse(dateString1);
        let date2 = Date.parse(dateString2);
        let diffMills = Math.abs(date1 - date2);
        return Math.round(diffMills / (1000 * 60 * 60))
    },
    getFormattedUri: function(stri){
        return stri.split(/[\s,]+/).join("+");
    },
    getUriToSend: function(address){
        let streetUri = "street=" + api.getFormattedUri(address.street);
        let cityUri = "&city=" + api.getFormattedUri(address.city);
        let stateUri = "&state=" + api.getFormattedUri(address.state);
        let zipcodeUri = "&zip=" + address.zipcode;
        let uriToSend = GEOCODINGAPIURI + streetUri + cityUri + stateUri + zipcodeUri + "&benchmark=9&format=jsonp";
        console.log("Uri: ", uriToSend);
        return uriToSend;
    },
    // eslint-disable-next-line
    checkAddress: function(addr){
        // eslint-disable-next-line
        let responses = Object.keys(addr).map(function(key) {
            let val = addr[key];
            //console.log("Address val: ", val);
            // eslint-disable-next-line
            if (val || val.trim().length !== 0) {
                switch (key) {
                    case "zipcode":
                        if (/^[0-9]+$/.test(String(val))) {
                            return { status: true, message: "" };
                        } else {
                            return { status: false, message: "Zipcode has some letters in it!!" };
                        } 
                        break;
                    // eslint-disable-next-line    
                    case "city":
                        // eslint-disable-next-line
                    case "country":
                        // eslint-disable-next-line
                    case "state":
                        let strTest = val.split(/[\s|,]/).filter(item => item !== "").every(item => /[a-zA-Z]+$/.test(item));
                        if (strTest) {
                            return { status: true, message: "" };
                        } else {
                            return { status: false, message: "City, country, state has some numbers in them!!" };
                        }
                        break;
                    // eslint-disable-next-line
                    case "street":
                        if (/[\s,]+/.test(String(val).toLowerCase())) {
                            return { status: true, message: "" };
                        } else {
                            return { status: false, message: "Address must contain spaces and/or commas" };
                        }
                        break;
                    // eslint-disable-next-line
                    default:
                        break;
                }
            }
        });
        if (responses.some(item => item.status === false)) {
            //console.log("Found an error");
            return false;
        }
        return true;
    }, 
    getGoogleRequestUrl: function(address) {
        let dests = address.street.split(",").join("").split(" ").join("+") + "+" + address.city.split(" ").join("+") +
            "+" + address.state + "+" + address.zipcode;
        let destUrl = "&destinations=" + dests;
        let requestUrl = GOOGLEDISTANCEMATRIXURI + ORIGINURL + destUrl + "&key=" + GOOGLEMAPSAPIKEY;
        var corsFinalUrl = CORSPROXYURI + requestUrl;
        return corsFinalUrl;
    }
}

export default api;