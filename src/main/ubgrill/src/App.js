import React, { Component } from "react";
import { Switch, Route } from "react-router";
import { BrowserRouter, Redirect } from "react-router-dom";
import LandingPage from "./components/LandingPage.js";
import AuthPage from "./components/auth/AuthPage.js";
import ReservationPage from "./containers/reservation/ReservationPage.js";
import Store from "./containers/store/Store.js";
import AccountPage from "./containers/account/AccountPage.js";
import CheckoutPage from "./containers/checkout/CheckoutPage.js";
import fontawesome from "@fortawesome/fontawesome";
import faCheckCircle from "@fortawesome/fontawesome-free-solid/faCheckCircle";
import faTimesCircle from "@fortawesome/fontawesome-free-solid/faTimesCircle";
import faInfoCircle from "@fortawesome/fontawesome-free-solid/faInfoCircle";
import faCalendarAlt from "@fortawesome/fontawesome-free-solid/faCalendarAlt";
import faThList from "@fortawesome/fontawesome-free-solid/faThList";
import faCreditCard from "@fortawesome/fontawesome-free-solid/faCreditCard";
import faUser from "@fortawesome/fontawesome-free-solid/faUser";
import faPlusCircle from "@fortawesome/fontawesome-free-solid/faPlusCircle";
//import logo from "./logo.svg";
import "./App.css";
//import api from "./services/api.js";

fontawesome.library.add(faCheckCircle, faTimesCircle, faInfoCircle, faCalendarAlt, faThList, faCreditCard, faUser, faPlusCircle);

function AuthenticatedRoute({ component: Component, loggedStatus, ...rest }) {
  return (
    <Route {...rest}
      render={({ history, match, props }) => 
        loggedStatus ? (
          <Component {...rest} history={history} match={match} />
        ) : (
          <Redirect to="/login" />    
        )              
      }
    />
  );
}

class App extends Component {
  constructor(){
    super();
    this.state = {
      isLoggedIn: false,
      username: " ",
      token: " ",
      isAtLoginPage: false
    }
    this.setLoggedStatus = this.setLoggedStatus.bind(this);
    this.setAuthToken = this.setAuthToken.bind(this);
    this.removeAuthToken = this.removeAuthToken.bind(this);
    this.setLeftLanding = this.setLeftLanding.bind(this);
  }
  
  setLoggedStatus(isLoggedIn){
    this.setState({ isLoggedIn });
  }
  
  setAuthToken(token, user){
    this.setState({ token: token, username: user }, function(){
      this.setLoggedStatus(true); 
    });
  }
  
  setLeftLanding(){
    this.setState({ isAtLoginPage: true });
  }
  
  removeAuthToken(){
    this.setState({ token: " ", username: " " }, function(){
      this.setLoggedStatus(false);
    });
  }
    
  render() {
    return (
      <BrowserRouter>
        <Switch>
         <Route exact path="/" render={() => (<LandingPage onClick={this.setLeftLanding} leftLanding={this.state.isAtLoginPage} />)} />
         <Route exact path="/login" render={() => (<AuthPage setToken={this.setAuthToken} loggedStatus={this.state.isLoggedIn} />)} />
         <Route exact path="/register" render={() => (<AuthPage setToken={this.setAuthToken} loggedStatus={this.state.isLoggedIn} />)} />
         <AuthenticatedRoute component={ReservationPage} 
            loggedStatus={this.state.isLoggedIn}
            path="/reservations"
            myToken={this.state.token}
            myName={this.state.username}
            removeToken={this.removeAuthToken}
         />
         <AuthenticatedRoute component={Store} 
            loggedStatus={this.state.isLoggedIn}
            path="/store"
            myToken={this.state.token}
            myName={this.state.username}
            removeToken={this.removeAuthToken}
         />
         <AuthenticatedRoute component={CheckoutPage} 
            loggedStatus={this.state.isLoggedIn}
            path="/checkout"
            myToken={this.state.token}
            myName={this.state.username}
            removeToken={this.removeAuthToken}
         />
         <AuthenticatedRoute component={AccountPage} 
            loggedStatus={this.state.isLoggedIn}
            path="/me"
            myToken={this.state.token}
            myName={this.state.username}
            removeToken={this.removeAuthToken}
         />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
