import React from "react";
import ReservationTimes from "../../containers/reservation/ReservationTimes.js";
import "../../styles/reservation.css";

const EditReservation = props => {
  return (
    <div className="reservation-details-container">
      <div className="selected-reservation-day">
        <div>{props.initialDate}</div>
      </div>
      <ReservationTimes setTimes={props.setTimes} setCanNotSubmit={props.setCanNotSubmit} />
    </div>
  );
}

export default EditReservation;