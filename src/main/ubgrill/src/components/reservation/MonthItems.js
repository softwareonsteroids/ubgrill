import React from "react";
import SelectionItem from "../../containers/reservation/SelectionItem.js";
import "../../styles/appshell.css";

const MonthItems = props => {
  return (
    <div className="actual-item-container">
      {props.child.map((link, i) => (
        <SelectionItem key={i} name={link} onClick={props.onClick} 
          iparams={props.currentMth} />
      ))};
    </div>
  );
}

export default MonthItems;
