import React from "react";
import { Redirect } from "react-router-dom";
import "../styles/shell.css";


const LandingPage = props => {
  if (props.leftLanding === false){
    return (
      <div className="outerContainer landingPage">
        <div className="landingPage-maincont">
          <h1 className="landingPage-h1"> Ubgrill </h1>
          <p><i> A fast, easy way to enjoy grilling </i></p>
          <button type="button" onClick={props.onClick} className="action-button"> START </button>
        </div>
      </div>
    );
  } else {
    return <Redirect to="/login" />;    
  }
}

export default LandingPage;