import React from "react";
import OrderItemColumns from "../../containers/account/OrderItemColumns.js";
import OrderDetails from "./OrderDetails.js";
import { BrowserRouter } from "react-router-dom";
import { Switch, Route } from "react-router";
import "../../styles/account.css";

const OrderTable = props => {
  let typeOfHeader = props.typeOfHeader;
  return (
    <BrowserRouter>
      <div className="account-page-ordertablecontent">
        <OrderItemColumns
          orders={props.routes}
          onClick={props.cancelOrder}
          typeOfHeader={props.typeOfHeader}
        />
        <div className="account-page-ordertablecontent-content">
          <Switch>
            {props.routes.map(item => (
              <Route
                path={item.path}
                exact={item.exact}
                key={item.path}
                render={props => (
                  <OrderDetails
                    path={item.path}
                    deliveryTime={item.details.deliveryTime}
                    deliveryDate={item.details.deliveryDate}
                    deliveryDistance={item.details.deliveryDistance}
                    details={item.details.itemDetails}
                    total={item.details.total}
                    orderId={item.details.orderId}
                    reservationId={item.details.reservationId}
                    typeOfHeader={typeOfHeader}
                    isDelivered={item.details.isDelivered}
                  />
                )}
              />
            ))}
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
};

export default OrderTable; 
