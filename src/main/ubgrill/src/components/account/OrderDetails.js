import React from "react";
import "../../styles/appshell.css";
import "../../styles/account.css";

const OrderDetails = props => {
  console.log("Details in OrderDetails: ", props);
  if (props.path !== "/me") {
    let details = props.details.map((item, i) => (
      <div key={i} style={{ flex: "1", marginRight: "3%" }}>
        {Object.keys(item).map((key, j) => (
          <p key={j}>
            {key}:{item[key]}
          </p>
        ))}
        <hr />
      </div>
    ));

    return (
      <div className="innerappcontainer order-details">
        <div className="order-details-innerview">
          <p>Estimated Delivery Time: {props.deliveryTime} </p>
          <p>Date of Delivery: {props.deliveryDate} </p>
          <p>Delivered: {props.isDelivered} </p>
          <p>Distance: {props.deliveryDistance} </p>
          <div>
            OrderDetails:
            <div style={{ display: "flex", flexFlow: "row wrap" }}>
              {details}
            </div>
          </div>
          <p>Total Amount: {props.total}</p>
        </div>
      </div>
    );
  } else {
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          display: "flex",
          justifyContent: "center"
        }}
      >
        <p>
          Click the links on the left hand side to see items details. If there
          is nothing in the left hand side, you have no current processing
          orders.
        </p>
      </div>
    );
  }
};

export default OrderDetails;
 
