import React from "react";
import ErrMessages from "../../components/extras/ErrMessages.js";
import "../../styles/account.css";
import api from "../../services/api.js";

const SetStatusContainer = props => {
  let errorMsgs =
      api.isObjectEmpty(props.errors) === false ? (
        <ErrMessages mapToUnpack={props.errors} cssClassName={"account-page-accntcontrol-status-err"} />
      ) : null;   
    
  return (
    <div className="account-page-accntcontrol-status">
      {errorMsgs}
      <button type="button" className="account-page-accntcontrol-status-btn"
        onClick={props.saveChanges}> 
        Save Changes
      </button>
    </div>
  );
};

export default SetStatusContainer; 
