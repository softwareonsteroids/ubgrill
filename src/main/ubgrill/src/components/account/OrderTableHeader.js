import React from "react";
import "../../styles/account.css";

const OrderTableHeader = props => {
  let secondColumn =
    props.typeOfHeader === "previous" ? "Date Delivered" : "Cancel Order";
  return (
    <div className="account-page-ordertableheader">
      <div className="account-page-ordertableheader-tablecell">
        <u>Order ID</u>
      </div>
      <div className="account-page-ordertableheader-tablecell">
        <u>{secondColumn}</u>
      </div>
      <div className="account-page-ordertableheader-tablecell-other">
        <u>Order Details</u>
      </div>
    </div>
  );
};

export default OrderTableHeader; 
