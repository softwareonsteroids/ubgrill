import React from "react";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import "../../styles/appshell.css";
import "../../styles/store.css";
import "../../styles/checkout.css";

const CmdBtns = props => {
  if (props.page.toLowerCase() === "reservation"){  
    return(
      <div className="subpage-cmd-box">
        <button className={props.canSubmit === false ? "submit-btn forward disabled" : "submit-btn forward"}
          type="button" onClick={props.firstLink} 
          disabled={props.canSubmit === false}>
          <FontAwesomeIcon icon="th-list" size="2x" />
          <div>STORE</div>
        </button>
      </div>
    );
  } else if (props.page.toLowerCase() === "store"){
    if (props.subpage !== null && props.subpage.toLowerCase() === "page"){
      return(
        <div className="subpage-cmd-box only">
          <button className="submit-btn backward" type="button" onClick={props.firstLink}>
            <div className="back">BACK</div>
          </button>
        </div>
      );
    } else {
      return(
        <div className="subpage-cmd-box">
          <button className="submit-btn backward" type="button" 
           onClick={props.firstLink}>
            <FontAwesomeIcon icon="calendar-alt" size="2x" />
            <div>RESERVE</div>
          </button>
          <div className="checkout-basket">
            <div className="checkout-basket-img" />
            <div className="checkout-basket-status">
              {props.orderLength}
            </div>
          </div>
          <button className={props.canSubmit === false ? "submit-btn forward disabled" : "submit-btn forward"} 
            type="button" onClick={props.secondLink}
            disabled={props.canSubmit === false}>
            <FontAwesomeIcon icon="credit-card" size="2x" />
            <div>CHECKOUT</div>
          </button>
        </div>
      );
    }  
  } else if (props.page.toLowerCase() === "checkout"){
    return(
        <div className="checkout-page-action-bar">
          <button type="button" className="checkout-page-main-action-btn"
            onClick={props.firstLink} disabled={props.canSubmit === false}>Place Final Order</button>
        </div>

    );  
  }
}

export default CmdBtns;
