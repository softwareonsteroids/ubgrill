import React from "react";
import "../../styles/appshell.css";

const AppHeaderDropDownItem = ({ toGoto, title }) => {
  return (
    <div className="drop-down-list-item" onClick={toGoto}>
      <p>{title}</p>
    </div>
  );
}

export default AppHeaderDropDownItem;
