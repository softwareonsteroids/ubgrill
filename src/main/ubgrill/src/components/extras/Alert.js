import React from "react";
import Modal from "./Modal.js";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

export const OKAlert = props => {
  return (
    <Modal show={props.show} handleClose={props.onClick}
      buttonMessage={props.title}>
      <FontAwesomeIcon icon="check-circle" size="5x" style={{ color: "green" }}/>
      <div>{props.message}</div>
    </Modal>
  );
}

export const ErrorAlert = props => {
  return (
    <Modal show={props.show} handleClose={props.onClick}
      buttonMessage={props.title}>
      <FontAwesomeIcon icon="times-circle" size="5x" style={{ color: "red" }}/>
      <div>{props.message}</div>
    </Modal>
  );
}

export const InfoAlert = props => {
  return (
    <Modal show={props.show} handleClose={props.onClick}
      buttonMessage={props.title}>
      <FontAwesomeIcon icon="info-circle" size="5x"/>
      <div>{props.message}</div>
    </Modal>
  );
}