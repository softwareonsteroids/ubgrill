import React from "react";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import "../../styles/appshell.css";

const AppContainer = props => {
  let appheader = 
    props.title.toLowerCase() === "reservation" ? (
      <figure className="main-app-container-header">
        <FontAwesomeIcon icon="calendar-alt" size="2x"/> 
        <figcaption>{props.title}</figcaption>
      </figure>
    ): props.title.toLowerCase() === "store" ? (
      <figure className="main-app-container-header">
        <FontAwesomeIcon icon="th-list" size="2x" /> 
        <figcaption>{props.title}</figcaption>
      </figure>  
    ): props.title.toLowerCase() === "order" ? (
      <figure className="main-app-container-header">
        <FontAwesomeIcon icon="credit-card" size="2x"/> 
        <figcaption>{props.title}</figcaption>
      </figure>  
    ): (
      <figure className="main-app-container-header">
        <FontAwesomeIcon icon="user" size="2x" /> 
        <figcaption>{props.title}</figcaption>
      </figure>  
    );
    
  return (
    <div className="app-container">
      <div className="main-app-container">
        {appheader}
        <div className="inner-app-container">
          <main>
            <div className="app-content-container">{props.children}</div>
          </main>
        </div>
      </div>
    </div>  
  );
}

export default AppContainer;
