import React from "react";
import "../../styles/shell.css";

const Modal = ({ handleClose, show, children, buttonMessage }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";
  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        <div className="modal-main-inner">
          {children}
          <button className="modal-btn" onClick={handleClose}>{buttonMessage}</button>
        </div>
      </section>
    </div>
  );
}

export default Modal;