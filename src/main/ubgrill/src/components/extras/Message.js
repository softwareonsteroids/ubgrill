import React from "react";
const Message = ({name, error}) => {
  return (
    <p style={{ color: "red" }}>
      <small>
        {name}:{error}
      </small>
    </p>
  );
}

export default Message;
