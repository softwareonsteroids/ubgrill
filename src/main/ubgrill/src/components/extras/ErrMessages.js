import React from "react";
import Message from "./Message.js";

const ErrMessages = props => {
  let errList = Object.entries(props.mapToUnpack).map(
    ([key, value], i) => {
      return <Message name={key} key={i} error={value.toString()} />;
    });
  return (
    <div className={props.cssClassName}>{errList}</div>     
  );
}

export default ErrMessages;
