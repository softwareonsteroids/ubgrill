import React from "react";
import "../../styles/appshell.css";

const CarouselItem = props => {
  return (
    <li className={props.index === props.activeIndex ? "item is_ref": "item"}>{props.child}</li>    
  );
}

export default CarouselItem;
