import React from "react";
import "../../styles/appshell.css";

const Arrow = ({ direction, onClick }) => {
  if (direction === "left"){
    return (
      <a href="" onClick={onClick} className="arrow">
        <div>&#9664;</div>
      </a>
    );
  } else if (direction === "right"){
    return (
      <a href="" onClick={onClick} className="arrow">
        <div>&#9654;</div>
      </a>
    );  
  } else if (direction === "up"){
    return (
      <a href="" onClick={onClick} className="arrow vertical">
        <div>&#9650;</div>
      </a>
    );   
  } else if (direction === "down"){
    return (
      <a href="" onClick={onClick} className="arrow vertical">
        <div>&#9660;</div>
      </a>
    );   
  }
}

export default Arrow;