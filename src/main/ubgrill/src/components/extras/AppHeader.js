import React from "react";
import AppHeaderDropDown from "../../containers/extras/AppHeaderDropDown.js";
import "../../styles/appshell.css";

const AppHeader = props => {
  return (
    <header className="app-header">
      <figure className="app-logo">
        <div />
        <figcaption>Ubgrill</figcaption>
      </figure>
      <AppHeaderDropDown firstLink={props.firstLink}
        secondLink={props.secondLink} thirdLink={props.thirdLink}
        dropDownType={props.dropDownType}
      />
    </header>
  );
}

export default AppHeader;