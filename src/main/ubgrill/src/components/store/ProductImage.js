import React from "react";
const ProductImage = props => {
  if (props.typeOfImage.toLowerCase() === "other") {
    return (
      <div
        style={{
          backgroundImage: "url(" + props.image + ")",
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          height: "100%",
          width: "100%"
        }}
      />
    );
  } else {
    return (
      <div
        style={{
          backgroundImage: "url(" + props.image + ")",
          backgroundSize: "contain",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          height: "100%",
          width: "100%"
        }}
      />
    );
  }
}; 

export default ProductImage;