import React from "react";
import AddButton from "../../containers/store/AddButton.js";
import ProductImage from "./ProductImage.js";
import "../../styles/store.css";

const ProductDetailsInfo = props => {
  return (
    <div style={{ width: "100%", height: "40%" }}>
      <h4 style={{ marginBottom: "0" }}>
        <u>{props.details.title}</u>
      </h4>
      <div style={{ margin: "0" }}>{props.details.content}</div>
    </div>
  );
};

const ProductDetails = props => {
  if (props.image !== null) {
    return (
      <div className="product-details-box-extra">
        <div style={{ width: "100%", height: "50%" }}>
          <ProductImage image={props.image} typeOfImage={props.details.typeOfImage} />
        </div>
        <AddButton onClick={props.addOnClick} name={props.image} />
        <ProductDetailsInfo details={props.details} />
       </div>
    );
  } else {
    return (
      <div className="product-details-initial-box">
        <p className="product-details-initial-box-info">
          Click on items on the left hand side to continue
        </p>
      </div>
    );
  }    
}

export default ProductDetails;
