import React from "react";
import { Link } from "react-router-dom";
import api from "../../services/api.js";
import "../../styles/store.css";

const InventoryLink = props => {
  let titler = props.item.split("/")[3];
  let name = api.titleCase(titler);
  let classname = `${titler}-img`;
  return (
    <Link to={props.item} className="inventory-link">
      <div className={classname}><p>{name}</p></div>
    </Link>
  );
}

const StoreInventoryLinks = props => {
  let modifiedLinks = props.links.map(item => {
    return `/store/inventory/${item.toLowerCase()}`;
  });
  return (
    <div className="inventory-links">
      {modifiedLinks.map((item, i) => <InventoryLink item={item} key={i} />)}
    </div>
  );
}

export default StoreInventoryLinks;
