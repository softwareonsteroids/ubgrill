import React from "react";
import { NavLink } from "react-router-dom";
import ProductImage from "./ProductImage.js";
import "../../styles/appshell.css";
import "../../styles/store.css";

const ProductItems = props => {
  return(
    <div className="actual-item-container-vertical">
      {props.child.map((item, i) => (
        <NavLink to={item.path} className="product-item"
          activeClassName="product-active" key={i}>
          <ProductImage image={item.productPicLoc} typeOfImage={item.productType} />
        </NavLink>
      ))}
    </div>
  );
}

export default ProductItems;
