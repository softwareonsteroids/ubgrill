import React from "react";
import StoreInventoryLinks from "./StoreInventoryLinks.js";
import CmdBtns from "../extras/CmdBtns.js";
import "../../styles/store.css";

const InitialStorePage = props => {
  return (
    <div className="main-store-page">
      <StoreInventoryLinks links={props.links} />
      <CmdBtns page={"store"} subpage={"main"} firstLink={props.goBack}
        secondLink={props.submitOrder} canSubmit={props.canCheckout}
        orderLength={props.orderLength}
      />
    </div>
  );
}

export default InitialStorePage;