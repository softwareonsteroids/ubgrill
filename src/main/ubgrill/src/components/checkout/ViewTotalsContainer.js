import React from "react";

const ViewTotalsContainer = props => {
  return (
    <div style={{ width:"100%", height: "100%"}}>
        <div style={{ display: "flex", justifyContent: "space-between", width: "90%" }}>
        <div> ItemsTotal : </div>
        <div>$ {props.itemsAmount}</div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between", width: "90%" }}>
        <div> Delivery Fees : </div>
        <div><u>$ {props.deliveryFee}</u></div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between", width: "90%", marginTop: "2%" }}>
        <div> <b>Final Amount : </b> </div>
        <div> <b>$ {props.totalAmount}</b> </div>
        </div>
    </div>
  );
}

export default ViewTotalsContainer; 
