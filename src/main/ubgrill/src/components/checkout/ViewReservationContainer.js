import React, { Component } from "react";
import "../../styles/checkout.css";

const ViewReservationContainer = props => {
  return (
    <div className="checkout-page-viewreservationcontainer">
      <div> Date: {props.reservation.dateReserved} </div>
      <div>
        Times: {props.reservation.timeReservedStart} to {" "}
        {props.reservation.timeReservedEnd}
      </div>
    </div>
  );
};

export default ViewReservationContainer; 
