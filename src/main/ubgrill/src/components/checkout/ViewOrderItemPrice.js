import React from "react";

const ViewOrderItemPrice = props => {
  if (props.productType.toLowerCase() === "other" || props.productType.toLowerCase() === "drinks") {
    return <span>{props.price}</span>;
  } else {
    return <span>{props.price}/hr</span>;
  }
};

export default ViewOrderItemPrice; 
