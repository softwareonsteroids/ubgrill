import React from "react";
import api from "../../services/api.js";
import Message from "../extras/Message.js";
import "../../styles/checkout.css";

const ActionButton = props => {
  return (
    <div className="checkout-action-button" onClick={props.onClick}>
      {props.actionName}
    </div>
  );
};

const StatusBar = props => {
  let errorStatus =
    api.isObjectEmpty(props.error) === false ? (
      <Message
        name={props.error.name}
        error={props.error.message}
      />
    ) : null;

  let stats = `${props.success}`;

  let successStatus =
    stats.length > 0 ? (
      <p style={{ color: "green" }}>
        <small>{props.success}</small>
      </p>
    ) : null;

  return (
    <div className="status-bar">
      <div className="status-bar-message">
        {errorStatus}
        {successStatus}
      </div>
      <ActionButton onClick={props.onClick} actionName={props.actionName} />
    </div>
  );
};

export default StatusBar; 
