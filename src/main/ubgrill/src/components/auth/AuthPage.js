import React from "react";
import { Route } from "react-router";
import { Redirect } from "react-router-dom"; 
import SignInForm from "../../containers/auth/SignInForm.js";
import SignUpForm from "../../containers/auth/SignUpForm.js";
import AuthPageFormLayout from "./AuthPageFormLayout.js";
import "../../styles/auth.css";

const AuthPage = props => {
  if (props.loggedStatus === false) {
    return (
      <div className="outerContainer mainBackground">
        <div className="mainContainer">
          <figure className="bannerImage">
            <div className="logo" />
            <figcaption>Ubgrill</figcaption>
          </figure>
          <AuthPageFormLayout>
            <Route exact path="/login" render={() => <SignInForm setToken={props.setToken}/> } />
            <Route exact path="/register" render={() => <SignUpForm setToken={props.setToken}/> } />
          </AuthPageFormLayout>
        </div>
      </div>
    );
  } else {
    return <Redirect to="/reservations" />;   
  }
}

export default AuthPage;