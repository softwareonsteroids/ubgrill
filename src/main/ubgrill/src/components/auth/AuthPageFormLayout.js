import React from "react";
import { NavLink } from "react-router-dom";
import "../../styles/auth.css";

const AuthPageFormLayout = ({ children }) => {
  return (
    <div className="form-container">
      <header>
        <div>
          <p>
            <NavLink to="/login" style={{ textDecoration: "none", color: "black" }} activeStyle={{ textDecoration: "underline" }}>Sign In</NavLink>
          </p>
        </div>
        <div>
          <p>
            <NavLink to="/register" style={{ textDecoration: "none", color: "black" }} activeStyle={{ textDecoration: "underline" }}>Sign Up</NavLink>
          </p>
        </div>
      </header>
      {children}
    </div>
  );
}

export default AuthPageFormLayout;
