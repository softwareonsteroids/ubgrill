SET FOREIGN_KEY_CHECKS = 0;
drop table if exists client_role;
drop table if exists lineitem;
drop table if exists clientorderaddress;
drop table if exists invoice;
drop table if exists clientorder;
drop table if exists reservation;
drop table if exists client;
drop table if exists address;
drop table if exists role;
drop table if exists product;
SET FOREIGN_KEY_CHECKS = 1;

create table product (
    product_id integer not null AUTO_INCREMENT,
    serial_id binary(16),
    price DECIMAL(6, 2) not null,
    product_quantity integer not null,
    product_range varchar(255) not null,
    product_type varchar(255) not null,
    product_colour varchar(255),
    product_model varchar(255),
    product_pic_loc varchar(255),
    primary key(product_id)
);


create table client (
    client_id integer not null AUTO_INCREMENT,
    email varchar(255) not null,
    username varchar(40) not null unique,
    password varchar(255) not null,
    firstname varchar(255) not null,
    lastname varchar(255) not null,
    enabled boolean not null,
    last_password_reset_date datetime not null,
    primary key (client_id)
);

create table role (
    role_id integer not null AUTO_INCREMENT,
    rolename varchar(255) not null,
    primary key (role_id)
);

create table client_role (
    role_id integer not null,
    client_id integer not null,
    primary key (client_id, role_id),
    KEY fk_clientrole_role (role_id),
    CONSTRAINT fk_clientrole_client FOREIGN KEY (client_id) REFERENCES client (client_id),
    CONSTRAINT fk_clientrole_role FOREIGN KEY (role_id) REFERENCES role (role_id)
);

-- One client can have many reservations (one to many on the client side)
-- One reservation belong to one client ( many to one on the reservation side)
create table reservation (
    reservation_id integer not null AUTO_INCREMENT,
    client_id integer not null,
    date_reserved date not null,
    time_reserved_start time not null,
    time_reserved_end time not null,
    primary key(reservation_id),
    KEY fk_reservation (reservation_id),
    CONSTRAINT fk_reservation_client FOREIGN KEY (client_id) REFERENCES client (client_id)
);

create table address (
    address_id integer not null AUTO_INCREMENT,
    street varchar(255) not null,
    city varchar(50) not null,
    zipcode varchar(50) not null,
    state varchar (50) not null,
    country varchar (50) not null,
    primary key(address_id)    
);


-- if you do not put an AUTO_INCREMENT you get an SQL Exception,
-- field does not have default value
create table clientorder (
    clientorder_id integer not null AUTO_INCREMENT,
    client_id integer not null,
    reservation_id integer not null,
    created_on date not null,
    primary key(clientorder_id),
    KEY fk_clientorder_order (clientorder_id),
    CONSTRAINT fk_clientorder_client FOREIGN KEY (client_id) REFERENCES client(client_id),
    CONSTRAINT fk_clientorder_reservation FOREIGN KEY (reservation_id) REFERENCES reservation(reservation_id)
);

-- one product can be on many orders
-- one order can contain many products 
create table lineitem(
    clientorder_id integer not null,
    product_id integer not null,
    product_quantity integer not null,
    is_fufilled boolean not null,
    primary key(clientorder_id, product_id),
    CONSTRAINT fk_lineitem_product FOREIGN KEY (product_id) REFERENCES product(product_id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_lineitem_clientorder FOREIGN KEY (clientorder_id) REFERENCES clientorder(clientorder_id) ON DELETE CASCADE ON UPDATE CASCADE
);

create table clientorderaddress(
    clientorder_id integer not null,
    address_id integer not null,
    address_type varchar(50) not null,
    primary key(clientorder_id, address_id),
    CONSTRAINT fk_clientorderaddress_order FOREIGN KEY (clientorder_id) REFERENCES clientorder(clientorder_id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_clientorderaddress_address FOREIGN KEY (address_id) REFERENCES address(address_id) ON DELETE CASCADE ON UPDATE CASCADE
);

create table invoice (
    invoice_id integer not null AUTO_INCREMENT,
    clientorder_id integer not null,
    total_cost DECIMAL(9, 2) not null,
    created_on datetime not null,
    updated_on datetime,
    has_payed boolean,
    primary key(invoice_id),
    KEY fk_invoice (invoice_id),
    CONSTRAINT fk_invoice_clientorder FOREIGN KEY (clientorder_id) REFERENCES clientorder(clientorder_id) ON DELETE CASCADE ON UPDATE CASCADE
);
