INSERT INTO product (product_id, product_quantity, product_type, price, product_range, product_model, product_colour, product_pic_loc) VALUES
(1001, 7, 'GRILL', 3.95, 'CHEAP', 'Small Stainless Steel Grill', 'GREY', null),
(1002, 9, 'GRILL', 5.58, 'CHEAP', 'Small Propane Gas Grill', 'GREEN', null),
(1003, 8, 'GRILL', 6.45, 'EXPENSIVE', 'Big Propane Gas Grill', 'BROWN', null),
(1004, 10, 'GRILL', 9.25, 'CHEAP', 'Small Char-Boil Liquid Propane Gas Grill', 'RED', null),
(1005, 5, 'GRILL', 10.56, 'EXPENSIVE', 'Big Propane Gas Grill', 'BLACK', null),
(1006, 12,'WINE', 5.02, 'CHEAP', 'FontanaFredda Briccotondo Piemonte Barbera', 'WHITE', null),
(1007, 6, 'WINE', 40.29, 'EXPENSIVE', 'Cabernet Sauvignon', 'RED', null),
(1008, 19,'WINE', 4.02, 'CHEAP', 'Maculan Pino & Toi Veneto', 'WHITE', null),
(1009, 8, 'WINE', 45.00, 'EXPENSIVE', 'Syrah wine', 'RED', null),
(1010, 48, 'CHAIR', 6.89, 'CHEAP', 'Plastic', 'WHITE', null),
(1011, 20, 'CHAIR', 13.00, 'EXPENSIVE', 'Steel', 'BLACK', null),
(1012, 48, 'CHAIR', 3.17, 'CHEAP', 'Steel', 'WHITE', null),
(1013, 12, 'CHAIR', 16.32, 'EXPENSIVE', 'Plastic', 'BLACK', null),
(1014, 9, 'TURF', 13.00, 'NA', 'Large', 'GREEN', null);


-- the password for the admin account is admin
-- the password for the test account is test
-- the password for the jeff09 account is jeffGid18

INSERT INTO client(client_id, email, username, password, firstname, lastname, enabled, last_password_reset_date) VALUES
(10, 'admin@ubgrill.com', 'admin', '$2a$10$0w1gRS50SSErIEG5y8W/.eu8Rd39Wbq85h21pHnnllD4upBjNQeiK', 'ADMIN', 'UBGRILL', true, STR_TO_DATE('15/5/2016 8:06:26 AM', '%d/%m/%Y %r')),
(11, 'test@ubgrill.com', 'test', '$2a$10$tKXLkJbkizRIf3IqjGhTLeTtf1H0/jVqXcRT4ox0OM3zTQFBymP5a', 'TEST', 'UBGRILL', true, STR_TO_DATE('13/4/2017 8:06:26 AM', '%d/%m/%Y %r')),
(12, 'jeff09@ubgrill.com', 'jeff09', '$2a$10$El6c6Wxtsxygy4xF5zJm2OxOHwD1qlUpJoQc.qxDbNIW4lkVqNVGe', 'Jefferey', 'McGrill', true, STR_TO_DATE('14/1/2018 1:30:56 PM', '%d/%m/%Y %r'));

INSERT INTO role(role_id, rolename) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

INSERT INTO client_role(role_id, client_id) VALUES
(1, 10),
(1, 11),
(2, 12);

INSERT INTO reservation(reservation_id, client_id, date_reserved, time_reserved_start, time_reserved_end) VALUES
(20, 12, STR_TO_DATE('18/1/2018', '%d/%m/%Y'), CONVERT('10:00:00', TIME), CONVERT('14:00:00', TIME)),
(21, 12, STR_TO_DATE('24/1/2018', '%d/%m/%Y'), CONVERT('14:00:00', TIME), CONVERT('15:15:00', TIME)),
(22, 12, STR_TO_DATE('30/1/2018', '%d/%m/%Y'), CONVERT('15:00:00', TIME), CONVERT('19:00:00', TIME));

INSERT INTO clientorder(clientorder_id, client_id, reservation_id, created_on) VALUES 
(40, 12, 20, STR_TO_DATE('16/1/2018 8:36:26 AM', '%d/%m/%Y %r')),
(41, 12, 21, STR_TO_DATE('20/1/2018 1:30:56 PM', '%d/%m/%Y %r')),
(42, 12, 22, STR_TO_DATE('25/1/2018 10:30:56 AM', '%d/%m/%Y %r'));

INSERT INTO lineitem(clientorder_id , product_id, product_quantity, is_fufilled) VALUES 
(40, 1002, 5, 0),
(40, 1008, 9, 0),
(41, 1008, 2, 0),
(41, 1004, 4, 0),
(42, 1004, 3, 0),
(42, 1007, 4, 0),
(42, 1011, 12, 0),
(42, 1014, 1, 0);

INSERT INTO address(address_id, street, city, zipcode, state, country) VALUES 
(90, '1812 16th St SE Apt 4','Saint Cloud', '56304', 'MN', 'USA'),
(91, '720 4th Ave S', 'Saint Cloud', '56301', 'MN', 'USA'),
(92, '500 Oracle Parkway','Redwood Shores', '94065', 'CA', 'USA');

INSERT INTO clientorderaddress(clientorder_id, address_id, address_type) VALUES 
(40, 90, 'BILLINGANDSHIPPING'),
(41, 90, 'BILLING'),
(41, 91, 'SHIPPING'),
(42, 92, 'BILLINGANDSHIPPING');

INSERT INTO invoice(invoice_id, clientorder_id, total_cost, created_on, has_payed) VALUES 
(300, 40, 64.08, STR_TO_DATE('13/4/2017 10:10:26 AM', '%d/%m/%Y %r'), 0),
(301, 41, 45.04, STR_TO_DATE('14/1/2018 2:10:26 PM', '%d/%m/%Y %r'), 1);

