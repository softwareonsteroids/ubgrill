package com.davide.Ubgrill.error;

import java.util.Arrays;
import java.util.List;
import java.util.Date;

public class ApiError {
 
    private Date timestamp;
    private String message;
 
    public ApiError(Date timestamp, String message) {
        super();
        this.timestamp = timestamp;
        this.message = message;
    }
    
    public Date getDate() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }
}
