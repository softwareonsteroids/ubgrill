package com.davide.Ubgrill.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;

@ControllerAdvice
@RestController
public class CustomResponseEntityErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MethodCannotBePerformedException.class)
    public final ResponseEntity<ApiError> handleMethodCannotBePerformed(MethodCannotBePerformedException ex, WebRequest request){
        ApiError apierror = new ApiError(new Date(), ex.getMessage());
        return new ResponseEntity<>(apierror, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ApiError> handleResourceNotFoundException(ResourceNotFoundException ex, WebRequest request){
        ApiError apierror = new ApiError(new Date(), ex.getMessage());
        return new ResponseEntity<>(apierror, HttpStatus.NOT_FOUND);
    }
    
    
}
