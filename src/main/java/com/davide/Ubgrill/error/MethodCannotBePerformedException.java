package com.davide.Ubgrill.error;

public final class MethodCannotBePerformedException extends RuntimeException {

    public MethodCannotBePerformedException() {
        super();
    }

    public MethodCannotBePerformedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public MethodCannotBePerformedException(final String message) {
        super(message);
    }

    public MethodCannotBePerformedException(final Throwable cause) {
        super(cause);
    }

} 
