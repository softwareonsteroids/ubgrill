package com.davide.Ubgrill.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.davide.Ubgrill.model.reservation.Reservation;
import com.davide.Ubgrill.model.user.Client;

import com.davide.Ubgrill.repository.ReservationRepository;
import com.davide.Ubgrill.repository.ClientRepository;

import java.util.ArrayList;
import java.util.Date;
//import java.sql.Date;
import java.util.Map;

import java.util.Map.Entry;
 
@Service
public class ReservationService{
    
    @Autowired
    private ReservationRepository reservationrepository;
    
    @Autowired
    private ClientRepository clientrepository;
    
    public Reservation createReservation(Reservation reservation, String username){
        Client client = clientrepository.findByUsername(username);
        reservation.setClient(client);
        client.getReservations().add(reservation);
        Reservation newReservation = reservationrepository.save(reservation);
        reservationrepository.flush();
        return newReservation;
    }
    
    public ArrayList<Reservation> getAllReservations(){
        return new ArrayList(reservationrepository.findAll());
    }
    
    public ArrayList<Reservation> getAllReservationsFor(int id){
        return new ArrayList<Reservation>(reservationrepository.findByClientId(id));
    }
    
    public Reservation getReservation(int id){
        return reservationrepository.findOne(id);
    }
    
    public String updatePartialReservation(
                Reservation old, 
                Map<String, Object> updates){
        String error = "";
        for (Map.Entry<String, Object> entry: updates.entrySet()){
            switch(entry.getKey()){
                case "timeReservedStart":
                    if (entry.getValue() != null){
                        Date newTimeStart = (Date)entry.getValue();
                        old.setTimeReservedStart(newTimeStart);
                    }
                    break;
                case "timeReservedEnd":
                    if (entry.getValue() != null){
                        Date newTimeEnd = (Date)entry.getValue();
                        old.setTimeReservedEnd(newTimeEnd);
                    }
                    break;
                case "dateReserved":
                    error="This property: (" + (String)entry.getKey() + 
                        ") can not be set here. Use DELETE instead";
                    break;
                default:
                    error = "This property: (" + (String)entry.getKey() + 
                    ") is not a propery of this object";
                    break;
            }
        }
        reservationrepository.save(old);
        return error;
    }
    
    public void deleteReservation(Reservation reservation){
        reservationrepository.deleteById(reservation.getReservationId());
    }
}
