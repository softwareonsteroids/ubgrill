package com.davide.Ubgrill.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpHeaders;

import com.davide.Ubgrill.config.security.JwtAuthUtils;
import com.davide.Ubgrill.config.security.JwtAuthUser;
import com.davide.Ubgrill.config.security.JwtAuthEnum;

import javax.servlet.http.HttpServletRequest; 

@Service("authservice")
public class AuthService {
    
    @Autowired
    private UserDetailsService userDetailsService;
    
    @Value("${jwt.header}")
    private String tokenHeader;
    
    @Autowired
    private JwtAuthUtils jwtAuthUtils;
    
    public String createNewToken(String username){
        final UserDetails userDetails = 
            userDetailsService.loadUserByUsername(username);
        final String token = jwtAuthUtils.generateToken(userDetails, JwtAuthEnum.AUDIENCE);
        return token;
    }
    
    public String createRefreshToken(HttpServletRequest request){
        String token = request.getHeader(tokenHeader);
        String username = jwtAuthUtils.getUsernameFromToken(token);
        JwtAuthUser user = (JwtAuthUser) userDetailsService.loadUserByUsername(username);
        if (jwtAuthUtils.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            return jwtAuthUtils.refreshToken(token);
        } else {
            return "";
        }
    }
    
    public JwtAuthUser retrieveAuthUser(HttpServletRequest request){
        String token = request.getHeader(tokenHeader).substring(7);
        return getAuthUserFromToken(token);
    }
    
    public JwtAuthUser retrieveAuthUserFromHeaders(HttpHeaders headers){
        String token = headers.getFirst(tokenHeader).substring(7);
        return getAuthUserFromToken(token);
    }
    
    private JwtAuthUser getAuthUserFromToken(String token){
        String username = jwtAuthUtils.getUsernameFromToken(token);
        JwtAuthUser user = (JwtAuthUser) userDetailsService.loadUserByUsername(username);
        return user;
    }
}
