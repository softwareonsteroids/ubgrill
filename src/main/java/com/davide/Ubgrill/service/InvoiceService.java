package com.davide.Ubgrill.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.davide.Ubgrill.model.store.LineItem;
import com.davide.Ubgrill.model.store.Invoice;
import com.davide.Ubgrill.model.store.ClientOrder;
import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.model.wrappers.InvoiceInputWrapper;
import com.davide.Ubgrill.repository.InvoiceRepository;
import com.davide.Ubgrill.repository.LineItemRepository;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
 
@Service
public class InvoiceService{
    @Autowired
    private InvoiceRepository invoicerepository;
    
    @Autowired
    private LineItemRepository lineitemrepository;
    
    public Invoice createInvoice(ClientOrder order, InvoiceInputWrapper wrapper){
        ArrayList<LineItem> items = new ArrayList<>(lineitemrepository.findByClientOrderId(order.getId()));
        double orderCost = 0.0;
        for ( LineItem item: items){
            orderCost = orderCost + (double)(item.getProductQuantity() * item.getProduct().getPrice());
        }
        Invoice invoice = new Invoice(order, orderCost);
        invoice.setDeliveryTime(wrapper.getDeliveryTime());
        invoice.setDeliveryCost(wrapper.getDeliveryCost());
        invoice.setDeliveryMiles(wrapper.getDeliveryMiles());
        Invoice newInvoice = invoicerepository.save(invoice);
        invoicerepository.flush();
        return newInvoice;
    }
	
    public ArrayList<Invoice> getMyInvoices(int userId){
        return new ArrayList<Invoice>(invoicerepository.findByClientId(userId));
    }
    
    public ArrayList<Invoice> getAllInvoices(){
        return new ArrayList<Invoice>(invoicerepository.findAll());
    }
    
    public Invoice getInvoiceFor(int invoiceId){
        return invoicerepository.findOne(invoiceId);
    }
    
    public Invoice markInvoicePayed(Invoice invoice){
        SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
        try{
            Date dt = sf.parse(sf.format(new Date()));
            invoice.setUpdatedOn(dt);
        } catch (ParseException ex){
            ex.printStackTrace();
        }
        invoice.setHasPayed(true);
        return invoice;
        //invoicerepository.setPayed(invoice.getId());
    }
    
    public void removeInvoice(Invoice invoice){
        invoice.setOrder(null);
        invoicerepository.deleteById(invoice.getId());
    }
}
