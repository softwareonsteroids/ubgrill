package com.davide.Ubgrill.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.davide.Ubgrill.model.wrappers.ReturnPair;
import com.davide.Ubgrill.model.product.Product;

import com.davide.Ubgrill.repository.ProductRepository;

import java.util.ArrayList;
import java.util.Map;

import java.util.Map.Entry;


@Service
public class ProductService{
    
    @Autowired
    private ProductRepository productrepository;

    public int createProduct(Product product){
        Product newProduct = productrepository.save(product);
        productrepository.flush();      // to actually save the object, must be called before we can get the product id
        return newProduct.getProductId();
    }
    
    public Product getProduct(int id){
        return productrepository.findOne(id);
    }
    
    public ReturnPair<Boolean, String> isProductListValid(ArrayList<Integer> ids){
        for (Integer id: ids){
            if (this.getProduct(id) == null){
                String str = "Product id (" + id + ") is not a valid product id";
                return new ReturnPair<Boolean, String>(false, str);
            }
        }
        return new ReturnPair<Boolean, String>(true, "");
    }
    
    public ArrayList<Product> getProductsForIds(ArrayList<Integer> ids){
        ArrayList<Product> products = new ArrayList<>();
        for (Integer id: ids){
            products.add(getProduct(id));
        }
        return products;
    }
    
    public void deleteProduct(Product product){
        productrepository.delete(product);
    }
    
    public ArrayList<Product> getAllProducts(){
        return new ArrayList(productrepository.findAll());
    }
    
    public String updatePartialProduct(Product old, Map<String, Object> updates){
        String error = "";
        for (Map.Entry<String, Object> entry: updates.entrySet()){
            switch(entry.getKey()){
                case "productQuantity":
                        if (entry.getValue() != null){
                            int newQuanity = (Integer)entry.getValue();
                            old.setProductQuantity(newQuanity);
                        }
                        break;
                case "price":
                        if (entry.getValue() != null){
                            double newPrice = (Double)entry.getValue();
                            old.setPrice(newPrice);
                        }
                        break;
                case "productType":
                        if (entry.getValue() != null){
                            String newType = (String)entry.getValue();
                            old.setProductType(newType);
                        }
                        break;
                case "productName":
                        if (entry.getValue() != null){
                            String newModel = (String)entry.getValue();
                            old.setProductName(newModel);
                        }
                        break;
                 case "productQuantityPerOrder":
                        if (entry.getValue() != null){
                            int quantityPerOrder = (Integer)entry.getValue();
                            old.setProductQuantityPerOrder(quantityPerOrder);
                        }
                        break;
                case "productPicLoc":
                        if (entry.getValue() != null){
                            String newLoc = (String)entry.getValue();
                            old.setProductPicLoc(newLoc);
                        }
                        break;
                 case "productDetails":
                        if (entry.getValue() != null){
                            String newDetails = (String)entry.getValue();
                            old.setProductDetails(newDetails);
                        }
                        break;
                default:
                      error = "This property: (" + (String)entry.getKey() + ") is not a propery of this object";
                      break;
            }   
        }
        productrepository.save(old);
        return error;
    }
}