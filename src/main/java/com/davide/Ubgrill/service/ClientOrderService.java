package com.davide.Ubgrill.service;

import com.davide.Ubgrill.model.product.Product;
import com.davide.Ubgrill.model.reservation.Reservation;
import com.davide.Ubgrill.model.store.ClientOrder;
import com.davide.Ubgrill.model.store.ClientOrderAddress;
import com.davide.Ubgrill.model.store.Invoice;
import com.davide.Ubgrill.model.store.LineItem;
import com.davide.Ubgrill.model.user.Address;
import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.model.wrappers.ReturnPair;
import com.davide.Ubgrill.model.wrappers.FufillInputWrapper;
import com.davide.Ubgrill.model.wrappers.OrderInputWrapper;
import com.davide.Ubgrill.repository.AddressRepository;
import com.davide.Ubgrill.repository.ClientOrderRepository;
import com.davide.Ubgrill.repository.ClientOrderAddressRepository;
import com.davide.Ubgrill.repository.LineItemRepository;
import com.davide.Ubgrill.repository.InvoiceRepository;
import com.davide.Ubgrill.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientOrderService{
    
    @Autowired
    private ClientOrderRepository clientorderrepository;

    @Autowired
    private LineItemRepository lineitemrepository;
    
    @Autowired
    private AddressRepository addressrepository;
    
    @Autowired
    private ClientOrderAddressRepository clientorderaddressrepository;
    
    @Autowired
    private InvoiceRepository invoicerepository;
    
    @Autowired
    private ProductRepository productrepository;
    
    public ClientOrder createClientOrder(Client client, Reservation reservation){    
        ClientOrder clientorder = new ClientOrder();
        clientorder.setClient(client);
        clientorder.setReservation(reservation);
        ClientOrder newClientOrder = clientorderrepository.save(clientorder);
        clientorderrepository.flush();
        return newClientOrder;
    }
    
    public String saveLineItems(ClientOrder order, 
            ArrayList<Product> products, OrderInputWrapper wrapper){
        for (Product product: products){
            int orderedquantity = wrapper.getQuantityFor(product.getProductId());
            int originalquantity = product.getProductQuantity();
            if ( originalquantity < orderedquantity ){
                String str = "Quantity supplied (" + String.valueOf(orderedquantity) 
                    + ") is greater than inventory ("+ String.valueOf(originalquantity) + ")";
                return str; 
            }
            
            LineItem item = new LineItem(order, product);
            item.setProductQuantity(orderedquantity);
            order.addLineItem(item);
            product.addLineItem(item);
            lineitemrepository.save(item);
        }
        return "";
    }

    public ArrayList<ClientOrder> getMyOrders(int clientId){
        return new ArrayList(clientorderrepository.findByClientId(clientId));
    }
    
    public ClientOrder getClientOrder(int orderId, int userId){
        ArrayList<ClientOrder> orders = getMyOrders(userId);
        for (ClientOrder order: orders){
            if (order.getId() == orderId){
                return order;
            }
        }
        return null;
    }
    
    public Address addAddress(String type, Address address,ClientOrder order){
        Address newAddress = addressrepository.save(address);
    
        ClientOrderAddress orderAddress = new ClientOrderAddress(order, newAddress);
        orderAddress.setAddressType(type);
        order.addAddress(orderAddress);
        address.addAddress(orderAddress);
        clientorderaddressrepository.save(orderAddress);
        
        return newAddress;
    }
    
    public Address getAddress(String typeOfAddress, ClientOrder clientorder){
        ArrayList<ClientOrderAddress> ordAddrs = new ArrayList<>(clientorderaddressrepository.findByClientOrderId(clientorder.getId()));
        for (ClientOrderAddress ord: ordAddrs){
            if (ord.getAddressType().equalsIgnoreCase(typeOfAddress)){
                return ord.getAddress();
            }
        }
        return null;
    }
    
    public ArrayList<LineItem> getAllMyLineItems(int clientId){
        return new ArrayList<LineItem>(lineitemrepository.findByClientId(clientId));
    }
    
    public ArrayList<LineItem> getAllLineItemsForId(int orderId){
        return new ArrayList<LineItem>(lineitemrepository.findByClientOrderId(orderId));
    }
    
    // Only fufill items if the items have been payed for
    public ReturnPair<Boolean, String> fufillItems(FufillInputWrapper wrapper, int orderId){
        ArrayList<LineItem> arr = this.getAllLineItemsForId(orderId);
        System.out.println("Arraylist size()=" + arr.size());
        Optional<Invoice> inv = invoicerepository.findByClientOrderId(orderId);
        if (inv.isPresent() == true && inv.get().hasPayed() == true){
            for (LineItem item : arr){
                if (wrapper.getProductsDelivered().contains(item.getProduct().getProductId())){
                    Product product = item.getProduct();
                    int quantity = product.getProductQuantity() - item.getProductQuantity();
                    if (product.getProductQuantity() > quantity){
                        product.setProductQuantity(quantity);
                        productrepository.save(product);
                        item.setFufilled(true);
                        lineitemrepository.save(item);
                    }
                }
            }
        } else {
            String str = "This id(" + orderId + ") has not been payed for yet";
            return new ReturnPair<Boolean, String>(false, str);
        }
        return new ReturnPair<Boolean, String>(true, "");
    }
    
    public ClientOrder getOrderForReservation(int reservation){
        return clientorderrepository.findByReservationId(reservation);
    }
    
    public ReturnPair<Boolean, String> stopOrder(ClientOrder clientorder){
        Optional<Invoice> inv = invoicerepository.findByClientOrderId(clientorder.getId());
        if (inv.isPresent() == false || inv.get().hasPayed() == false){
            removeOrder(clientorder);
            return new ReturnPair<Boolean, String>(true, "");
        }
        String str = "Invoice for this order(" + String.valueOf(clientorder.getId()) + ") has been processed";
        return new ReturnPair<Boolean, String>(false, str);
    }
    
    public void removeOrder(ClientOrder order){
        ArrayList<LineItem> items = new ArrayList<>(lineitemrepository.findByClientOrderId(order.getId()));
        for (LineItem item: items){
            order.removeLineItem(item);
            Product product = item.getProduct();
            product.removeLineItem(item);
        }
        lineitemrepository.removeByClientOrderId(order.getId());
        ArrayList<ClientOrderAddress> ritems = new ArrayList<>(clientorderaddressrepository
            .findByClientOrderId(order.getId()));
        for (ClientOrderAddress item: ritems){
            order.removeAddress(item);
            Address address = item.getAddress();
        }
        clientorderaddressrepository.removeByClientOrderId(order.getId());
        Optional<Invoice> inv = invoicerepository.findByClientOrderId(order.getId());
        if (inv.isPresent() == true){
            invoicerepository.deleteById(inv.get().getId());
        }
        clientorderrepository.deleteById(order.getId());
    }
}