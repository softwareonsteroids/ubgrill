package com.davide.Ubgrill.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.davide.Ubgrill.model.user.Role;
import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.repository.RoleRepository;
import com.davide.Ubgrill.repository.ClientRepository;

import com.davide.Ubgrill.config.security.JwtAuthUser;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.text.SimpleDateFormat;
import java.text.ParseException;

@Service("clientservice")
public class ClientService{
    @Autowired
    private RoleRepository rolerepository;
    
    @Autowired
    private ClientRepository clientrepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    public Client saveClient(Client client){
        client.setPassword(bCryptPasswordEncoder.encode(client.getPassword())); 
        client.setEnabled(true);
	
	// Save the date the password was created in the particular format
        SimpleDateFormat sf = new SimpleDateFormat("dd-MM-YYYY hh:mm:ss a");
        try{
	    Date dt = sf.parse(sf.format(new Date()));
	    client.setLastPasswordResetDate(dt);
        } catch (ParseException ex){
            ex.printStackTrace();
        }

        Role clientRole = rolerepository.findByRolename("ROLE_USER");       // Saves only users
        client.setRoles(new ArrayList<Role>(Arrays.asList(clientRole)));
        
        return clientrepository.save(client);
    }
    
    public Client getClientByUsername(String username){
        return clientrepository.findByUsername(username);
    }
    // Get all users [only admin] 
    
    public String updatePartialClient(Client old, Map<String, Object> updates){
        String error = "";
        for (Map.Entry<String, Object> entry: updates.entrySet()){
            switch(entry.getKey()){
                case "username":
                    if (entry.getValue() != null){
                        String newusername = (String)entry.getValue();
                        old.setUsername(newusername);
                    }
                    break;
                case "password":
                    if (entry.getValue() != null){
                        String newpassword = (String)entry.getValue();
                        old.setPassword(bCryptPasswordEncoder.encode(newpassword));
                        SimpleDateFormat sf = new SimpleDateFormat("dd-MM-YYYY hh:mm:ss a");
                        try{
                            Date dt = sf.parse(sf.format(new Date()));
                            old.setLastPasswordResetDate(dt);
                        } catch (ParseException ex){
                            ex.printStackTrace();
                        }
                    }
                    break;
                case "email":
                    if (entry.getValue() != null){
                        String newemail = (String)entry.getValue();
                        old.setEmail(newemail);
                    }
                    break;
                case "firstname":
                    if (entry.getValue() != null){
                        String newfirstname = (String)entry.getValue();
                        old.setFirstname(newfirstname);
                    }
                    break;
                case "lastname":
                    if (entry.getValue() != null){
                        String newlastname = (String)entry.getValue();
                        old.setLastname(newlastname);
                    }
                    break;
                default:
                    error = "This property: (" + (String)entry.getKey() + ") is not a propery of this object";
                    break;
            }
        }
        clientrepository.save(old);
        return error;
    }
}
