package com.davide.Ubgrill.service;

import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.model.user.Role;
import com.davide.Ubgrill.config.security.JwtAuthUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Set;
import java.util.HashSet;

@Service
public class CustomUserDetailsService implements UserDetailsService{

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private ClientService clientservice;
    
    @Override
    public UserDetails loadUserByUsername(String username) 
        throws UsernameNotFoundException{
        Client client = clientservice.getClientByUsername(username);
        if (client == null){
            throw new UsernameNotFoundException("username not found");
        }
        return new JwtAuthUser(client);
    }    
}
