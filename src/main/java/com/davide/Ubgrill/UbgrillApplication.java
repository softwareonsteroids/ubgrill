package com.davide.Ubgrill;

import javax.annotation.PostConstruct;
import java.util.TimeZone;
//import java.time.ZoneId;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UbgrillApplication {

        @PostConstruct
        void setUTCTimezone(){
            // Apparently you have to use a region based id
            //ZoneId defat = ZoneId.of("America/Chicago");   
            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        }

	public static void main(String[] args) {
		SpringApplication.run(UbgrillApplication.class, args);
	}
}

