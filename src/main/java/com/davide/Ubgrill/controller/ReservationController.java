package com.davide.Ubgrill.controller;

import com.davide.Ubgrill.config.security.JwtAuthUser;
import com.davide.Ubgrill.error.*;
import com.davide.Ubgrill.model.reservation.Reservation;
import com.davide.Ubgrill.model.store.ClientOrder;
import com.davide.Ubgrill.model.store.OrderDTO;
import com.davide.Ubgrill.service.ReservationService;
import com.davide.Ubgrill.service.ClientOrderService;
import com.davide.Ubgrill.service.AuthService;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Map; 



@RestController
@RequestMapping("/ubgrill/api")
public class ReservationController {
    
    @Autowired
    private ReservationService reservationservice;
    
    @Autowired
    private AuthService authService;
    
    @Autowired
    private ClientOrderService orderservice;
    
    // Create a new reservation
    @RequestMapping(value = "/my/reservations", method = RequestMethod.POST)
    public ResponseEntity<Reservation> createReservation(
                @Valid @RequestBody Reservation reservation,
                @RequestHeader HttpHeaders headers,
                UriComponentsBuilder ucb){
        JwtAuthUser user = authService.retrieveAuthUserFromHeaders(headers);
        Reservation newReservation = reservationservice.createReservation(reservation, user.getUsername());
        HttpHeaders newHeaders = new HttpHeaders();
        newHeaders.setLocation(ucb.path("/ubgrill/api/my/reservations/").path(String.valueOf(newReservation.getReservationId())).build().toUri());
        return new ResponseEntity<Reservation>(
                newReservation, newHeaders, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/reservations", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ArrayList<Reservation>> getAllReservations(){
        return ResponseEntity.ok().body(reservationservice.getAllReservations());
    }
    
    @RequestMapping(value = "/my/reservations", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Reservation>> getAllReservationsForMe(
                @RequestHeader HttpHeaders headers){
        JwtAuthUser user = authService.retrieveAuthUserFromHeaders(headers);
	ArrayList<Reservation> reservations = reservationservice.getAllReservationsFor(user.getId());
        return ResponseEntity.ok().body(reservations);
    }
    
    @RequestMapping(value = "/my/reservations/{id}", method = RequestMethod.GET)
    public ResponseEntity<Reservation> getReservation(@PathVariable(value = "id") int reservationId){
        Reservation reservation = reservationservice.getReservation(reservationId);
        if (reservation != null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(reservation);
    }
    
    // Get associated order with reservation
    @RequestMapping(value="/my/reservations/{id}/order", method=RequestMethod.GET)
    public ResponseEntity<OrderDTO> getOrderForReservation(@PathVariable(value="id") int reservationId){
        Reservation reservation = reservationservice.getReservation(reservationId);
        if ( reservation == null){
            throw new ResourceNotFoundException("This reservation is not a valid reservation");
        }
        ClientOrder order = orderservice.getOrderForReservation(reservationId);
        OrderDTO orderDto = new OrderDTO(order);
        return ResponseEntity.ok().body(orderDto);
    }
    
    @RequestMapping(value = "/my/reservations/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> updateReservation(
                    @PathVariable(value = "id") int reservationId,
                    @RequestBody Map<String, Object> updates){
        Reservation reservationOld = reservationservice.getReservation(reservationId);
        if (reservationOld != null){
            return ResponseEntity.notFound().build();
        }
        String error = reservationservice.updatePartialReservation(reservationOld, updates);
        if (!error.isEmpty()){
            return ResponseEntity.badRequest().body(error);
        }
        Reservation updatedReservation = reservationservice.getReservation(reservationId);
        return ResponseEntity.ok().body(updatedReservation);
    }
    
    @RequestMapping(value = "/my/reservations/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Reservation> deleteReservation(@PathVariable(value = "id") int reservationId){
        Reservation reservation = reservationservice.getReservation(reservationId);
        if (reservation != null){
            return ResponseEntity.notFound().build();
        }
        reservationservice.deleteReservation(reservation);
        return ResponseEntity.noContent().build();
    }
}
