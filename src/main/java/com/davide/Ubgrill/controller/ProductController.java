package com.davide.Ubgrill.controller;

import com.davide.Ubgrill.service.ProductService;
import com.davide.Ubgrill.model.product.Product;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Map;

// v1 should be a plain API
// v2 should be queryable

@RestController
@RequestMapping("/ubgrill/api")
public class ProductController {

    @Autowired
    private ProductService productservice;
    
    // Create a new Product
    @RequestMapping(value = "/products", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Product> createProduct(@Valid @RequestBody Product product,
                    UriComponentsBuilder ucb){
        int newProductId = productservice.createProduct(product);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/ubgrill/api/products/").path(String.valueOf(newProductId)).build().toUri());
        return new ResponseEntity<Product>(
                product, headers, HttpStatus.CREATED);
    }
    
    // Get all products
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Product>> getAllProducts(){
        return ResponseEntity.ok().body(productservice.getAllProducts());
    }
    
    // Get product by Id
    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getProduct(@PathVariable(value = "id") int productId) {
        Product product = productservice.getProduct(productId);
        if ( product == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(product);
    }
    
    // Delete product by Id
    @RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Product> deleteProduct(@PathVariable(value = "id") int productId) {
        Product product = productservice.getProduct(productId);
        if ( product == null){
            return ResponseEntity.notFound().build();
        }
        productservice.deleteProduct(product);
        return ResponseEntity.noContent().build();
    }
    
    // Update a product partially
    @RequestMapping(value = "/products/{id}", method = RequestMethod.PATCH)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateProduct(@PathVariable(value="id") int productId,
                            @RequestBody Map<String, Object> updates) {
        Product productOld = productservice.getProduct(productId);
        if ( productOld == null){
            return ResponseEntity.notFound().build();
        }
        String error = productservice.updatePartialProduct(productOld, updates);
        if (!error.isEmpty()){
            return ResponseEntity.badRequest().body(error);
        }
        Product updatedProduct = productservice.getProduct(productId);
        return ResponseEntity.ok().body(updatedProduct);
    }
}
