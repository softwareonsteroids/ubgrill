package com.davide.Ubgrill.controller;

import com.davide.Ubgrill.config.security.JwtAuthUser;
import com.davide.Ubgrill.error.*;
import com.davide.Ubgrill.model.store.ClientOrder;
import com.davide.Ubgrill.model.store.Invoice;
import com.davide.Ubgrill.model.wrappers.InvoiceInputWrapper;
import com.davide.Ubgrill.service.ClientOrderService;
import com.davide.Ubgrill.service.InvoiceService;
import com.davide.Ubgrill.service.AuthService;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;

@RestController
@RequestMapping("/ubgrill/api")
public class InvoiceController {
	
    @Autowired
    private InvoiceService invoiceservice;
    
    @Autowired
    private ClientOrderService orderservice;
    
    @Autowired
    private AuthService authService;
	
	// Create a new Invoice
    @RequestMapping(value = "/my/orders/{id}/invoice", method = RequestMethod.POST)
    public ResponseEntity<Invoice> createInvoice(@PathVariable(value="id") int orderId,
			@RequestHeader HttpHeaders recvHeaders,
			@RequestBody InvoiceInputWrapper wrapper,
			UriComponentsBuilder ucb){
        JwtAuthUser user = authService.retrieveAuthUserFromHeaders(recvHeaders);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        Invoice invoice = invoiceservice.createInvoice(order, wrapper);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/ubgrill/api/my/invoices/").path(String.valueOf(invoice.getId())).build().toUri());
        return new ResponseEntity<Invoice>(
            invoice, headers, HttpStatus.CREATED);
    }
    
    // Get all my invoices 
    @RequestMapping(value = "/my/invoices", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Invoice>> getMyInvoices(@RequestHeader HttpHeaders headers){
        JwtAuthUser user = authService.retrieveAuthUserFromHeaders(headers);
        return ResponseEntity.ok().body(invoiceservice.getMyInvoices(user.getId()));
    }
	
    // Get all invoices 
    @RequestMapping(value = "/invoices", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ArrayList<Invoice>> getMyInvoices(){
        return ResponseEntity.ok().body(invoiceservice.getAllInvoices());
    }
    
    // Fufill orders 
    @RequestMapping(value="/my/invoices/{id}/fufill", method= RequestMethod.PATCH)
    public ResponseEntity<Invoice> markInvoicePayed(@PathVariable(value="id") int invoiceId){
        Invoice invoice = invoiceservice.getInvoiceFor(invoiceId);
        if ( invoice == null){
            return ResponseEntity.notFound().build();
        } 
        Invoice newInvoice = invoiceservice.markInvoicePayed(invoice);
        return ResponseEntity.ok().body(newInvoice);
    }
	
    // Delete invoice
    @RequestMapping(value="/invoices/{id}", method= RequestMethod.DELETE)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity removeInvoice(@PathVariable(value="id") int invoiceId){
        Invoice invoice = invoiceservice.getInvoiceFor(invoiceId);
        if ( invoice == null){
            return ResponseEntity.notFound().build();
        }
        invoiceservice.removeInvoice(invoice);
        return ResponseEntity.noContent().build();
    }
}