package com.davide.Ubgrill.controller;

import com.davide.Ubgrill.config.security.JwtAuthUser;
import com.davide.Ubgrill.error.*;
import com.davide.Ubgrill.model.product.Product;
import com.davide.Ubgrill.model.reservation.Reservation;
import com.davide.Ubgrill.model.store.ClientOrder;
import com.davide.Ubgrill.model.store.LineItem;
import com.davide.Ubgrill.model.store.LineItemOutputDTO;
import com.davide.Ubgrill.model.store.LineItemPartialOutputDTO;
import com.davide.Ubgrill.model.store.OrderDTO;
import com.davide.Ubgrill.model.user.Address;
import com.davide.Ubgrill.model.user.AddressDTO;
import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.model.wrappers.FufillInputWrapper;
import com.davide.Ubgrill.model.wrappers.OrderInputWrapper;
import com.davide.Ubgrill.model.wrappers.ReturnPair;
import com.davide.Ubgrill.service.ClientOrderService;
import com.davide.Ubgrill.service.ProductService;
import com.davide.Ubgrill.service.AuthService;
import com.davide.Ubgrill.service.ClientService;
import com.davide.Ubgrill.service.ReservationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;

@RestController
@RequestMapping("/ubgrill/api")
public class ClientOrderController {
    
    @Autowired
    private ClientOrderService orderservice;
    
    @Autowired
    private AuthService authservice;
    
    @Autowired
    private ClientService clientservice;
    
    @Autowired
    private ProductService productservice;
    
    @Autowired
    private ReservationService reservationservice;
    
    // Create a new ClientOrder
    @RequestMapping(value = "/my/orders", method = RequestMethod.POST)
    public ResponseEntity<OrderDTO> createClientOrder(
        @RequestBody OrderInputWrapper wrapper,
        @RequestHeader HttpHeaders recvHeaders,
        UriComponentsBuilder ucb){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(recvHeaders);
        Client client = clientservice.getClientByUsername(user.getUsername());
        
        ReturnPair<Boolean,String> response = productservice.isProductListValid(wrapper.getProductIds());
        if (response.getKey() == false){
            throw new ResourceNotFoundException(response.getValue());
        }
        ArrayList<Product> products = productservice.getProductsForIds(wrapper.getProductIds());
    
        Reservation reservation = reservationservice.getReservation(wrapper.getReservationId());
        if ( reservation == null){
            throw new ResourceNotFoundException("This reservation is not a valid reservation");
        }
    
        ClientOrder order = orderservice.createClientOrder(client, reservation);
        String returned = orderservice.saveLineItems(order, products, wrapper);
        if (!returned.isEmpty()){
            throw new MethodCannotBePerformedException(returned);
        }
        
        OrderDTO orderDto = new OrderDTO(order);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/ubgrill/api/my/orders/").path(String.valueOf(order.getId())).build().toUri());
        return new ResponseEntity<OrderDTO>(orderDto, headers, HttpStatus.CREATED);
    }
    
    // Create a new ClientOrder
    @RequestMapping(value = "/my/orders", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<OrderDTO>> getMyOrders(
        @RequestHeader HttpHeaders recvHeaders){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(recvHeaders);
        Client client = clientservice.getClientByUsername(user.getUsername());
        ArrayList<OrderDTO> list = new ArrayList<>();
        for (ClientOrder order: orderservice.getMyOrders(client.getId())){
            list.add(new OrderDTO(order));
        }
        return ResponseEntity.ok().body(list);
    }
    
    // Add a billing address
    @RequestMapping(value = "/my/orders/{id}/billing", method = RequestMethod.POST)
    public ResponseEntity<AddressDTO> addBillingAddress(@Valid @RequestBody Address address,
        @RequestHeader HttpHeaders recvHeaders,
        @PathVariable(value="id") int orderId, 
        UriComponentsBuilder ucb){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(recvHeaders);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        Address newAddress=orderservice.addAddress("billing", address, order);
        AddressDTO addressDto = new AddressDTO(newAddress);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/ubgrill/api/my/orders/")
            .path(String.valueOf(orderId))
            .path(String.valueOf(address.getId()))
            .build().toUri());
        return new ResponseEntity<AddressDTO>(addressDto, headers, HttpStatus.CREATED);
    }
    
    // Get billing address
    @RequestMapping(value = "/my/orders/{id}/billing", method = RequestMethod.GET)
    public ResponseEntity<AddressDTO> getBillingAddress(@PathVariable(value="id") int orderId,
                        @RequestHeader HttpHeaders headers){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        Address address = orderservice.getAddress("BILLING", order);
        if ( address == null){
            throw new ResourceNotFoundException("No billing address found. Set billing address");
        }
        AddressDTO addressDto = new AddressDTO(address);
        return ResponseEntity.ok().body(addressDto);
    }
    
    // Add a shipping address
    @RequestMapping(value = "/my/orders/{id}/shipping", method = RequestMethod.POST)
    public ResponseEntity<AddressDTO> addShippingAddress(@Valid @RequestBody Address address,
        @RequestHeader HttpHeaders recvHeaders,
        @PathVariable(value="id") int orderId, 
        UriComponentsBuilder ucb){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(recvHeaders);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        Address newAddress=orderservice.addAddress("shipping", address, order);
        AddressDTO addressDto = new AddressDTO(newAddress);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/ubgrill/api/my/orders/")
            .path(String.valueOf(orderId))
            .path(String.valueOf(address.getId()))
            .build().toUri());
        return new ResponseEntity<AddressDTO>(addressDto, headers, HttpStatus.CREATED);
    }
    
    // Get shipping address
    @RequestMapping(value = "/my/orders/{id}/shipping", method = RequestMethod.GET)
    public ResponseEntity<AddressDTO> getShippingAddress(@PathVariable(value="id") int orderId,
                        @RequestHeader HttpHeaders headers){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        Address address = orderservice.getAddress("SHIPPING", order);
        if ( address == null){
            throw new ResourceNotFoundException("No shipping address found. Set shipping address");
        }
        AddressDTO addressDto = new AddressDTO(address);
        return ResponseEntity.ok().body(addressDto);
    }
    
    // Add a billing/shipping address
    @RequestMapping(value = "/my/orders/{id}/billingshipping", method = RequestMethod.POST)
    public ResponseEntity<AddressDTO> makeBillingAddressShipping(@Valid @RequestBody Address address,
        @RequestHeader HttpHeaders recvHeaders,
        @PathVariable(value="id") int orderId, 
        UriComponentsBuilder ucb){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(recvHeaders);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        Address newAddress=orderservice.addAddress("billingandshipping", address, order);
        AddressDTO addressDto = new AddressDTO(newAddress);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/ubgrill/api/my/orders/")
            .path(String.valueOf(orderId))
            .path(String.valueOf(address.getId()))
            .build().toUri());
        return new ResponseEntity<AddressDTO>(addressDto, headers, HttpStatus.CREATED);
    }
    
    // Get billing/shipping address
    @RequestMapping(value = "/my/orders/{id}/billingshipping", method = RequestMethod.GET)
    public ResponseEntity<AddressDTO> getBillingAddressShipping(@PathVariable(value="id") int orderId,
                        @RequestHeader HttpHeaders headers){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        Address address = orderservice.getAddress("BILLINGANDSHIPPING", order);
        if ( address == null){
            throw new ResourceNotFoundException("No suitable address found. Set either billing or shipping address");
        }
        AddressDTO addressDto = new AddressDTO(address);
        return ResponseEntity.ok().body(addressDto);
    }
    
    // Get all the mapping of the productIds and quantities
    @RequestMapping(value = "/my/lineitems", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<LineItemPartialOutputDTO>> getAllMyLineItems(
            @RequestHeader HttpHeaders headers){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        Client client = clientservice.getClientByUsername(user.getUsername());
        ArrayList<LineItemPartialOutputDTO> arr = new ArrayList<LineItemPartialOutputDTO>();
        for ( LineItem item: orderservice.getAllMyLineItems(client.getId()) ){
            arr.add(new LineItemPartialOutputDTO(item));
        }
        return ResponseEntity.ok().body(arr);
    }
    
    // Get all the mapping of the productIds and quantities
    @RequestMapping(value = "/my/orders/{id}/lineitems", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<LineItemOutputDTO>> getAllLineItemsForId(
            @PathVariable(value="id") int orderId,
            @RequestHeader HttpHeaders headers){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        
        ArrayList<LineItem> items = orderservice.getAllLineItemsForId(orderId);
        ArrayList<Integer> productsIds = new ArrayList<Integer>();
        for (LineItem item: items){
            productsIds.add(item.getId().productId);
        }
        ArrayList<Product> prods = productservice.getProductsForIds(productsIds);
        
        Iterator<Product> productIterator = prods.iterator();
        Iterator<LineItem> lineitemIterator = items.iterator();
        
        ArrayList<LineItemOutputDTO> arr = new ArrayList<LineItemOutputDTO>();
        
        while( productIterator.hasNext() && lineitemIterator.hasNext()){
            Product prod = productIterator.next();
            LineItem item = lineitemIterator.next();
            if (prod.getProductId() == item.getId().productId){
               arr.add(new LineItemOutputDTO(item, prod));
            }
        }
        return ResponseEntity.ok().body(arr);
    }
    
    // Stop order if invoice has not been processed
    @RequestMapping(value = "/my/orders/{id}", method = RequestMethod.DELETE)
    public ResponseEntity stopOrder(@PathVariable(value = "id") int orderId,
            @RequestHeader HttpHeaders headers) {
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        ReturnPair<Boolean, String> hasBeenDeleted = orderservice.stopOrder(order);
        if (hasBeenDeleted.getKey() == true){
                return ResponseEntity.noContent().build();
        }
        throw new MethodCannotBePerformedException(hasBeenDeleted.getValue());
    }
    
    // Set order(items) to be fufilled
    @RequestMapping(value = "/my/orders/{id}/fufill", method = RequestMethod.PATCH)
    public ResponseEntity<ArrayList<LineItemOutputDTO>> fufillOrder(
            @PathVariable(value = "id") int orderId,
            @RequestBody FufillInputWrapper wrapper,
            @RequestHeader HttpHeaders headers){
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        ReturnPair<Boolean, String> response = orderservice.fufillItems(wrapper, orderId);
        if (response.getKey() == false){
            throw new MethodCannotBePerformedException(response.getValue());
        }
        
        ArrayList<LineItem> items = orderservice.getAllLineItemsForId(orderId);
        ArrayList<Integer> productsIds = new ArrayList<Integer>();
        for (LineItem item: items){
            productsIds.add(item.getId().productId);
        }
        ArrayList<Product> prods = productservice.getProductsForIds(productsIds);
        
        Iterator<Product> productIterator = prods.iterator();
        Iterator<LineItem> lineitemIterator = items.iterator();
        
        ArrayList<LineItemOutputDTO> arr = new ArrayList<LineItemOutputDTO>();
        
        while( productIterator.hasNext() && lineitemIterator.hasNext()){
            Product prod = productIterator.next();
            LineItem item = lineitemIterator.next();
            if (prod.getProductId() == item.getId().productId){
               arr.add(new LineItemOutputDTO(item, prod));
            }
        }
        return ResponseEntity.ok().body(arr);
    }
    
    // Delete order
    @RequestMapping(value = "/orders/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity removeClientOrder(@PathVariable(value = "id") int orderId,
            @RequestHeader HttpHeaders headers) {
        JwtAuthUser user = authservice.retrieveAuthUserFromHeaders(headers);
        ClientOrder order=orderservice.getClientOrder(orderId, user.getId());
        if ( order == null){
            String str = "This id (" + orderId + ") is not a valid id";
            throw new ResourceNotFoundException(str);
        }
        orderservice.removeOrder(order);
        return ResponseEntity.noContent().build();
    }
}