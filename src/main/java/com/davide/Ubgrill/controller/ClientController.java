package com.davide.Ubgrill.controller;

import com.davide.Ubgrill.config.security.JwtAuthUser;
import com.davide.Ubgrill.service.ClientService;
import com.davide.Ubgrill.service.AuthService;
import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.model.user.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Map;

@RestController
@RequestMapping("/ubgrill/api")
public class ClientController{

    @Autowired
    private ClientService clientservice;
    
    @Autowired
    private AuthService authService;
    
    // Save a client
    @RequestMapping(value="/register", method = RequestMethod.POST)
    public ResponseEntity<Client> saveUser(@Valid @RequestBody Client client){
        Client savedClient = clientservice.saveClient(client);
        return ResponseEntity.ok().body(savedClient);
    }
    
    // Edit a client
    @RequestMapping(value="/me/edit", method = RequestMethod.PATCH)
    public ResponseEntity<?> editUser(@RequestHeader HttpHeaders recvHeaders,
            @RequestBody Map<String, Object> updates){
        JwtAuthUser user = authService.retrieveAuthUserFromHeaders(recvHeaders);
        Client clientOld = clientservice.getClientByUsername(user.getUsername());
        String error = clientservice.updatePartialClient(clientOld, updates);
        if (!error.isEmpty()){
            return ResponseEntity.badRequest().body(error);
        }
        return ResponseEntity.ok().build();
    }

}