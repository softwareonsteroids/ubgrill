package com.davide.Ubgrill.controller; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.davide.Ubgrill.config.security.JwtAuthRequest;
import com.davide.Ubgrill.config.security.JwtAuthUser;
import com.davide.Ubgrill.config.security.JwtAuthResponse;
import com.davide.Ubgrill.service.AuthService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/ubgrill/api")
public class AuthController{
    
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AuthService authService;
    
    @RequestMapping(value="/auth", method = RequestMethod.POST)
    public ResponseEntity<?> createAndGetAuthToken(
        @RequestBody JwtAuthRequest authenticationRequest) 
        throws AuthenticationException {
        
        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        // Reload password post-security so we can generate token
        String token = authService.createNewToken(authenticationRequest.getUsername());
        
        // Return the token
        return ResponseEntity.ok(new JwtAuthResponse(token));
    }
    
    @RequestMapping(value="/auth/refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthToken(HttpServletRequest request){
        String refreshedToken = authService.createRefreshToken(request);
        if (refreshedToken.isEmpty()){
            return ResponseEntity.badRequest().body(null);
        }
        return ResponseEntity.ok(new JwtAuthResponse(refreshedToken));
    }
    
    //@PreAuthorize("hasAnyRole({'ROLE_ADMIN','ROLE_USER'})")
    @RequestMapping(value="/me", method = RequestMethod.GET)
    public ResponseEntity<?> getAuthUser (HttpServletRequest request){
        JwtAuthUser user = authService.retrieveAuthUser(request);
        return ResponseEntity.ok(user);
    }
}