package com.davide.Ubgrill.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param; 
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.davide.Ubgrill.model.store.LineItem;
import com.davide.Ubgrill.model.store.LineItemId;
import java.util.List;

@Repository("LineItemRepository")
public interface LineItemRepository extends JpaRepository<LineItem, LineItemId>{
    @Query(value="SELECT * from lineitem l where l.clientorder_id = :clientorder_id", nativeQuery=true)
    List<LineItem> findByClientOrderId(@Param("clientorder_id") int clientorderId);
    
    @Query(value="SELECT l.clientorder_id, l.product_id, l.product_quantity, l.is_fufilled from lineitem l, clientorder co where l.clientorder_id = co.clientorder_id and co.client_id = :client_id", nativeQuery=true)
    List<LineItem> findByClientId(@Param("client_id") int clientId);
    
    @Transactional
    @Modifying
    @Query(value="DELETE FROM lineitem where clientorder_id = :clientorder_id", nativeQuery=true)
    void removeByClientOrderId(@Param("clientorder_id") int clientorderId);
}