package com.davide.Ubgrill.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param; 
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.davide.Ubgrill.model.store.ClientOrder;
import java.util.List;

@Repository("ClientOrderRepository")
public interface ClientOrderRepository extends JpaRepository<ClientOrder, Integer>{
        @Query(value="SELECT * from clientorder co where co.client_id = :client_id", nativeQuery=true)
        List<ClientOrder> findByClientId(@Param("client_id")int clientId);
        
        @Transactional
        @Modifying
        @Query(value="DELETE from clientorder where clientorder_id = :clientorder_id",
        nativeQuery=true)
        void deleteById(@Param("clientorder_id")int clientorderId);
        
        @Query(value="SELECT * from clientorder co where co.reservation_id = :reservation_id", 
        nativeQuery=true)
        ClientOrder findByReservationId(@Param("reservation_id") int reservationId);
}
