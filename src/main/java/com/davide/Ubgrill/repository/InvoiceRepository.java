package com.davide.Ubgrill.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param; 
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.davide.Ubgrill.model.store.Invoice;
import java.util.List;
import java.util.Optional;

@Repository("InvoiceRepository")
public interface InvoiceRepository extends JpaRepository<Invoice, Integer>{ 
		
        //@Nullable
        @Query(value="SELECT * from invoice inv where inv.clientorder_id = :clientorder_id", nativeQuery=true)
        Optional<Invoice> findByClientOrderId(@Param("clientorder_id") int clientOrderId);
		
        @Query(value="SELECT i.invoice_id, i.clientorder_id, i.order_cost, i.created_on, i.updated_on, i.delivery_time, i.delivery_miles, i.delivery_cost, i.has_payed from invoice i, clientorder co where i.clientorder_id = co.clientorder_id and co.client_id = :client_id", nativeQuery=true)
        List<Invoice> findByClientId(@Param("client_id") int clientId);
        
        // If you dont put @Modifying above the delete query, you get a 
        // java.sql.SQLException: Can not issue data manipulation statements with executeQuery()
        // because the @Modifying converts the default executeQuery() into executeUpdate()
        @Transactional
        @Modifying
        @Query(value="DELETE FROM invoice where invoice_id = :invoice_id", nativeQuery=true)
        void deleteById(@Param("invoice_id") int invoiceId);
        
        @Transactional
        @Modifying
        @Query(value="UPDATE invoice set has_payed=1 where invoice_id = :invoice_id", nativeQuery=true)
        void setPayed(@Param("invoice_id") int invoiceId);
}