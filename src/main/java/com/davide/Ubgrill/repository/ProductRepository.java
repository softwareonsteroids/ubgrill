package com.davide.Ubgrill.repository;

import com.davide.Ubgrill.model.product.Product;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository("productRepository")
public interface ProductRepository extends JpaRepository<Product, Integer>{}
 
