package com.davide.Ubgrill.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.davide.Ubgrill.model.user.Client;

@Repository("clientRepository")
public interface ClientRepository extends JpaRepository<Client, Integer>{
        Client findByUsername(String username);
}