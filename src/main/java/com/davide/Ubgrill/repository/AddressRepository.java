package com.davide.Ubgrill.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param; 
import org.springframework.data.jpa.repository.Query;


import com.davide.Ubgrill.model.user.Address;

@Repository("AddressRepository")
public interface AddressRepository extends JpaRepository<Address, Integer>{
        
        @Query(value="SELECT adr from address adr where adr.address_id = :address_id", nativeQuery=true)
        Address findById(@Param("address_id") int id);
} 
