package com.davide.Ubgrill.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param; 
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.davide.Ubgrill.model.reservation.Reservation;
import java.util.List;

@Repository("reservationRepository")
public interface ReservationRepository extends JpaRepository<Reservation, Integer>{    
    // write your class name in capital 
    // https://stackoverflow.com/questions/22010335/custom-queries-with-crudrepository
    // or match up your entity names
    // or use a nativeQuery (works)
    @Query(value="SELECT * from reservation where client_id = :client_id",
    nativeQuery=true)
    List<Reservation> findByClientId(@Param("client_id") int client_id);
    
    @Transactional
    @Modifying
    @Query(value="DELETE from reservation where reservation_id = :reservation_id",
    nativeQuery=true)
    void deleteById(@Param("reservation_id") int reservationId);
}
