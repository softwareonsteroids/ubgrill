package com.davide.Ubgrill.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param; 
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.davide.Ubgrill.model.store.ClientOrderAddress;
import com.davide.Ubgrill.model.store.ClientOrderAddressId;
import java.util.List;

@Repository("ClientOrderAddressRepository")
public interface ClientOrderAddressRepository extends JpaRepository<ClientOrderAddress, ClientOrderAddressId>{

        @Query(value="SELECT * from clientorderaddress coa where coa.clientorder_id = :clientorder_id",
            nativeQuery=true)
        List<ClientOrderAddress> findByClientOrderId(@Param("clientorder_id")int clientorderId);
        
        @Transactional
        @Modifying
        @Query(value="DELETE FROM clientorderaddress where clientorder_id = :clientorder_id", nativeQuery=true)
        void removeByClientOrderId(@Param("clientorder_id") int clientorderId);
} 
