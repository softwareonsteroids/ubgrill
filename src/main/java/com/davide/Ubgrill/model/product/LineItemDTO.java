package com.davide.Ubgrill.model.product;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LineItemDTO{
    
    @JsonProperty("productId")
    public int productId;
    
    @JsonProperty("productQuantity")
    public int productQuantity;
    
    LineItemDTO(){}
    
    LineItemDTO(int productId, int productQuantity){
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
    
    public int getId(){
        return this.productId;
    }
    
    public int getQuantity(){
        return this.productQuantity;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Product To Be Ordered: ");
        sb.append("Id:[");
        sb.append(String.valueOf(this.productId));
        sb.append("] Quantity:[");
        sb.append(String.valueOf(this.productQuantity));
        sb.append("]");
        
        return sb.toString();
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        LineItemDTO that = (LineItemDTO) o;
        
        return Objects.equals(productId, that.productId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.productId);
    }
}
