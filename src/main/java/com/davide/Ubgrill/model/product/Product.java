package com.davide.Ubgrill.model.product;

import javax.persistence.*;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;
import com.davide.Ubgrill.model.store.LineItem;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name="Product")
@Table(name="product")
public class Product{
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="product_entities_sequence_generator")
    @GenericGenerator(
        name = "product_entities_sequence_generator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
                //@Parameter(name = "sequence_name" , value = "product_entities_sequence")
                @Parameter(name = "optimizer" , value = "pooled-lo"),
                @Parameter(name = "initial_value", value = "1020"),
                @Parameter(name = "increment_size", value = "1")
        }
    )
    @Column(name="product_id")
    private int productId;
    
    @Column(name="product_quantity")
    @NotNull
    private int productQuantity;

    @Column(name="product_quantity_per_order")
    @NotNull
    private int productQuantityPerOrder;
    
    @Column(name="product_name")
    @NotNull
    private String productName;
    
    @Column(name="product_pic_loc")
    @NotNull
    private String productPicLoc;
    
    @Column(name="product_details")
    @NotNull
    private String productDetails;
    
    // Thought of using Attribute Converters ( maybe v2.0)
    @Enumerated(EnumType.STRING)
    @Column(name="product_type")
    @NotNull
    private ProductType productType;
    
    @Column(name="price")
    @NotNull
    private Double price;
    
    @JsonIgnoreProperties("product")
    @OneToMany(mappedBy="product",fetch=FetchType.LAZY)
    private List<LineItem> lineitems = new ArrayList<LineItem>();
    
    public Product(){
    
    }
    
    public Product(int productQuantity, 
            double price,
            int productQuantityPerOrder,
            String productType,
            String productName,
            String productPicLoc,
            String productDetails){
        this.productQuantity = productQuantity;
        this.price = price;
        this.productQuantityPerOrder = productQuantityPerOrder;
        this.productName = productName;
        this.productType = ProductType.valueOf(productType.toUpperCase());
        this.productPicLoc = productPicLoc;
        this.productDetails = productDetails;
    }
    
    public int getProductId(){
        return this.productId;
    }
    
    public void setProductId(int productId){
        this.productId = productId;
    }
    
    public void setPrice(Double price){
        this.price = price;
    }
    
    public int getProductQuantity(){
        return this.productQuantity;
    }
    
    public void setProductQuantity(int productQuantity){
        this.productQuantity = productQuantity;
    }
    
    public int getProductQuantityPerOrder(){
        return this.productQuantityPerOrder;
    }
    
    public void setProductQuantityPerOrder(int productQuantityPerOrder){
        this.productQuantityPerOrder = productQuantityPerOrder;
    }
    
    public Double getPrice(){
        return this.price;
    }
    
    public void setProductType(String productType){
        this.productType = ProductType.valueOf(productType.toUpperCase());
    }
    
    public String getProductType(){
        return this.productType.name();
    }

    public String getProductName(){
        return this.productName;
    }
    
    public void setProductName(String productName){
        this.productName = productName;
    }

    public String getProductPicLoc(){
        return this.productPicLoc;
    }
    
    public void setProductPicLoc(String loc){
        this.productPicLoc = loc;
    }
    
    public String getProductDetails(){
        return this.productDetails;
    }
    
    public void setProductDetails(String details){
        this.productDetails = details;
    }
    
    public void setLineItems(List<LineItem> lineitems){
        this.lineitems = lineitems;
    }
    
    public List<LineItem> getLineItems(){
        return this.lineitems;
    }
    
    public void addLineItem(LineItem lineitem){
        this.lineitems.add(lineitem);
    }
    
    public void removeLineItem(LineItem lineitem){
        this.lineitems.remove(lineitem);
    }
}
