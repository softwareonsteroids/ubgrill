package com.davide.Ubgrill.model.product;

public enum ProductType{
    GRILLS,
    DRINKS,
    OTHER,
    CHAIRS
}
