package com.davide.Ubgrill.model.wrappers;

import java.util.Set;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FufillInputWrapper{
    
    @JsonProperty("productsDelivered")
    public Set<Integer> productsDelivered; 

    public Set<Integer> getProductsDelivered(){
        return this.productsDelivered;
    }
    
    public void setProductsDelivered(Set<Integer> productsDelivered){
        this.productsDelivered = productsDelivered;
    }
    
}