package com.davide.Ubgrill.model.wrappers;

import java.util.ArrayList;
import java.util.List;
import com.davide.Ubgrill.model.product.LineItemDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderInputWrapper{
    
    @JsonProperty("productsToBeOrdered")
    public List<LineItemDTO> productsToBeOrdered;
    
    @JsonProperty("reservation")
    public int reservationId;
    
    public void setReservation(int reservationId){
        this.reservationId = reservationId;
    }
    
    public int getReservationId(){
        return this.reservationId;
    }
    
    public List<LineItemDTO> getProductsToBeOrdered(){
        return this.productsToBeOrdered;
    }
    
    public void setProductsToBeOrdered(List<LineItemDTO> productsToBeOrdered){
        this.productsToBeOrdered = productsToBeOrdered;
    }
    
    public ArrayList<Integer> getProductIds(){
        ArrayList<Integer> productIds = new ArrayList<>();
        for (LineItemDTO product: this.productsToBeOrdered){
            System.out.println(product);
            productIds.add(product.getId());
        }
        return productIds;
    }
    
    public int getQuantityFor(int productId){
        for (LineItemDTO product: this.productsToBeOrdered){
            if (product.getId() == productId){
                return product.getQuantity();
            }
        }
        return -1;
    }
}
