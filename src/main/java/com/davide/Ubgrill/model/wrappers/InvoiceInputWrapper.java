package com.davide.Ubgrill.model.wrappers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvoiceInputWrapper{
    @JsonProperty("deliveryCost")
    public double deliveryCost;
    
    @JsonProperty("deliveryTime")
    public String deliveryTime;
    
    @JsonProperty("deliveryMiles")
    public int deliveryMiles;
    
    public void setDeliveryCost(double deliveryCost){
        this.deliveryCost = deliveryCost;
    }
    
    public double getDeliveryCost(){
        return this.deliveryCost;
    }
    
    public void setDeliveryTime(String deliveryTime){
        this.deliveryTime = deliveryTime;
    }
    
    public String getDeliveryTime(){
        return this.deliveryTime;
    }
    
    public void setDeliveryMiles(int deliveryMiles){
        this.deliveryMiles = deliveryMiles;
    }
    
    public int getDeliveryMiles(){
        return this.deliveryMiles;
    }
}
