package com.davide.Ubgrill.model.user;

import javax.persistence.*;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Set;
import java.util.Date;

import com.davide.Ubgrill.model.reservation.Reservation;
import com.davide.Ubgrill.model.store.ClientOrder;


@Entity(name="CLIENT")
@Table(name="client")
public class Client{
    
    static final int MAX_LENGTH_USERNAME = 40;
    static final int MIN_LENGTH_USERNAME = 5;
    static final int MIN_LENGTH_PASSWORD = 8;
    
    @Id
    @Column(name="client_id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="users_sequence_generator")
    @GenericGenerator(
        name = "users_sequence_generator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
                //@Parameter(name = "sequence_name" , value = "product_entities_sequence")
                @Parameter(name = "optimizer" , value = "pooled-lo"),
                @Parameter(name = "initial_value", value = "13"),
                @Parameter(name = "increment_size", value = "1")
        }
    )
    private int clientId;
    
    @Column(name="email")
    @NotBlank
    private String email;
    
    @Column(name="username",nullable = false,unique = true)
    @Length(max=MAX_LENGTH_USERNAME, message="Your username can have at most 40 characters and at least 5 characters",
        min=MIN_LENGTH_USERNAME)
    @NotBlank
    private String username;
    
    @JsonProperty(access=JsonProperty.Access.WRITE_ONLY)
    @Column(name = "password")
    @Length(min = MIN_LENGTH_PASSWORD, message = "*Your password must have at least 8 characters")
    @NotBlank(message = "*Please provide your password")
    //@Transient  // This is for translating into JSON output not for storing passwords into the db
    private String password;
    
    @Column(name="firstname")
    @NotBlank
    private String firstname;
    
    @Column(name="lastname")
    @NotBlank
    private String lastname;
    
    @Column(name="enabled")
    @NotNull
    private boolean enabled;
    
    @Column(name = "last_password_reset_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordResetDate;
    
    // One to many mapping (one client can have many reservations)
    @OneToMany(mappedBy="client", cascade=CascadeType.ALL)
    private Set<Reservation> reservations;
    
    //private Address address; 
    
    // One to many mapping (one client can have many orders)
    @OneToMany(mappedBy="client", cascade=CascadeType.ALL)
    private Set<ClientOrder> orders;
    
    @ManyToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST}, fetch=FetchType.EAGER)
    @JoinTable(
        name="client_role",
        joinColumns= {@JoinColumn(name="client_id", referencedColumnName="client_id")}, 
        inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="role_id")}
    )
    private List<Role> roles;
    
    public Client(){}
    
    public Client(String email, String username,String password,String firstname,String lastname){
        this.email = email;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
    }
    
    
    public Integer getId(){
        return this.clientId;
    }
    
    public void setEnabled(boolean enabled){
        this.enabled = enabled;
    }
    
    public boolean getEnabled(){
        return this.enabled;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    public String getPassword(){
        return this.password;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public void setUsername(String username){
        this.username = username;
    }
    
    public String getUsername(){
        return this.username;
    }
    
    public void setFirstname(String firstname){
        this.firstname = firstname;
    }
    
    public String getFirstname(){
        return this.firstname;
    }
    
    public void setLastname(String lastname){
        this.lastname = lastname;
    }
    
    public String getLastname(){
        return this.lastname;
    }
    
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    
    public void setOrders(Set<ClientOrder> orders){
        this.orders = orders;
    }
    
    public Set<ClientOrder> getOrders(){
        return this.orders;
    }
    
    public void setReservations(Set<Reservation> reservations){
        this.reservations = reservations;
    }
    
    public Set<Reservation> getReservations(){
        return this.reservations;
    }
    
    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

}
