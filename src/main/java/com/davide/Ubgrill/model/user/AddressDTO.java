package com.davide.Ubgrill.model.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class AddressDTO{
    
    //@JsonProperty("addressId")
    @JsonIgnore
    public int addressId;
    
    @JsonProperty("zipcode")
    private String zipcode;
    
    @JsonProperty("state")
    private String state;
    
    @JsonProperty("country")
    private String country;
    
    @JsonProperty("street")
    private String street;
    
    AddressDTO(){}
    
    public AddressDTO(Address address){
        this.state = address.getState();
        this.country = address.getCountry();
        this.street = address.getStreet();
        this.zipcode = address.getZipcode();
        this.addressId = address.getId();
    }
    
    public int getId(){
        return this.addressId;
    }
    
    public String getState(){
        return this.state;
    }
    
    public String getCountry(){
        return this.country;
    }
    
    public String getStreet(){
        return this.street;
    }
    
    public String getZipcode(){
        return this.zipcode;
    }
    
    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder();
        sb.append("Address State:[");
        sb.append(this.state);
        sb.append("] Country:[");
        sb.append(this.country);
        sb.append("] Street:[");
        sb.append(this.street);
        sb.append("] Zipcode:[");
        sb.append(this.zipcode);
        sb.append("]");
        
        return sb.toString();
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        AddressDTO that = (AddressDTO) o;
        
        return Objects.equals(addressId, that.addressId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.addressId);
    }
    
}
