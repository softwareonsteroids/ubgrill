package com.davide.Ubgrill.model.user;

public enum RoleName{
    ROLE_USER, ROLE_ADMIN
}
