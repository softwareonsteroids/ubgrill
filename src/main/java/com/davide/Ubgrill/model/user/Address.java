package com.davide.Ubgrill.model.user;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import java.util.Objects;
import java.util.ArrayList;
import java.util.List;

import com.davide.Ubgrill.model.store.ClientOrderAddress;

@Entity(name="Address")
@Table(name="address")
public class Address{
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="address_id", nullable=false)
    private int addressId;
    
    @Column(name="zipcode")
    @Size(max=50)
    @NotBlank
    private String zipcode;
    
    @Column(name="city")
    @Size(max=50)
    @NotBlank
    private String city;
    
    @Column(name="state")
    @Size(max=50)
    @NotBlank
    private String state;
    
    @Column(name="country")
    @Size(max=50)
    @NotBlank
    private String country;
    
    @OneToMany(mappedBy="address", fetch=FetchType.LAZY)
    private List<ClientOrderAddress> orderAddresses = new ArrayList<>();
    
    @Column(name="street")
    @NotNull
    private String street;
    
    public Address() {}
    
    public Address(String street, String city, String state, 
        String country, String zipcode){
        this.street = street;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zipcode = zipcode;
    }
	
    public int getId(){
        return this.addressId;
    }
    
    public void setCountry(String country){
        this.country = country;
    }
    
    public String getCountry(){
        return this.country;
    }
    
    public void setState(String state){
        this.state = state;
    }
    
    public String getState(){
        return this.state;
    }
    
    public void setCity(String city){
        this.city = city;
    }
    
    public String getCity(){
        return this.city;
    }
    
    public void setStreet(String street){
        this.street = street;
    }
    
    public String getStreet(){
        return this.street;
    }
    
    public void setZipcode(String zipcode){
        this.zipcode = zipcode;
    }
    
    public String getZipcode(){
        return this.zipcode;
    }
    
    public void setClientOrderAddresses(List<ClientOrderAddress> orderAddresses){
        this.orderAddresses = orderAddresses;
    }
    
    public List<ClientOrderAddress> getClientOrderAddresses(){
        return this.orderAddresses;
    }
    
    public void addAddress(ClientOrderAddress address){
        this.orderAddresses.add(address);
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        Address address = (Address) o;
        return Objects.equals(this.addressId, address.addressId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.addressId);
    }
}