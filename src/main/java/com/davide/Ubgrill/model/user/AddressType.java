package com.davide.Ubgrill.model.user;

public enum AddressType{
   BILLING,
   SHIPPING,
   BILLINGANDSHIPPING
}
