package com.davide.Ubgrill.model.user;

import java.util.List;

import javax.persistence.*;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.validation.constraints.NotNull; 

@Entity(name="Role")
@Table(name="role")
public class Role{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="role_id")
    private Integer roleid;
    
    @Column(name="rolename")
    @NotBlank
    private String rolename;
    
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private List<Client> clients;

    public Role(){}
    
    public Integer getId(){
        return this.roleid;
    }
    
    public void setId(int id) {
	this.roleid = id;
    }
    
    public Role(String rolename){
        this.rolename = rolename;
    }
    
    public void setRolename(String rolename){
        this.rolename = rolename;
    }
    
    public String getRolename(){
        return this.rolename;
    }
    
    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role )) return false;
        return roleid != null && roleid.equals(((Role) o).roleid);
    }
    @Override
    public int hashCode() {
        return 31;
    }
}
