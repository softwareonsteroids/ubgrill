package com.davide.Ubgrill.model.reservation;

import javax.persistence.*;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Time;
import java.util.Date;
//import java.sql.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.davide.Ubgrill.model.user.Client;


@Entity(name="Reservation")
@Table(name="reservation")
public class Reservation{
    
    @Id
    @Column(name="reservation_id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="reservation_sequence_generator")
    @GenericGenerator(
        name = "reservation_sequence_generator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
                //@Parameter(name = "sequence_name" , value = "product_entities_sequence")
                @Parameter(name = "optimizer" , value = "pooled-lo"),
                @Parameter(name = "initial_value", value = "50"),
                @Parameter(name = "increment_size", value = "1")
        }
    )
    private int reservationId;
    
    @Column(name="date_reserved")
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
        pattern="yyyy-dd-MM hh:mm:ss",
        timezone="UTC")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date dateReserved;
    
    @Column(name="time_reserved_start")
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
        pattern="HH:mm:ss",
        timezone="UTC")
    @Temporal(TemporalType.TIME)
    @NotNull
    private Date timeReservedStart;
    
    @Column(name="time_reserved_end")
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
        pattern="HH:mm:ss",
        timezone="UTC")
    @Temporal(TemporalType.TIME)
    @NotNull
    private Date timeReservedEnd;
    
    // Many to one mapping, one reservation for one client
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;

    public Reservation(){
    
    }
    
    public Reservation(Date dateReserved, 
            Date timeReservedStart,
            Date timeReservedEnd){
        this.dateReserved = dateReserved;
        this.timeReservedStart = timeReservedStart;
        this.timeReservedEnd = timeReservedEnd;
    }
    
    public Integer getReservationId(){
        return this.reservationId;
    }
    
    public void setDateReserved(Date dateReserved){
        this.dateReserved = dateReserved;
    }
    
    public Date getDateReserved(){
        return this.dateReserved;
    }
    
    public void setTimeReservedStart(Date timeReservedStart){
        this.timeReservedStart = timeReservedStart;
    }
    
    public Date getTimeReservedStart(){
        return this.timeReservedStart;
    }
    
    public void setTimeReservedEnd(Date timeReservedEnd){
        this.timeReservedEnd = timeReservedEnd;
    }
    
    public Date getTimeReservedEnd(){
        return this.timeReservedEnd;
    }
    
    public Client getClient(){
        return this.client;
    }
    
    public void setClient(Client client){
        this.client = client;
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return Objects.equals(reservationId, that.reservationId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.reservationId);
    }
}
