package com.davide.Ubgrill.model.store;

import javax.persistence.*;
import java.io.Serializable;
import com.davide.Ubgrill.model.product.Product;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="LineItem")
@Table(name="lineitem")
public class LineItem implements Serializable{
    
    @EmbeddedId
    public LineItemId id = new LineItemId();
    
    @Column(name="product_quantity")
    private int productQuantity;
    
    @Column(name="is_fufilled")
    private boolean isFufilled;
    
    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(
        name="clientorder_id",
        insertable = false, updatable = false)
    public ClientOrder clientorder;
    
    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="product_id",
        insertable = false, updatable = false)
    public Product product;
    
    public LineItem(){}
    
    public LineItem(ClientOrder order,
        Product product){
        this.clientorder = order;
        this.product = product;
        this.id.clientOrderId = order.getId();
        this.id.productId = product.getProductId();
    }
    
    public LineItemId getId(){
        return this.id;
    }
    
    public ClientOrder getClientOrder(){
        return this.clientorder;
    }
    
    public Product getProduct(){
        return this.product;
    }
    
    public void setProductQuantity(int productQuantity){
        this.productQuantity = productQuantity;
    }
    
    public int getProductQuantity(){
        return this.productQuantity;
    }
    
    public void setFufilled(boolean isFufilled){
        this.isFufilled = isFufilled;
    }
    
    public boolean isFufilled(){
        return this.isFufilled;
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        LineItem that = (LineItem) o;
        
        return Objects.equals(clientorder, that.clientorder) &&
            Objects.equals(product, that.product);
    }   
    
    @Override
    public int hashCode(){
        return Objects.hash(clientorder, product);
    }
}
