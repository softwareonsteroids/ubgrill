package com.davide.Ubgrill.model.store;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.GenericGenerator;
import java.util.Objects;
import java.util.Date;
import java.sql.Time;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

// You have to name your get statements the same name as your fields

@Entity(name="Invoice")
@Table(name="invoice")
public class Invoice{
        
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="invoice_id")
    private Integer invoiceId;
    
    // One to one unidirectional relationship with orders
    //@JsonIgnore
    @OneToOne
    @JoinColumn(name="clientorder_id", unique=true, nullable=false, updatable=true)
    private ClientOrder order;
    
    @Column(name="order_cost")
    @NotNull
    private double orderCost;
    
    @Column(name="created_on")
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
        pattern="yyyy-dd-MM HH:mm:ss",
        timezone="UTC")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date createdOn = new Date();
    
    @Column(name="updated_on")
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
        pattern="yyyy-dd-MM HH:mm:ss",
        timezone="UTC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    
    @JsonProperty(value="hasPayed")
    @Column(name="has_payed")
    private boolean hasPayed;
    
    @Column(name="delivery_time")
    private String deliveryTime;
    
    @Column(name="delivery_miles")
    private int deliveryMiles;
    
    @Column(name="delivery_cost")
    private double deliveryCost;
    
    public Invoice() {}
    
    public Invoice(ClientOrder order, double orderCost){
        this.order = order;
        this.orderCost = orderCost;
        this.hasPayed = false;
    }
	
    public int getId(){
        return this.invoiceId;
    }
    
    public void setUpdatedOn(Date date){
        this.updatedOn = date;
    }
    
    public Date getUpdatedOn(){
        return this.updatedOn;
    }
    
    public void setOrderCost(double orderCost){
        this.orderCost = orderCost;
    }
    
    public double getOrderCost(){
        return this.orderCost;
    }
    
    public void setDeliveryTime(String deliveryTime){
        this.deliveryTime = deliveryTime;
    }
    
    public String getDeliveryTime(){
        return this.deliveryTime;
    }
    
    public void setDeliveryMiles(int deliveryMiles){
        this.deliveryMiles = deliveryMiles;
    }
    
    public int getDeliveryMiles(){
        return this.deliveryMiles;
    }
    
    public void setDeliveryCost(double deliveryCost){
        this.deliveryCost = deliveryCost;
    }
    
    public double getDeliveryCost(){
        return this.deliveryCost;
    }
    
    public void setOrder(ClientOrder order){
        this.order = order;
    }
	
    public ClientOrder getOrder(){
        return this.order;
    }
	
    public void setHasPayed(boolean hasPayed){
        this.hasPayed = hasPayed;	
    }
    
    public boolean hasPayed(){
        return this.hasPayed;
    }
}
