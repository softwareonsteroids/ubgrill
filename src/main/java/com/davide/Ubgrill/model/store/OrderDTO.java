package com.davide.Ubgrill.model.store;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

public class OrderDTO{
    
    @JsonProperty("orderId")
    public int orderId;
    
    @JsonProperty("dateCreated")
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
        pattern="dd-MM-yyyy HH:mm:ss",
        timezone="UTC")
    public Date createdOn;
    
    OrderDTO(){}

    public OrderDTO(ClientOrder order){
        this.orderId = order.getId();
        this.createdOn = order.getCreatedOn(); 
    }
    
    public Date getCreatedOn(){
        return this.createdOn;
    }
    
    public int getOrderId(){
        return this.orderId;
    }
    
    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder();
        sb.append("Order id: ");
        sb.append("Id:[");
        sb.append(String.valueOf(this.orderId));
        sb.append("]");
        
        return sb.toString();
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        OrderDTO that = (OrderDTO) o;
        
        return Objects.equals(orderId, that.orderId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.orderId);
    }
}
