package com.davide.Ubgrill.model.store;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LineItemPartialOutputDTO{
    
    @JsonProperty("orderId")
    public int orderId;
    
    @JsonProperty("productId")
    public int productId;
    
    @JsonProperty("productQuantity")
    public int productQuantity;
    
    @JsonProperty("isFufilled")
    public boolean isFufilled;
    
    LineItemPartialOutputDTO(){}
    
    public LineItemPartialOutputDTO(LineItem item){
        this.orderId = item.getId().clientOrderId;
        this.productId = item.getId().productId;
        this.productQuantity = item.getProductQuantity();
        this.isFufilled = item.isFufilled();
    }
    
    public int getProductId(){
        return this.productId;
    }
    
    public int getOrderId(){
        return this.orderId;
    }
    
    public int getQuantity(){
        return this.productQuantity;
    }
    
    public boolean getIsFufilled(){
        return this.isFufilled;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Product To Be Ordered: ");
        sb.append("Id:[");
        sb.append(String.valueOf(this.productId));
        sb.append("] Quantity:[");
        sb.append(String.valueOf(this.productQuantity));
        sb.append("] Order it belongs to:[");
        sb.append(String.valueOf(this.orderId));
        sb.append("]");
        return sb.toString();
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        LineItemPartialOutputDTO that = (LineItemPartialOutputDTO) o;
        
        return Objects.equals(productId, that.productId) && 
            Objects.equals(orderId, that.orderId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.productId, this.orderId);
    }
} 
