package com.davide.Ubgrill.model.store;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.validation.constraints.NotNull;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Iterator;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.davide.Ubgrill.model.reservation.Reservation;
import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.model.user.Address;
import com.davide.Ubgrill.model.product.Product;

@Entity(name="ClientOrder")
@Table(name="clientorder")
public class ClientOrder{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="clientorder_id", nullable=false)
    private Integer clientorderId;
    
    @Column(name="created_on")
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
        pattern="dd-MM-yyyy HH:mm:ss",
        timezone="UTC")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date createdOn = new Date();
    
    // Using ArrayList returns an error[Illegal attempt to map a non collection as a @OneToMany, 
    // @ManyToMany or @CollectionOfElements], because you should always map to interfaces instead of implementations 
    @OneToMany(mappedBy="clientorder", fetch=FetchType.LAZY)
    private List<LineItem> lineitems = new ArrayList<>();
    
    @OneToMany(mappedBy="clientorder", fetch=FetchType.LAZY)
    private List<ClientOrderAddress> clientOrderAddresses = new ArrayList<>();
    
    // Many to one mapping, one reservation for one client
    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;
    
    @OneToOne
    @JoinColumn(name="reservation_id")
    private Reservation reservation;
    
    public ClientOrder(){}
    
    public void setId(int id){
        this.clientorderId = id;
    }
    
    public Integer getId(){
        return this.clientorderId;
    }
    
    public void setCreatedOn(Date createdOn){
        this.createdOn = createdOn;
    }
    
    public Date getCreatedOn(){
        return this.createdOn;
    }
    
    public void setClient(Client client){
        this.client = client;
    }
    
    public Client getClient(){
        return this.client;
    }
    
    public void setReservation(Reservation reservation){
        this.reservation = reservation;
    }
    
    public Reservation getReservation(){
        return this.reservation;
    }
    
    public List<LineItem> getLineItems(){
        return this.lineitems;
    }
    
    public void setLineItems(List<LineItem> lineitems){
        this.lineitems = lineitems;
    }
    
    public void addLineItem(LineItem lineitem){
        this.lineitems.add(lineitem);
    }
    
    public void removeLineItem(LineItem lineitem){
        this.lineitems.remove(lineitem);
    }
    
    public List<ClientOrderAddress> getClientOrderAddresses(){
        return this.clientOrderAddresses;
    }
    
    public void setClientOrderAddresses(List<ClientOrderAddress> clientOrderAddresses){
        this.clientOrderAddresses = clientOrderAddresses;
    }
    
    public void addAddress(ClientOrderAddress address){
        this.clientOrderAddresses.add(address);
    }
    
    public void removeAddress(ClientOrderAddress address){
        this.clientOrderAddresses.remove(address);
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ClientOrder order = (ClientOrder) o;
        return Objects.equals(this.clientorderId, order.clientorderId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(this.clientorderId);
    }
}
