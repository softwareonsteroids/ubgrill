package com.davide.Ubgrill.model.store;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class LineItemId implements Serializable{
    
    @Column(name="clientorder_id")
    public int clientOrderId;
    
    @Column(name="product_id")
    public int productId;
    
    public LineItemId(){ }
    
    public LineItemId(int clientOrderId, int productId){
        this.productId = productId;
        this.clientOrderId = clientOrderId;
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        LineItemId that = (LineItemId) o;
        
        return Objects.equals(productId, that.productId) &&
            Objects.equals(clientOrderId, that.clientOrderId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(productId, clientOrderId);
    }
}
