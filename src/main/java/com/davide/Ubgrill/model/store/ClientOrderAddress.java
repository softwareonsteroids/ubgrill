package com.davide.Ubgrill.model.store;

import javax.persistence.*;
import java.io.Serializable;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import com.davide.Ubgrill.model.user.Address;
import com.davide.Ubgrill.model.user.AddressType;
import com.davide.Ubgrill.model.product.Product;

@Entity(name="ClientOrderAddress")
@Table(name="clientorderaddress")
public class ClientOrderAddress implements Serializable{
    
    @EmbeddedId
    public ClientOrderAddressId id = new ClientOrderAddressId();
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(
        name="clientorder_id",
        insertable = false, updatable = false)
    public ClientOrder clientorder;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="address_id",
        insertable = false, updatable = false)
    public Address address;
    
    @Enumerated(EnumType.STRING)
    @Column(name="address_type")
    @NotNull
    private AddressType addressType;
    
    public ClientOrderAddress(){}
    
    public ClientOrderAddress(ClientOrder order, 
        Address address){
        this.clientorder = order;
        this.address = address;
        this.id.clientOrderId = order.getId();
        this.id.addressId = address.getId();
    }
    
    public ClientOrder getClientOrder(){
        return this.clientorder;
    }
    
    public Address getAddress(){
        return this.address;
    }
    
    public void setAddressType(String addressType){
        this.addressType = AddressType.valueOf(addressType.toUpperCase());
    }
    
    public String getAddressType(){
        return this.addressType.name();
    }
       
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ClientOrderAddress that = (ClientOrderAddress) o;
        
        return Objects.equals(clientorder, that.clientorder) &&
            Objects.equals(address, that.address);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(clientorder, address);
    }
    
    @Override
    public String toString(){
        return "Address type=" + this.addressType.name(); 
    }
} 
