package com.davide.Ubgrill.model.store;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ClientOrderAddressId implements Serializable{
    
    @Column(name="clientorder_id")
    public int clientOrderId;
    
    @Column(name="address_id")
    public int addressId;
    
    public ClientOrderAddressId(){ }
    
    public ClientOrderAddressId(int clientOrderId, int addressId){
        this.addressId = addressId;
        this.clientOrderId = clientOrderId;
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ClientOrderAddressId that = (ClientOrderAddressId) o;
        
        return Objects.equals(addressId, that.addressId) &&
            Objects.equals(clientOrderId, that.clientOrderId);
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(addressId, clientOrderId);
    }
} 
