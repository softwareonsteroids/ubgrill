package com.davide.Ubgrill.config.security;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthUtils implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;
 
    @Value("${jwt.secret}")
    private String secret;
    
    @Value("${jwt.expiration}")
    private Long expirationTime;
    
    private Date presentDate(){
        return new Date();
    }
    
    private Claims getClaimsFromToken(String token){
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }
    
    private Object getValueFromTokenClaims(String token, JwtAuthEnum en){
        switch(en){
            case SUBJECT:
                return getClaimsFromToken(token).getSubject();
            case ISSUEDAT:
                return getClaimsFromToken(token).getIssuedAt();
            case EXPIRATION:
                return getClaimsFromToken(token).getExpiration();
            case AUDIENCE:
                return getClaimsFromToken(token).getAudience();
        }
        return null;
    }
    
    public String getUsernameFromToken(String token){
        return (String)getValueFromTokenClaims(token, JwtAuthEnum.SUBJECT);
    }
    
    public Date getIssuedAtDateFromToken(String token) {
        return (Date)getValueFromTokenClaims(token, JwtAuthEnum.ISSUEDAT);
    }
    
    public Date getExpirationDateFromToken(String token) {
        return (Date)getValueFromTokenClaims(token, JwtAuthEnum.EXPIRATION);
    }

    public String getAudienceFromToken(String token) {
        return (String)getValueFromTokenClaims(token, JwtAuthEnum.AUDIENCE);
    }

    
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(presentDate());
    }
    
    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordResetDate) {
        return (lastPasswordResetDate != null && created.before(lastPasswordResetDate));
    }
    
    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + expirationTime * 1000);
    }
    
    public String generateToken(UserDetails userDetails, JwtAuthEnum en){
        final Date createdDate = presentDate();
        final Date expirationDate = calculateExpirationDate(createdDate);
        Map<String, Object> claims = new HashMap<>(); 
        
        System.out.println("generateToken " + createdDate);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setAudience(en.getShortName())
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
    
    public Boolean canTokenBeRefreshed(String token, Date lastPasswordResetDate) {
        final Date created = getIssuedAtDateFromToken(token);
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordResetDate)
                && (isTokenExpired(token) == false);
    }
    
    public String refreshToken(String token) {
        final Date createdDate = presentDate();
        final Date expirationDate = calculateExpirationDate(createdDate);

        final Claims claims = getClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        JwtAuthUser user = (JwtAuthUser) userDetails;
        final String username = getUsernameFromToken(token);
        final Date created = getIssuedAtDateFromToken(token);
        return (
              username.equals(user.getUsername())
                    && (isTokenExpired(token) == false)
                    && (isCreatedBeforeLastPasswordReset(created, user.getLastPasswordResetDate()) == false)
        );
    }
}