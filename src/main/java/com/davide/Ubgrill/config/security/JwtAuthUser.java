package com.davide.Ubgrill.config.security;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.davide.Ubgrill.model.user.Client;
import com.davide.Ubgrill.model.user.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class JwtAuthUser implements UserDetails{

    private final Integer id;
    private final String username;
    private final String firstname;
    private final String lastname;
    private final String password;
    private final String email;
    private final Collection<? extends GrantedAuthority> roles;
    private final boolean enabled;
    private final Date lastPasswordResetDate;
    
    public JwtAuthUser(Client client){
        this.id = client.getId();
        this.username = client.getUsername();
        this.firstname = client.getFirstname();
        this.lastname = client.getLastname();
        this.password = client.getPassword();
        this.email = client.getEmail();
        this.roles = this.getRoles(client);
        this.enabled = client.getEnabled();
        this.lastPasswordResetDate = client.getLastPasswordResetDate();
    }
    
    @JsonIgnore
    public Integer getId() {
        return id;
    }
    
    @Override
    public String getUsername() {
        return this.username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    
    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getEmail() {
        return this.email;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate() {
        return this.lastPasswordResetDate;
    }
     
    private Collection <GrantedAuthority> getRoles(Client client){
        Collection <GrantedAuthority> roles = new ArrayList<>();
        for (Role role:client.getRoles()){
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRolename());
            roles.add(grantedAuthority);
        }
        return roles;
    }

}
