package com.davide.Ubgrill.config.security;

public enum JwtAuthEnum{
    SUBJECT("sub"),
    ISSUEDAT("iat"),
    AUDIENCE("aud"),
    EXPIRATION("exp");
    
    private final String shortName;
    
    JwtAuthEnum(String shortName) {
        this.shortName = shortName;
    }
    
    public String getShortName() {
        return this.shortName;
    }
}
