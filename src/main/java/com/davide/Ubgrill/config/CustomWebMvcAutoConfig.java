package com.davide.Ubgrill.config;

import org.springframework.context.annotation.Configuration;
//import org.springframework.boot.autoconfigure.AutoConfigureAfter;
//import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.MediaType;
import java.util.List;
import java.util.ArrayList;


// For the FetchType.LAZY
@Configuration
//@AutoConfigureAfter(DispatcherServletAutoConfiguration.class)
public class CustomWebMvcAutoConfig extends WebMvcConfigurerAdapter{
    /**
    @Override
    public void extendMessageConverters(List [HttpMessageConverter?>> converters){
        for (HttpMessageConverter converter : converters){
            if (converter instanceof org.springframework.http.converter.json.MappingJackson2HttpMessageConverter){
                ObjectMapper mapper = ((MappingJackson2HttpMessageConverter) converter).getObjectMapper();
                mapper.registerModule(new Hibernate4Module());
            }
        }
    }**/
    
    public void configureMessageConverters(List <HttpMessageConverter<?>> converters){
        List<MediaType> supportedMediaTypes=new ArrayList<>();
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
        supportedMediaTypes.add(MediaType.TEXT_PLAIN);
        AbstractJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(new HibernateAwareObjectMapper());
        converter.setPrettyPrint(true);
        converter.setSupportedMediaTypes(supportedMediaTypes);
        converters.add(converter);
        super.configureMessageConverters(converters);
    }
}

