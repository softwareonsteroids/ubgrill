# Ubgrill

> This is an application  for grill delivery. It is a toy app. Any one can use to reserve, receive, deliver the grills on offer to anywhere, along with some accountrements.

## Features
- User will be able to make an acccount.
- User will be able to edit account.
- User will be able to make a reservation/order.
- User will be able to edit reservation/order.

## Technologies

### Backend
- Spring Boot
- Spring Data Rest
- Hibernate
- MySQL
- Maven
- Junit
- JWT

### Frontend
- React
- Webpack
- Axios

## Running the application

### Backend(To build)

> mvn clean package docker:build

### Backend(To run)

> docker-compose up -d

#### Testing

- To get auth
> > http POST http://localhost:9090/api/auth username=***** password=******

- To start interacting with api (for example with /products)
> > http GET http://localhost:9090/api/products Authorization:" Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqZWZmMDkiLCJhdWQiOiJhdWQiLCJleHAiOjE1MTg0ODk5MzMsImlhdCI6MTUxNzg4NTEzM30.h9QJEFYGuKkNRUWXF8DvkdoeQZzgBe4pk8AUIz-boTU4bINOw7z8uCHN-G5RAUmRnkNOeA52tXl2WOQysyaJug"

### Frontend 
> `cd src/main/frontend`

> `yarn start`

### Stuff for version 2

- [] Make the api more queryable
- [] Add a Stripe webhook
- [] Add an admin interface
- [] Add a timer 
- [] Add Swagger integration
